/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/style_db.hpp"

namespace MWdg
{

/// @brief Mixer_Style_Db
///
class Mixer_Style_Db : public Wdg::Style_Db
{
  public:
  // -- Construction

  Mixer_Style_Db ( QObject * parent_n = nullptr );

  ~Mixer_Style_Db ();

  // -- Palette interface

  void
  update_palettes ( const QPalette & pal_n );

  // -- Dark theme

  bool
  force_dark_theme () const
  {
    return _force_dark_theme;
  }

  Q_SLOT
  void
  set_force_dark_theme ( bool value_n );

  private:
  // -- Private methods

  bool
  detect_dark_theme ( const QPalette & pal_n );

  QPalette
  palette_playback ( const QPalette & pal_default_n );

  QPalette
  palette_capture ( const QPalette & pal_default_n );

  private:
  // -- Attributes
  bool _force_dark_theme = false;
};

} // namespace MWdg
