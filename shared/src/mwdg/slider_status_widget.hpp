/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/label_elide.hpp"
#include "wdg/label_width.hpp"
#include <QDoubleSpinBox>
#include <QLabel>
#include <QLocale>
#include <QSpinBox>
#include <QWidget>

// Forward declaration
namespace Wdg::Sliders_Pad
{
class Pad;
}

namespace MWdg
{

/// @brief Slider_Status_Widget
///
class Slider_Status_Widget : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Slider_Status_Widget ( QWidget * parent_n = nullptr );

  ~Slider_Status_Widget ();

  // -- Sliders pad

  Wdg::Sliders_Pad::Pad *
  sliders_pad () const
  {
    return _sliders_pad;
  }

  void
  set_sliders_pad ( Wdg::Sliders_Pad::Pad * pad_n );

  // -- Value string generators

  QString
  volume_string ( long volume_n ) const
  {
    return _loc.toString ( (int)volume_n );
  }

  QString
  db_string ( double db_val_n ) const
  {
    return _loc.toString ( db_val_n, 'f', 2 );
  }

  // -- Element info

  virtual QString
  elem_name () const = 0;

  virtual bool
  elem_has_volume () const = 0;

  virtual bool
  elem_has_dB () const = 0;

  virtual long
  elem_volume_value () const = 0;

  virtual void
  elem_set_volume ( long value_n ) const = 0;

  virtual long
  elem_volume_min () const = 0;

  virtual long
  elem_volume_max () const = 0;

  virtual long
  elem_dB_value () const = 0;

  virtual void
  elem_set_nearest_dB ( long dB_value_n ) const = 0;

  virtual long
  elem_dB_min () const = 0;

  virtual long
  elem_dB_max () const = 0;

  // -- Slider selection

  virtual void
  select_slider ( unsigned int grp_idx_n, unsigned int col_idx_n ) = 0;

  // -- Public slots

  Q_SLOT
  void
  mixer_values_changed ();

  Q_SLOT
  void
  slider_focus_changed ();

  // -- Protected slots
  protected:
  Q_SLOT
  void
  update_values ();

  Q_SLOT
  void
  volume_value_changed ( int value_n );

  Q_SLOT
  void
  db_value_changed ( double value_n );

  protected:
  // -- Protected methods

  void
  setup_values ();

  void
  contextMenuEvent ( QContextMenuEvent * event_n );

  private:
  // -- Attributes
  Wdg::Sliders_Pad::Pad * _sliders_pad;

  // Widgets
  Wdg::Label_Elide _elem_name;

  QLabel _lbl_value;
  QLabel _lbl_max;
  QLabel _lbl_min;

  QLabel _volume_title;
  QSpinBox _volume_value;
  Wdg::Label_Width _volume_max;
  Wdg::Label_Width _volume_min;

  QLabel _db_title;
  QDoubleSpinBox _db_value;
  Wdg::Label_Width _db_max;
  Wdg::Label_Width _db_min;

  QLocale _loc;
};

} // namespace MWdg
