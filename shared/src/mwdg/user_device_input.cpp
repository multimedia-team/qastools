/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "user_device_input.hpp"
#include <QLabel>
#include <QVBoxLayout>

namespace MWdg
{

User_Device_Input::User_Device_Input ( QWidget * parent_n )
: QWidget ( parent_n )
{
  QLabel * title = new QLabel ( this );
  {
    QFont fnt = title->font ();
    fnt.setBold ( true );
    title->setFont ( fnt );
  }
  title->setText ( tr ( "User device" ) );

  _line_edit = new QLineEdit ( this );
  connect ( _line_edit,
            &QLineEdit::editingFinished,
            this,
            &User_Device_Input::line_edit_string_changed );

  QVBoxLayout * lay_vbox ( new QVBoxLayout );
  lay_vbox->setContentsMargins ( 0, 0, 0, 0 );
  lay_vbox->addWidget ( title, 0 );
  lay_vbox->addWidget ( _line_edit, 0 );
  setLayout ( lay_vbox );
}

User_Device_Input::~User_Device_Input () = default;

void
User_Device_Input::set_device_silent ( const QString & str_n )
{
  if ( _device != str_n ) {
    _device = str_n;
    {
      _line_edit_signal_block = true;
      _line_edit->setText ( str_n );
      _line_edit_signal_block = false;
    }
    Q_EMIT sig_device_changed ();
  }
}

void
User_Device_Input::line_edit_string_changed ()
{
  if ( _device != _line_edit->text () ) {
    _device = _line_edit->text ();
    if ( !_line_edit_signal_block ) {
      Q_EMIT sig_device_changed ();
    }
  }
}

} // namespace MWdg
