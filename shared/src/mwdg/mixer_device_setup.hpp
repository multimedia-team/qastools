/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/ctl_address.hpp"

namespace MWdg
{

/// @brief Mixer_Device_Setup
///
class Mixer_Device_Setup
{
  public:
  // -- Construction

  Mixer_Device_Setup ();

  ~Mixer_Device_Setup ();

  // -- Attributes
  QSnd::Ctl_Address control_address;
};

} // namespace MWdg
