/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/style_db.hpp"

namespace MWdg
{

enum Mixer_Style
{
  CUSTOM = Wdg::Style_Db::STYLE_USER,
  PLAYBACK = CUSTOM,
  CAPTURE = CUSTOM + 1
};

} // namespace MWdg
