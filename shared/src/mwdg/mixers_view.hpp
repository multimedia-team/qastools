/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QListView>

namespace MWdg
{

/// @brief Mixers_View
///
class Mixers_View : public QListView
{
  Q_OBJECT

  public:
  // -- Construction

  Mixers_View ( QWidget * parent_n = nullptr );

  ~Mixers_View ();

  // -- Size hints

  QSize
  minimumSizeHint () const override;

  QSize
  sizeHint () const override;

  void
  setModel ( QAbstractItemModel * model_n ) override;

  bool
  event ( QEvent * event_n ) override;

  // -- Protected slots
  protected:
  Q_SLOT
  void
  maximum_height_update_request ();

  protected:
  // -- Protected methods

  void
  changeEvent ( QEvent * event_n ) override;

  void
  maximum_height_update ();

  private:
  // -- Attributes
  int _show_rows_avrg = 10;
  int _chars_vis_min = 12;
  int _chars_vis_avrg = 20;

  bool _maximum_update_requested = false;
};

} // namespace MWdg
