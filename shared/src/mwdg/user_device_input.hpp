/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLineEdit>
#include <QString>
#include <QWidget>

namespace MWdg
{

/// @brief User_Device_Input
///
class User_Device_Input : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  User_Device_Input ( QWidget * parent_n = nullptr );

  ~User_Device_Input ();

  // -- Device string

  const QString &
  device () const
  {
    return _device;
  }

  void
  set_device_silent ( const QString & str_n );

  Q_SIGNAL
  void
  sig_device_changed ();

  // Private slots
  private:
  Q_SLOT
  void
  line_edit_string_changed ();

  private:
  // -- Attributes
  QString _device;
  QLineEdit * _line_edit = nullptr;
  bool _line_edit_signal_block = false;
};

} // namespace MWdg
