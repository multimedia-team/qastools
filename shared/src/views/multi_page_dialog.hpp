/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "basic_dialog.hpp"
#include <QByteArray>
#include <QModelIndex>
#include <cstddef>

// Forward declarations
class QSplitter;
class QStandardItemModel;
class QStackedLayout;
class QListView;

namespace Views
{

/// @brief Multi_Page_Dialog
///
class Multi_Page_Dialog : public Views::Basic_Dialog
{
  Q_OBJECT

  public:
  // -- Construction

  Multi_Page_Dialog ( QWidget * parent_n = nullptr,
                      Qt::WindowFlags flags_n = Qt::WindowFlags () );

  ~Multi_Page_Dialog ();

  // -- Setup

  void
  add_pages_begin ();

  void
  add_page ( const QString & name_n, QWidget * wdg_n );

  void
  add_page_vscroll ( const QString & name_n, QWidget * wdg_n );

  void
  add_pages_end ();

  // -- Page indices

  std::size_t
  num_pages () const;

  std::size_t
  current_page_idx () const;

  void
  set_current_page_idx ( std::size_t idx_n );

  // -- Splitter

  QByteArray
  splitter_state () const;

  void
  restore_splitter_state ( const QByteArray & state_n );

  private:
  // -- Private slots

  Q_SLOT
  void
  page_changed ( const QModelIndex & cur_n, const QModelIndex & prev_n );

  Q_SLOT
  void
  page_selected ( const QModelIndex & index_n );

  private:
  // -- Attributes
  // Pages
  QSplitter * _splitter = nullptr;
  QListView * _page_selection = nullptr;
  QStackedLayout * _lay_pages_stack = nullptr;
  QStandardItemModel * _pages_model = nullptr;
};

} // namespace Views
