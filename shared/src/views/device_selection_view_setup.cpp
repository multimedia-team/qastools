/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "device_selection_view_setup.hpp"

namespace Views
{

Device_Selection_View_Setup::Device_Selection_View_Setup () = default;

Device_Selection_View_Setup::~Device_Selection_View_Setup () = default;

} // namespace Views
