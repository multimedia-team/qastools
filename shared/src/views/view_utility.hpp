/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QMainWindow>
#include <QWidget>

namespace Views
{

void
load_translators ( QApplication * app_n );

void
load_application_icon ( QApplication * app_n, const QString & fallback_n );

/// @return true on successful size determination
bool
win_default_geometry ( const QMainWindow * mwin_n, QRect & rect_n );

/// @return true on successful size determination
bool
win_default_size ( const QMainWindow * mwin_n, QSize & size_n );

void
resize_to_default ( QMainWindow * mwin_n );

} // namespace Views
