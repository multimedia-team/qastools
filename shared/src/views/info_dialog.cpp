/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "info_dialog.hpp"
#include "qastools_config.hpp"
#include "wdg/text_browser.hpp"
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QLocale>
#include <QPushButton>
#include <QVBoxLayout>

namespace Views
{

QAction *
Info_Dialog::create_action_about ( QObject * parent_n )
{
  QAction * act_about = new QAction ( tr ( "&About" ), parent_n );
  act_about->setShortcut ( QKeySequence ( QKeySequence::HelpContents ) );
  act_about->setIcon ( QIcon::fromTheme ( "help-about" ) );
  return act_about;
}

QAction *
Info_Dialog::create_action_about_qt ( QObject * parent_n )
{
  QAction * act_about_qt = new QAction ( tr ( "About &Qt" ), parent_n );
  return act_about_qt;
}

Info_Dialog::Info_Dialog ( QWidget * parent_n, Qt::WindowFlags flags_n )
: Views::Multi_Page_Dialog ( parent_n, flags_n )
{
  // Window title
  {
    QString txt ( "%1 - %2" );
    txt = txt.arg ( PROGRAM_TITLE );
    txt = txt.arg ( tr ( "About" ) );
    setWindowTitle ( txt );
  }

  // Title label
  {
    QString txt ( "%1 - %2" );
    txt = txt.arg ( PACKAGE_TITLE );
    txt = txt.arg ( PACKAGE_VERSION );
    set_title_str ( txt );
  }

  Wdg::Text_Browser * _txt_info = nullptr;
  Wdg::Text_Browser * _txt_people = nullptr;
  Wdg::Text_Browser * _txt_license = nullptr;

  const QString hmask ( "<h3>%1</h3>\n" );

  // Description text
  {
    const QString amask ( "<a href=\"%1\" title=\"%2\">%1</a>" );
    QString txt;
    txt += hmask.arg ( tr ( "About" ) );

    txt += "<p>";
    {
      QString val ( tr ( "%1 is a collection of desktop applications for the "
                         "Linux sound system %2." ) );
      val = val.arg ( PACKAGE_TITLE );
      val = val.arg ( "<a href=\"http://www.alsa-project.org\" title=\"The "
                      "ALSA project\">ALSA</a>" );
      txt += val;
    }
    txt += "</p>";

    // Internet
    txt += hmask.arg ( tr ( "Internet" ) );

    txt += "<p>";
    txt += amask.arg ( "https://gitlab.com/sebholt/qastools",
                       tr ( "Project page" ) );
    txt += "</p>";

    _txt_info = new Wdg::Text_Browser;
    _txt_info->setAlignment ( Qt::AlignLeft | Qt::AlignTop );
    _txt_info->setWordWrapMode ( QTextOption::NoWrap );
    _txt_info->setOpenLinks ( true );
    _txt_info->setOpenExternalLinks ( true );
    _txt_info->setHtml ( txt );
  }

  // People text
  {
    const QString pemask ( "%1 &lt;<a href=\"mailto:%2\">%2</a>&gt;" );
    QString trmask;
    QString pdivmask;
    {
      const QString pdiv ( "<div style=\"margin-bottom: 4px;\">%1</div>" );
      trmask.append ( pemask );
      trmask.append ( " [%3]" );
      trmask = pdiv.arg ( trmask );
      pdivmask = pdiv.arg ( pemask );
    }

    QString txt;

    // Developers
    {
      txt += hmask.arg ( tr ( "Developers" ) );
      auto add = [ &txt, &pdivmask ] ( const QString & name_n,
                                       const QString & email_n ) {
        txt += pdivmask.arg ( name_n, email_n );
      };
      add ( "Sebastian Holtermann", "sebholt@web.de" );
    }

    // Contributors
    {
      txt += hmask.arg ( tr ( "Contributors" ) );
      auto add = [ &txt, &pdivmask ] ( const QString & name_n,
                                       const QString & email_n ) {
        txt += pdivmask.arg ( name_n, email_n );
      };
      add ( "Jose Lencioni", "elcorreodelcoco@gmail.com" );
      add ( "Ivan Sorokin", "vanyacpp@gmail.com" );
      add ( "Fernando Auil", "auil@usp.br" );
    }

    // Translators
    {
      txt += hmask.arg ( tr ( "Translators" ) );
      auto add = [ &txt, &trmask ] ( const QString & name_n,
                                     const QString & email_n,
                                     const QString & lang_n ) {
        txt += trmask.arg ( name_n, email_n, lang_n );
      };
      add ( "Pavel Fric", "pavelfric@seznam.cz", "cs" );
      add ( "Sebastian Holtermann", "sebholt@web.de", "de" );
      add ( "Jose Lencioni", "elcorreodelcoco@gmail.com", "es" );
      add ( "Ivan Sorokin", "vanyacpp@gmail.com", "ru" );
    }

    _txt_people = new Wdg::Text_Browser;
    _txt_people->setAlignment ( Qt::AlignLeft | Qt::AlignTop );
    _txt_people->setWordWrapMode ( QTextOption::NoWrap );
    _txt_people->setOpenLinks ( true );
    _txt_people->setOpenExternalLinks ( true );
    _txt_people->setHtml ( txt );
  }

  // License text
  {
    QString txt;
    {
      // List of license files
      QStringList files;
      files.append ( QString::fromUtf8 ( PACKAGE_LICENSE_FILE, -1 ) );
      files.append ( QString::fromUtf8 ( PACKAGE_LICENSE_FILE_EXTERN, -1 ) );
      // Read the first available license file
      bool success = false;
      for ( const QString & fname : files ) {
        if ( read_utf8_file ( fname, txt ) ) {
          success = true;
          break;
        }
      }
      // Set fallback text on demand
      if ( !success ) {
        txt = tr ( "The license file %1 is not available." );
        txt = txt.arg ( files[ 0 ] );
      }
    }

    _txt_license = new Wdg::Text_Browser;
    //_txt_license->setFrameStyle ( QFrame::NoFrame );
    _txt_license->setAlignment ( Qt::AlignLeft | Qt::AlignTop );
    _txt_license->setWordWrapMode ( QTextOption::NoWrap );
    _txt_license->setOpenLinks ( true );
    _txt_license->setOpenExternalLinks ( true );
    {
      QFont fnt ( _txt_license->document ()->defaultFont () );
      fnt.setFamily ( "courier" );
      fnt.setFixedPitch ( true );
      _txt_license->document ()->setDefaultFont ( fnt );
    }
    _txt_license->setPlainText ( txt );
  }

  add_pages_begin ();
  add_page ( tr ( "Information" ), _txt_info );
  add_page ( tr ( "People" ), _txt_people );
  add_page ( tr ( "License" ), _txt_license );
  add_pages_end ();

  set_current_page_idx ( 0 );
}

Info_Dialog::~Info_Dialog () = default;

bool
Info_Dialog::read_utf8_file ( const QString & filename_n,
                              QString & txt_n ) const
{
  bool res = false;
  {
    QFile file ( filename_n );
    file.open ( QIODevice::ReadOnly );
    if ( file.isOpen () ) {
      QByteArray ba = file.readAll ();
      txt_n = QString::fromUtf8 ( ba.data (), ba.size () );
      file.close ();
      res = true;
    }
  }
  return res;
}

} // namespace Views
