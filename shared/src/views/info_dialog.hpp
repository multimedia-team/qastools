/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "views/multi_page_dialog.hpp"

namespace Views
{

/// @brief Info_Dialog
///
class Info_Dialog : public Views::Multi_Page_Dialog
{
  Q_OBJECT

  public:
  // -- Static utility

  static QAction *
  create_action_about ( QObject * parent_n );

  static QAction *
  create_action_about_qt ( QObject * parent_n );

  // -- Construction

  Info_Dialog ( QWidget * parent_n = nullptr,
                Qt::WindowFlags flags_n = Qt::WindowFlags () );

  ~Info_Dialog ();

  protected:
  // -- Protected methods

  bool
  read_utf8_file ( const QString & filename_n, QString & txt_n ) const;
};

} // namespace Views
