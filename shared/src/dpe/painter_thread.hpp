/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QThread>

// Forward declaration
namespace dpe
{
class Painter_Thread_Shared;
class Paint_Job;
} // namespace dpe

namespace dpe
{

/// @brief Painter_Thread
///
class Painter_Thread : public QThread
{
  public:
  // -- Construction

  Painter_Thread (
      const std::shared_ptr< dpe::Painter_Thread_Shared > & shared_n );

  ~Painter_Thread ();

  protected:
  // -- Protected methods

  void
  run ();

  private:
  // -- Attributes
  std::shared_ptr< dpe::Painter_Thread_Shared > _shared;
};

} // namespace dpe
