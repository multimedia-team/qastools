/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QMutex>
#include <QWaitCondition>
#include <cstddef>

namespace dpe
{

/// @brief Image_Set_State
///
class Image_Set_State
{
  public:
  // -- Construction

  Image_Set_State ();

  ~Image_Set_State ();

  void
  wait_for_finish ();

  void
  init_todo ( std::size_t num_n );

  /// @brief To be called by render threads ( thread safe )
  ///
  /// This method is called by render threads after finishing an
  /// image paint.
  ///
  /// @return True if there's more to do
  bool
  one_done ();

  private:
  // -- Attributes
  QMutex _mutex;
  QWaitCondition _cond_finish;
  std::size_t _num_todo = 0;
};

} // namespace dpe
