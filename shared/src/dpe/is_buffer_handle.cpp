/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "is_buffer_handle.hpp"
#include "image_set.hpp"
#include "image_set_meta.hpp"
#include "image_set_state.hpp"
#include <cassert>

namespace dpe
{

IS_Buffer_Handle::IS_Buffer_Handle ()
: state ( std::make_unique< dpe::Image_Set_State > () )
{
}

IS_Buffer_Handle::~IS_Buffer_Handle ()
{
  assert ( num_users == 0 );
}

} // namespace dpe
