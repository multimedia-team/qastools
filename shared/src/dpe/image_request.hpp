/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <vector>

// Forward declaration
namespace dpe
{
class Image_Set_Meta;
class Image_Set_Group;
class Image_Set_State;
} // namespace dpe

namespace dpe
{

/// @brief Image_Request
///
class Image_Request
{
  public:
  // -- Construction

  Image_Request ( dpe::Image_Set_Group * group_n );

  ~Image_Request ();

  /// @brief To be called by the requesting thread
  ///
  /// This method must be called at some point after
  /// sending this request to an Image_Allocator.
  void
  wait_for_finish ();

  public:
  // -- Attributes
  dpe::Image_Set_Group * group;
  std::vector< dpe::Image_Set_Meta * > meta;
  std::vector< dpe::Image_Set_State * > states;
};

} // namespace dpe
