/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <cstddef>

// Forward declaration
namespace dpe
{
class Image_Request;
class Image_Set;
class Image_Set_Meta;
class Image_Set_State;
} // namespace dpe

namespace dpe
{

/// @brief Paint_Job
///
class Paint_Job
{
  public:
  // -- Construction

  Paint_Job ();

  ~Paint_Job () = default;

  public:
  // -- Attributes
  dpe::Image_Set_Meta * meta = nullptr;
  dpe::Image_Set * img_set = nullptr;
  std::size_t img_idx = 0;
  dpe::Image_Set_State * state = nullptr;
};

} // namespace dpe
