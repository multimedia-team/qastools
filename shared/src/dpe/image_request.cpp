/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "image_request.hpp"
#include "image_set_group.hpp"
#include "image_set_meta.hpp"
#include "image_set_state.hpp"

namespace dpe
{

Image_Request::Image_Request ( dpe::Image_Set_Group * group_n )
: group ( group_n )
, meta ( group_n->img_sets.size (), nullptr )
, states ( group_n->img_sets.size (), nullptr )
{
}

Image_Request::~Image_Request () = default;

void
Image_Request::wait_for_finish ()
{
  for ( auto & state : states ) {
    if ( state != nullptr ) {
      state->wait_for_finish ();
    }
  }
}

} // namespace dpe
