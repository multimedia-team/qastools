/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

// Forward declaration
namespace dpe
{
class Image;
class Image_Set;
class Image_Set_Meta;
class Paint_Job;
} // namespace dpe

namespace dpe
{

/// @brief Painter
///
class Painter
{
  public:
  // -- Construction

  Painter ( unsigned int group_type_n, unsigned int group_variant_n = 0 );

  virtual ~Painter ();

  // -- Group

  unsigned int
  group_type () const
  {
    return _group_type;
  }

  unsigned int
  group_variant () const
  {
    return _group_variant;
  }

  void
  set_group_variant ( unsigned int variant_n );

  int
  check_and_paint ( dpe::Paint_Job * pjob_n );

  protected:
  // -- Protected methods

  /// @brief Allocate the image an fill it with zeros
  int
  create_image_data ( dpe::Image * img_n, const dpe::Image_Set_Meta * meta_n );

  /// @brief Single image painting method
  ///
  /// @return 0 on success ( no error )
  virtual int
  paint_image ( dpe::Paint_Job * pjob_n ) = 0;

  private:
  // -- Attributes
  /// @brief Type of the group this painter knows how to paint
  unsigned int _group_type;
  /// @brief Group variant id
  unsigned int _group_variant;
};

} // namespace dpe
