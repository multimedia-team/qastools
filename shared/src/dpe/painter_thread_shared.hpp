/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QMutex>
#include <QWaitCondition>
#include <deque>
#include <memory>
#include <vector>

// Forward declaration
namespace dpe
{
class Paint_Job;
class Painter;
class Image_Set_Meta;
} // namespace dpe

namespace dpe
{

/// @brief Painter_Thread_Shared
///
class Painter_Thread_Shared
{
  public:
  // -- Construction

  Painter_Thread_Shared ();

  ~Painter_Thread_Shared ();

  // -- Public interface

  void
  install_painter ( const std::shared_ptr< dpe::Painter > & painter_n );

  void
  enqueue_job ( std::unique_ptr< dpe::Paint_Job > pjob_n );

  bool
  is_idle () const;

  /// @brief Sends abort signals to the given number of threads
  void
  abort_threads ( std::size_t num_n );

  // -- Painter thread interface

  /// @return false if there are no more jobs to process
  bool
  process_job ();

  private:
  // -- Attributes
  /// @brief Fallback painter
  std::shared_ptr< dpe::Painter > _painter_def;
  /// @brief Painters
  std::vector< std::shared_ptr< dpe::Painter > > _painters;

  mutable QMutex _queue_mutex;
  std::deque< std::unique_ptr< dpe::Paint_Job > > _queue;
  QWaitCondition _queue_cond;
  std::size_t _jobs_rendering = 0;
};

} // namespace dpe
