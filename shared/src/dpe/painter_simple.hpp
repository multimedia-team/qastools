/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "painter.hpp"

namespace dpe
{

/// @brief Painter_Simple
///
class Painter_Simple : public dpe::Painter
{
  public:
  // -- Construction

  Painter_Simple ();

  ~Painter_Simple ();

  protected:
  // -- Paint interface

  int
  paint_image ( dpe::Paint_Job * pjob_n ) override;
};

} // namespace dpe
