/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "image_set_state.hpp"

namespace dpe
{

Image_Set_State::Image_Set_State () = default;

Image_Set_State::~Image_Set_State () = default;

void
Image_Set_State::wait_for_finish ()
{
  _mutex.lock ();
  while ( _num_todo != 0 ) {
    _cond_finish.wait ( &_mutex );
  }
  _mutex.unlock ();
}

void
Image_Set_State::init_todo ( std::size_t num_n )
{
  _mutex.lock ();
  _num_todo = num_n;
  _mutex.unlock ();
}

bool
Image_Set_State::one_done ()
{
  bool finished = false;
  {
    _mutex.lock ();
    --_num_todo;
    finished = ( _num_todo == 0 );
    _mutex.unlock ();
  }
  if ( finished ) {
    _cond_finish.wakeAll ();
    return false;
  }
  return true;
}

} // namespace dpe
