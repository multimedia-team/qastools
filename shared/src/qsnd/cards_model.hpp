/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/card_info.hpp"
#include <QAbstractListModel>
#include <memory>
#include <vector>

namespace QSnd
{

// Forward declaration
class Cards_Db;

/// @brief Cards_Model
///
class Cards_Model : public QAbstractListModel
{
  Q_OBJECT;

  public:
  // -- Types

  using Card_Handle = std::shared_ptr< const QSnd::Card_Info >;
  using Cards = std::vector< Card_Handle >;

  enum Extra_Roles
  {
    ROLE_INDEX = ( Qt::UserRole + 1 ),
    ROLE_ID = ( Qt::UserRole + 2 ),
    ROLE_DRIVER = ( Qt::UserRole + 3 ),
    ROLE_NAME = ( Qt::UserRole + 4 ),
    ROLE_LONG_NAME = ( Qt::UserRole + 5 ),
    ROLE_MIXER_NAME = ( Qt::UserRole + 6 ),
    ROLE_COMPONENTS = ( Qt::UserRole + 7 )
  };

  // -- Properties

  Q_PROPERTY ( int count READ count NOTIFY countChanged )

  // -- Construction

  Cards_Model ( QSnd::Cards_Db * cards_db_n, QObject * parent_n = nullptr );

  ~Cards_Model ();

  // -- Cards database

  QSnd::Cards_Db *
  cards_db () const
  {
    return _cards_db;
  }

  // -- Card info accessors

  int
  count () const
  {
    return _cards.size ();
  }

  Q_SIGNAL
  void
  countChanged ();

  std::size_t
  num_cards () const
  {
    return _cards.size ();
  }

  const Card_Handle &
  card_info ( std::size_t index_n ) const
  {
    return _cards[ index_n ];
  }

  Card_Handle
  card_info_by_model_index ( const QModelIndex & idx_n ) const;

  QModelIndex
  model_index_by_card_id ( const QString & id_str_n ) const;

  // -- Model interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role_n = Qt::DisplayRole ) const override;

  private:
  // -- Private slots

  Q_SLOT
  void
  on_card_new ( std::shared_ptr< const QSnd::Card_Info > handle_n );

  Q_SLOT
  void
  on_card_removed ( std::shared_ptr< const QSnd::Card_Info > handle_n );

  private:
  // -- Attributes
  QSnd::Cards_Db * _cards_db = nullptr;
  Cards _cards;
};

} // namespace QSnd
