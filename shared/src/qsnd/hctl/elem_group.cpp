/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "elem_group.hpp"

namespace QSnd::HCtl
{

Elem_Group::Elem_Group () = default;

Elem_Group::~Elem_Group () = default;

void
Elem_Group::clear ()
{
  _snd_elems.clear ();
}

} // namespace QSnd::HCtl
