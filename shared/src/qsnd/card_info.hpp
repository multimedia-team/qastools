/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>

namespace QSnd
{

/// @brief Card_Info
///
class Card_Info
{
  public:
  // -- Construction

  Card_Info ();

  Card_Info ( const int card_idx_n );

  Card_Info ( const QString & dev_str_n );

  ~Card_Info ();

  // -- Setup

  void
  clear ();

  /// @return true on success
  bool
  acquire_info ( const int card_idx_n );

  /// @return true on success
  bool
  acquire_info ( const QString & dev_str_n );

  // -- Accessors

  bool
  is_valid () const
  {
    return _is_valid;
  }

  int
  index () const
  {
    return _index;
  }

  const QString &
  id () const
  {
    return _id;
  }

  const QString &
  driver () const
  {
    return _driver;
  }

  const QString &
  name () const
  {
    return _name;
  }

  const QString &
  long_name () const
  {
    return _long_name;
  }

  const QString &
  mixer_name () const
  {
    return _mixer_name;
  }

  const QString &
  components () const
  {
    return _components;
  }

  // -- Comparison operators

  bool
  operator== ( const QSnd::Card_Info & cinfo_n ) const;

  bool
  operator!= ( const QSnd::Card_Info & cinfo_n ) const;

  private:
  // -- Attributes
  bool _is_valid = false;
  int _index = -1;
  QString _id;
  QString _driver;
  QString _name;
  QString _long_name;
  QString _mixer_name;
  QString _components;
};

} // namespace QSnd
