/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <libudev.h>
#include <QObject>
#include <string>

namespace QSnd
{

/// @brief UDev_Device_Lookout
///
class UDev_Device_Lookout : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  UDev_Device_Lookout ( QObject * parent_n = nullptr );

  ~UDev_Device_Lookout ();

  // -- Signals

  Q_SIGNAL
  void
  sig_change ();

  // -- Protected slots
  protected:
  Q_SLOT
  void
  udev_process ();

  private:
  // -- Attributes
  // udev device detection
  ::udev * _udev = nullptr;
  ::udev_monitor * _udev_mon = nullptr;
};

} // namespace QSnd
