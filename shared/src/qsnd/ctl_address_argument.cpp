/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "ctl_address_argument.hpp"

namespace QSnd
{

Ctl_Address_Argument::Ctl_Address_Argument () = default;

Ctl_Address_Argument::Ctl_Address_Argument ( const QString & name_n,
                                             const QString & value_n )
: _name ( name_n )
, _value ( value_n )
{
}

Ctl_Address_Argument::Ctl_Address_Argument (
    const Ctl_Address_Argument & addr_n ) = default;

Ctl_Address_Argument::Ctl_Address_Argument ( Ctl_Address_Argument && addr_n ) =
    default;

Ctl_Address_Argument::~Ctl_Address_Argument () = default;

void
Ctl_Address_Argument::clear ()
{
  _name.clear ();
  _value.clear ();
}

Ctl_Address_Argument &
Ctl_Address_Argument::operator= ( const Ctl_Address_Argument & addr_n ) =
    default;

Ctl_Address_Argument &
Ctl_Address_Argument::operator= ( Ctl_Address_Argument && addr_n ) = default;

} // namespace QSnd
