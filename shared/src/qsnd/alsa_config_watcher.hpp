/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QFileSystemWatcher>
#include <QObject>
#include <QString>

namespace QSnd
{

/// @brief Main_Window
///
class ALSA_Config_Watcher : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  ALSA_Config_Watcher ( QObject * parent_n = nullptr );

  ~ALSA_Config_Watcher ();

  // -- Signals

  Q_SIGNAL
  void
  sig_change ();

  // Private slots
  private:
  Q_SLOT
  void
  change_in_file ( const QString & fl_n );

  Q_SLOT
  void
  change_in_dir ( const QString & fl_n );

  private:
  // -- Attributes
  QFileSystemWatcher _fwatch;
  QString _cfg_system_dir;
  QString _cfg_system_file;
  QString _cfg_home_dir;
  QString _cfg_home_file;
};

} // namespace QSnd
