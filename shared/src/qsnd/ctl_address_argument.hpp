/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>

namespace QSnd
{

/// @brief Ctl_Address_Argument
///
class Ctl_Address_Argument
{
  public:
  // -- Construction

  Ctl_Address_Argument ();

  Ctl_Address_Argument ( const QString & name_n,
                         const QString & value_n = QString () );

  Ctl_Address_Argument ( const Ctl_Address_Argument & addr_n );

  Ctl_Address_Argument ( Ctl_Address_Argument && addr_n );

  ~Ctl_Address_Argument ();

  // -- Setup

  void
  clear ();

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  void
  set_name ( const QString & name_n )
  {
    _name = name_n;
  }

  // -- Value

  const QString &
  value () const
  {
    return _value;
  }

  void
  set_value ( const QString & value_n )
  {
    _value = value_n;
  }

  // -- Assignment operators

  Ctl_Address_Argument &
  operator= ( const Ctl_Address_Argument & addr_n );

  Ctl_Address_Argument &
  operator= ( Ctl_Address_Argument && addr_n );

  private:
  // -- Attributes
  QString _name;
  QString _value;
};

} // namespace QSnd
