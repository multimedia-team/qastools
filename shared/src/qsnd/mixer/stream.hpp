/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <cstddef>

namespace QSnd::Mixer
{

/// @brief Stream direction indices
enum Stream_Dir
{
  SD_PLAYBACK = 0,
  SD_CAPTURE = 1
};

/// @brief Number of stream directions
static constexpr std::size_t SD_COUNT = 2;

} // namespace QSnd::Mixer
