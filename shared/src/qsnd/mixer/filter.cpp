/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "filter.hpp"

namespace QSnd::Mixer
{

Filter::Filter () = default;

Filter::~Filter () = default;

} // namespace QSnd::Mixer
