/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/mixer/filter.hpp"
#include <QString>
#include <vector>

namespace QSnd::Mixer::Filters
{

/// @brief Name filter
///
/// Filters mixer elements from a list by their name
///
class Name : public Filter
{
  public:
  // -- Construction

  Name ( bool blacklist_n = true );

  ~Name () override;

  // -- Blacklist

  /// @brief true if this is a blacklist. false if it is a whitelist.
  bool
  blacklist () const
  {
    return _blacklist;
  }

  void
  set_blacklist ( bool flag_n );

  // -- Names

  const std::vector< QString > &
  names () const
  {
    return _names;
  }

  void
  append_name ( const QString & name_n );

  void
  append_names ( const std::vector< QString > & names_n );

  // -- Filter

  void
  filter ( Element_Vector & accept_n, Element_Vector & drop_n ) override;

  private:
  // -- Attributes
  std::vector< QString > _names;
  bool _blacklist = true;
};

} // namespace QSnd::Mixer::Filters
