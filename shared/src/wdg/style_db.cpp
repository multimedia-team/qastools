/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "style_db.hpp"

namespace Wdg
{

Style_Db::Style_Db ( QObject * parent_n )
: QObject ( parent_n )
{
  // Insert default palette
  QPalette pal;
  _palettes.insert ( STYLE_DEFAULT, pal );
}

Style_Db::~Style_Db () = default;

const QPalette &
Style_Db::palette_default () const
{
  return _palettes.find ( STYLE_DEFAULT ).value ();
}

const QPalette &
Style_Db::palette ( unsigned int style_id_n ) const
{
  auto it = _palettes.find ( style_id_n );
  if ( it != _palettes.end () ) {
    return it.value ();
  }
  return _palettes.find ( STYLE_DEFAULT ).value ();
}

void
Style_Db::insert_palette ( unsigned int style_id_n, const QPalette & pal_n )
{
  auto it = _palettes.find ( style_id_n );
  if ( it != _palettes.end () ) {
    // Check if there's a palette change
    if ( it.value () == pal_n ) {
      return;
    }
  }
  // Insert an notify
  _palettes.insert ( style_id_n, pal_n );
  Q_EMIT sig_palette_changed ( style_id_n );
}

} // namespace Wdg
