/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QScrollArea>

namespace Wdg
{

/// @brief Scroll_Area_Vertical
///
class Scroll_Area_Vertical : public QScrollArea
{
  public:
  // -- Construction

  Scroll_Area_Vertical ( QWidget * parent_n = nullptr );

  ~Scroll_Area_Vertical ();

  // -- Size hint

  QSize
  minimumSizeHint () const override;

  // -- Widget

  void
  set_widget ( QWidget * wdg_n );

  QWidget *
  take_widget ();

  protected:
  // -- Protected methods

  bool
  eventFilter ( QObject * watched_n, QEvent * event_n ) override;
};

} // namespace Wdg
