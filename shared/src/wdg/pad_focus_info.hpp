/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

namespace Wdg
{

/// @brief Specifies which element of a slider/switches pad has the focus
///
class Pad_Focus_Info
{
  public:
  // -- Construction

  Pad_Focus_Info ();

  ~Pad_Focus_Info ();

  void
  clear ();

  // Public attributes

  bool has_focus;
  unsigned int group_idx;
  unsigned int column_idx;
  unsigned int row_idx;
};

} // namespace Wdg
