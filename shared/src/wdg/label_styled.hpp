/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>
#include <QWidget>

// Forward declaration
namespace Wdg
{
class Style_Db;
}

namespace Wdg
{

/// @brief Label that selects the text color from a style database
///
class Label_Styled : public QWidget
{
  public:
  // -- Construction

  Label_Styled ( const Wdg::Style_Db * style_db_n,
                 QWidget * parent_n = nullptr );

  ~Label_Styled ();

  // -- Style database

  const Wdg::Style_Db *
  style_db () const
  {
    return _style_db;
  }

  // -- Style id

  unsigned int
  style_id () const
  {
    return _style_id;
  }

  void
  set_style_id ( unsigned int id_n );

  // -- Text

  const QString &
  text () const
  {
    return _text;
  }

  void
  setText ( const QString txt_n );

  // -- Alignment

  Qt::Alignment
  alignment () const
  {
    return _alignment;
  }

  void
  setAlignment ( Qt::Alignment align_n );

  // -- Size hints

  QSize
  minimumSizeHint () const override;

  QSize
  sizeHint () const override;

  protected:
  // -- Protected methods

  Q_SLOT
  void
  style_palette_changed ( unsigned int style_id_n );

  void
  resizeEvent ( QResizeEvent * event_n ) override;

  void
  paintEvent ( QPaintEvent * event_n ) override;

  private:
  // -- Attributes
  const Wdg::Style_Db * _style_db = nullptr;
  unsigned int _style_id = 0;
  QString _text;
  Qt::Alignment _alignment = ( Qt::AlignLeft | Qt::AlignVCenter );
};

} // namespace Wdg
