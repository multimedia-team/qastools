/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QScrollBar>
#include <QString>
#include <QTextBrowser>
#include <QTextFrame>

namespace Wdg
{

/// @brief Text_Browser
///
class Text_Browser : public QTextBrowser
{
  public:
  // -- Construction

  Text_Browser ( QWidget * parent = nullptr );

  ~Text_Browser ();

  // -- Content

  void
  setPlainText ( const QString & txt_n );

  void
  setHtml ( const QString & txt_n );

  // -- Size hint

  QSize
  sizeHint () const;

  protected:
  // -- Protected methods

  void
  update_size_hint ();

  private:
  // -- Attributes
  int _hint_width = 0;
  int _hint_height = 0;
};

} // namespace Wdg
