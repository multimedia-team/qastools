/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/image_allocator.hpp"
#include "dpe/image_request.hpp"
#include "dpe/image_set_group.hpp"
#include <memory>

// Forward declaration
class QWidget;

namespace Wdg::DS
{

/// @brief Imaging
///
class Imaging
{
  public:
  // -- Construction

  Imaging ( std::size_t num_image_groups_n,
            dpe::Image_Allocator * alloc_n = nullptr );

  ~Imaging ();

  // -- Image allocator

  dpe::Image_Allocator *
  image_alloc () const
  {
    return _image_alloc;
  }

  void
  set_image_alloc ( dpe::Image_Allocator * alloc_n );

  // -- Images

  dpe::Image_Set_Group &
  images ()
  {
    return _images;
  }

  // -- Image request

  dpe::Image_Request *
  image_request () const
  {
    return _image_request.get ();
  }

  void
  send_request ()
  {
    _request_sent = true;
    _image_alloc->send_request ( image_request () );
  }

  void
  wait_for_request ()
  {
    if ( _request_sent ) {
      _request_sent = false;
      image_request ()->wait_for_finish ();
    }
  }

  // -- Meta data setting

  void
  set_images_meta_variant_id ( unsigned int variant_n );

  void
  set_images_meta_style_id ( unsigned int style_n );

  void
  set_images_meta_style_sub_id ( unsigned int style_n );

  static unsigned int
  style_sub_id ( QWidget * wdg_n );

  void
  set_images_meta_palette ( const QPalette & pal_n );

  private:
  // -- Attributes
  dpe::Image_Set_Group _images;
  dpe::Image_Allocator * _image_alloc = nullptr;
  std::unique_ptr< dpe::Image_Request > _image_request;
  bool _request_sent = false;
};

} // namespace Wdg::DS
