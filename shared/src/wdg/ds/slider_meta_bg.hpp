/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/image_set_meta.hpp"

namespace Wdg::DS
{

/// @brief Slider_Meta_Bg
///
class Slider_Meta_Bg : public dpe::Image_Set_Meta
{
  public:
  // -- Construction

  Slider_Meta_Bg ( unsigned int num_images_n,
                   unsigned int group_type_n,
                   unsigned int type_id_n );

  Slider_Meta_Bg ( const Slider_Meta_Bg & meta_n );

  ~Slider_Meta_Bg ();

  // -- Image_Set_Meta interface

  bool
  matches ( const dpe::Image_Set_Meta * meta_n ) const;

  dpe::Image_Set_Meta *
  new_copy () const;

  // -- Attributes
  unsigned int ticks_max_idx = 0;
  int ticks_range_max_idx = 0;
  int ticks_range_start = 0;
  // Style
  int bg_show_image = 0;
  unsigned int bg_tick_min_idx = 0; // Which tick has the minimum width
};

} // namespace Wdg::DS
