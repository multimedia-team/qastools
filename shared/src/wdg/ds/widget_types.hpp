/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QPalette>

namespace Wdg::DS
{

enum Widget_Type
{
  WT_UNKNOWN = 0,
  WT_SWITCH = 1,
  WT_SLIDER = 2
};

enum Widget_State
{
  ST_NORMAL = 0,
  ST_DISABLED = 1,
  ST_INACTIVE = 2
};

enum Switch_Variants
{
  SV_CIRCLE = 0,
  SV_SVG_JOINED = 1
};

inline QPalette::ColorGroup
color_group ( Wdg::DS::Widget_State state_n )
{
  QPalette::ColorGroup res;
  switch ( state_n ) {
  case Wdg::DS::ST_DISABLED:
    res = QPalette::Disabled;
    break;
  case Wdg::DS::ST_INACTIVE:
    res = QPalette::Inactive;
    break;
  default:
    res = QPalette::Normal;
  }
  return res;
}

inline QPalette::ColorGroup
color_group ( unsigned int state_n )
{
  return color_group ( static_cast< Wdg::DS::Widget_State > ( state_n ) );
}

} // namespace Wdg::DS
