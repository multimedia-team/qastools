/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/painter.hpp"

namespace Wdg::DS::Painter
{

/// @brief Switch_Circle
///
class Switch_Circle : public dpe::Painter
{
  public:
  // -- Construction

  Switch_Circle ();

  ~Switch_Circle ();

  protected:
  // -- Protected methods

  int
  paint_image ( dpe::Paint_Job * pjob_n );

  private:
  // -- Private methods

  // Declaration
  struct PData;

  // Background

  int
  paint_bg ( dpe::Paint_Job * pjob_n, PData & pd );

  void
  paint_bg_area ( PData & pd );

  void
  paint_bg_deco ( PData & pd );

  void
  paint_bg_border ( PData & pd );

  // Handle

  int
  paint_handle ( dpe::Paint_Job * pjob_n, PData & pd );

  void
  paint_handle_area ( PData & pd );

  void
  paint_handle_deco ( PData & pd );

  // Shared

  void
  paint_highlight ( PData & pd );

  void
  paint_focus_path ( PData & pd );
};

} // namespace Wdg::DS::Painter
