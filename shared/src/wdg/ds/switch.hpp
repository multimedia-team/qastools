/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/image_set_meta.hpp"
#include "wdg/ds/imaging.hpp"
#include <QAbstractButton>

// Forward declaration
namespace Wdg
{
class Style_Db;
}

namespace Wdg::DS
{

/// @brief Dynamic sized switch
///
/// Background images:
/// 0 - idle
/// 1 - focus
/// 2 - hover idle
/// 3 - hover focus
///
/// Handle images:
/// 0 - unchecked idle
/// 1 - unchecked focus
/// 2 - unchecked hover
/// 3 - unchecked hover & focus
///
/// 4 - checked idle
/// 5 - checked focus
/// 6 - checked hover
/// 7 - checked hover & focus
///
/// 8 - half checked focus
/// 9 - half checked hover & focus
///
/// It is possible to use this switch widget as a push button
/// by setting setCheckable ( false ).
/// Push buttons don't require the images 4-7.
///
class Switch : public QAbstractButton
{
  Q_OBJECT

  public:
  // -- Construction

  Switch ( const Wdg::Style_Db * style_db_n,
           dpe::Image_Allocator * image_alloc_n,
           QWidget * parent_n = nullptr );

  ~Switch ();

  // -- Size hints

  QSize
  sizeHint () const override;

  QSize
  minimumSizeHint () const override;

  // -- Image allocator

  dpe::Image_Allocator *
  image_alloc () const
  {
    return _dsi.image_alloc ();
  }

  // -- Style database

  const Wdg::Style_Db *
  style_db () const
  {
    return _style_db;
  }

  // -- Style id

  unsigned int
  style_id () const
  {
    return _meta_bg.style_id;
  }

  void
  set_style_id ( unsigned int id_n );

  // -- Variant id

  void
  set_variant_id ( unsigned int id_n );

  protected:
  // -- Protected methods

  Q_SLOT
  void
  style_palette_changed ( unsigned int style_id_n );

  void
  changeEvent ( QEvent * event_n ) override;

  void
  enterEvent ( QEnterEvent * event_n ) override;

  void
  leaveEvent ( QEvent * event_n ) override;

  void
  resizeEvent ( QResizeEvent * event_n ) override;

  void
  paintEvent ( QPaintEvent * event_n ) override;

  void
  update_palette ();

  void
  update_pixmaps ();

  private:
  // -- Attributes
  static const unsigned int num_images_bg = 4;
  static const unsigned int num_images_handle = 10;

  Wdg::DS::Imaging _dsi;

  QRect _pxmap_rect;
  bool _update_pixmaps_pending;

  dpe::Image_Set_Meta _meta_bg;
  dpe::Image_Set_Meta _meta_handle;
  const Wdg::Style_Db * _style_db = nullptr;
};

} // namespace Wdg::DS
