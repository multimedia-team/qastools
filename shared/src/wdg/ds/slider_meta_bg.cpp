/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "slider_meta_bg.hpp"
#include "wdg/ds/widget_types.hpp"

namespace Wdg::DS
{

Slider_Meta_Bg::Slider_Meta_Bg ( unsigned int num_images_n,
                                 unsigned int group_type_n,
                                 unsigned int type_id_n )
: dpe::Image_Set_Meta ( num_images_n, group_type_n, type_id_n )
{
}

Slider_Meta_Bg::Slider_Meta_Bg ( const Slider_Meta_Bg & meta_n ) = default;

Slider_Meta_Bg::~Slider_Meta_Bg () = default;

bool
Slider_Meta_Bg::matches ( const dpe::Image_Set_Meta * meta_n ) const
{
  bool res = false;

  const Slider_Meta_Bg * meta_c =
      dynamic_cast< const Slider_Meta_Bg * > ( meta_n );
  if ( meta_c != nullptr ) {
    res = dpe::Image_Set_Meta::matches ( meta_n ) &&
          ( meta_c->ticks_max_idx == ticks_max_idx ) &&
          ( meta_c->ticks_range_max_idx == ticks_range_max_idx ) &&
          ( meta_c->ticks_range_start == ticks_range_start ) &&
          ( meta_c->bg_show_image == bg_show_image ) &&
          ( meta_c->bg_tick_min_idx == bg_tick_min_idx );
  }

  return res;
}

dpe::Image_Set_Meta *
Slider_Meta_Bg::new_copy () const
{
  return new Slider_Meta_Bg ( *this );
}

} // namespace Wdg::DS
