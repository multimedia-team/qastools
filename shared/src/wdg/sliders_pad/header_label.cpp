/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "header_label.hpp"

namespace Wdg::Sliders_Pad
{

Header_Label::Header_Label () = default;

Header_Label::~Header_Label () = default;

} // namespace Wdg::Sliders_Pad
