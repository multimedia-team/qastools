/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/sliders_pad/header_label.hpp"
#include <QPointer>
#include <QWidget>
#include <vector>

namespace Wdg::Sliders_Pad
{

/// @brief Header_Data
///
class Header_Data
{
  public:
  // -- Construction

  Header_Data ();

  ~Header_Data ();

  // -- Attributes
  bool update_elided_texts;
  bool update_decoration;
  bool upside_down;
  bool column_labels;
  bool label_sliding;

  std::vector< Header_Label > labels;

  double angle;
  double angle_sin;
  double angle_cos;

  double center_x;
  double center_y;

  unsigned int max_str_length_px;

  unsigned int pad_left;
  unsigned int pad_right;
  unsigned int spacing_inter;
  unsigned int spacing_vertical;

  QPointer< QWidget > widget;
};

} // namespace Wdg::Sliders_Pad
