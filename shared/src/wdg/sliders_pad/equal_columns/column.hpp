/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <memory>
#include <vector>

namespace Wdg::Sliders_Pad::Equal_Columns
{

// Forward declaration
class Row;

/// @brief Column
///
class Column
{
  public:
  // -- Construction

  Column ( unsigned int column_idx_n );

  virtual ~Column ();

  // -- Column index

  unsigned int
  column_index () const
  {
    return _column_index;
  }

  void
  set_column_index ( unsigned int idx_n )
  {
    _column_index = idx_n;
  }

  // -- Active rows

  unsigned int
  active_rows () const
  {
    return _active_rows;
  }

  void
  set_active_rows ( unsigned int num_n )
  {
    _active_rows = num_n;
  }

  // -- Column position

  unsigned int
  column_pos () const
  {
    return _column_pos;
  }

  void
  set_column_pos ( unsigned int pos_n );

  // -- Column width

  unsigned int
  column_width () const
  {
    return _column_width;
  }

  void
  set_column_width ( unsigned int pos_n );

  // -- Rows

  void
  append_row ( Row * item_n );

  void
  remove_empty_rows_at_back ();

  std::size_t
  num_rows () const
  {
    return _rows.size ();
  }

  const Row *
  row ( std::size_t idx_n ) const
  {
    return _rows[ idx_n ].get ();
  }

  Row *
  row ( std::size_t idx_n )
  {
    return _rows[ idx_n ].get ();
  }

  private:
  // -- Attributes
  unsigned int _column_index = 0;
  unsigned int _active_rows = 0;
  unsigned int _column_pos = 0;
  unsigned int _column_width = 0;
  std::vector< std::unique_ptr< Row > > _rows;
};

} // namespace Wdg::Sliders_Pad::Equal_Columns
