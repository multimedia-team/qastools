/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "row.hpp"

namespace Wdg::Sliders_Pad::Equal_Columns
{

Row::Row ( unsigned int row_idx_n )
: _row_index ( row_idx_n )
{
}

Row::~Row () = default;

} // namespace Wdg::Sliders_Pad::Equal_Columns
