/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLayoutItem>
#include <memory>

namespace Wdg::Sliders_Pad::Equal_Columns
{

// Forward declaration
class Group;
class Column;
class Row;

/// @brief Index
///
class Index
{
  public:
  // -- Construction

  Index ();

  Index ( const Index & index_n ) = delete;

  Index ( Index && index_n );

  ~Index ();

  // -- Assignment operators

  Index &
  operator= ( const Index & index_n ) = delete;

  Index &
  operator= ( Index && index_n );

  // -- Attributes
  std::unique_ptr< QLayoutItem > item;
  Group * group = nullptr;
  Column * column = nullptr;
  Row * row = nullptr;
};

} // namespace Wdg::Sliders_Pad::Equal_Columns
