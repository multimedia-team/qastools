/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/sliders_pad/header.hpp"

namespace Wdg::Sliders_Pad
{

/// @brief Footer
///
class Footer : public Header
{
  Q_OBJECT

  public:
  // -- Construction

  Footer ( Data * sp_data_n, Style * sp_style_n, QWidget * parent_n = nullptr );

  ~Footer ();
};

} // namespace Wdg::Sliders_Pad
