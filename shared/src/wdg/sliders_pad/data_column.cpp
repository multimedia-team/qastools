/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "data_column.hpp"
#include "wdg/pad_proxy/column.hpp"
#include "wdg/sliders_pad/data.hpp"
#include "wdg/sliders_pad/data_group.hpp"
#include "wdg/sliders_pad/footer.hpp"
#include <iostream>

namespace Wdg::Sliders_Pad
{

Data_Column::Data_Column ( QObject * parent_n )
: QObject ( parent_n )
{
}

Data_Column::~Data_Column () = default;

Data_Group *
Data_Column::sp_group ()
{
  return static_cast< Data_Group * > ( parent () );
}

Data *
Data_Column::sp_data ()
{
  return sp_group ()->sp_data ();
}

void
Data_Column::update_footer_label ()
{
  if ( show_value_label ) {
    const QString & txt ( sppc->value_string () );
    sp_data ()->footer_cast ()->set_label_text ( col_total_idx, txt );
  }
}

} // namespace Wdg::Sliders_Pad
