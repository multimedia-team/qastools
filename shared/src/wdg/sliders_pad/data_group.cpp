/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "data_group.hpp"
#include "wdg/sliders_pad/data.hpp"
#include "wdg/sliders_pad/data_column.hpp"
#include <iostream>

namespace Wdg::Sliders_Pad
{

Data_Group::Data_Group ( QObject * parent_n )
: QObject ( parent_n )
{
}

Data_Group::~Data_Group () = default;

Data *
Data_Group::sp_data ()
{
  return static_cast< Data * > ( parent () );
}

void
Data_Group::reserve_columns ( std::size_t size_n )
{
  _columns.reserve ( size_n );
}

void
Data_Group::append_column ( Data_Column * column_n )
{
  _columns.emplace_back ( column_n );
}

} // namespace Wdg::Sliders_Pad
