/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "footer.hpp"
#include "wdg/sliders_pad/data.hpp"

namespace Wdg::Sliders_Pad
{

Footer::Footer ( Data * sp_data_n, Style * sp_style_n, QWidget * parent_n )
: Header ( sp_data_n, sp_style_n, parent_n )
{
  hd_data ().upside_down = true;
  hd_data ().column_labels = true;
  hd_data ().label_sliding = false;

  update_painter_states ();
}

Footer::~Footer () = default;

} // namespace Wdg::Sliders_Pad
