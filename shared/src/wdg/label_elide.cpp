/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "label_elide.hpp"
#include <QPainter>

namespace Wdg
{

Label_Elide::Label_Elide ( QWidget * parent_n )
: QWidget ( parent_n )
, _alignment ( Qt::AlignLeft | Qt::AlignVCenter )
, _elide_mode ( Qt::ElideMiddle )
{
}

Label_Elide::~Label_Elide () = default;

QSize
Label_Elide::minimumSizeHint () const
{
  QSize res ( 0, fontMetrics ().height () );
  {
    const QMargins mgs = contentsMargins ();
    res.rwidth () += mgs.left ();
    res.rwidth () += mgs.right ();
    res.rheight () += mgs.top ();
    res.rheight () += mgs.bottom ();
  }
  return res;
}

QSize
Label_Elide::sizeHint () const
{
  // The offset is to fix a too small bounding rect.
  const int offset = 2;
  QSize res ( fontMetrics ().boundingRect ( text () ).width () + offset,
              fontMetrics ().height () );
  {
    const QMargins mgs = contentsMargins ();
    res.rwidth () += mgs.left ();
    res.rwidth () += mgs.right ();
    res.rheight () += mgs.top ();
    res.rheight () += mgs.bottom ();
  }
  return res;
}

void
Label_Elide::setText ( const QString txt_n )
{
  if ( txt_n != _text ) {
    _text = txt_n;
    update_text_elided ();
    updateGeometry ();
    update ();
  }
}

void
Label_Elide::setAlignment ( Qt::Alignment align_n )
{
  if ( align_n != _alignment ) {
    _alignment = align_n;
    update_text_elided ();
    update ();
  }
}

void
Label_Elide::setElideMode ( Qt::TextElideMode mode_n )
{
  if ( mode_n != _elide_mode ) {
    _elide_mode = mode_n;
    update_text_elided ();
    update ();
  }
}

void
Label_Elide::update_text_elided ()
{
  const QMargins mgs = contentsMargins ();
  _text_elided = fontMetrics ().elidedText (
      _text, _elide_mode, qMax ( width () - mgs.left () - mgs.right (), 0 ) );
}

void
Label_Elide::resizeEvent ( QResizeEvent * event_n )
{
  QWidget::resizeEvent ( event_n );
  update_text_elided ();
  update ();
}

void
Label_Elide::paintEvent ( QPaintEvent * event_n )
{
  QWidget::paintEvent ( event_n );
  {
    QPainter painter ( this );
    painter.setRenderHints ( QPainter::Antialiasing |
                             QPainter::TextAntialiasing |
                             QPainter::SmoothPixmapTransform );
    painter.drawText ( contentsRect (), _alignment, _text_elided );
  }
}

} // namespace Wdg
