/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "slider.hpp"

namespace Wdg::Pad_Proxy
{

Slider::Slider ( QObject * parent_n )
: Proxy ( parent_n )
{
}

Slider::~Slider () = default;

void
Slider::set_slider_index ( unsigned long index_n )
{
  if ( index_n > slider_index_max () ) {
    index_n = slider_index_max ();
  }
  if ( slider_index () != index_n ) {
    _slider_index = index_n;
    this->slider_index_changed ();
    Q_EMIT sig_slider_index_changed ( slider_index () );
  }
}

void
Slider::set_slider_index_max ( unsigned long index_n )
{
  if ( slider_index_max () != index_n ) {
    _slider_index_max = index_n;
    if ( slider_index () > slider_index_max () ) {
      set_slider_index ( slider_index_max () );
    }
    this->slider_index_max_changed ();
    Q_EMIT sig_slider_index_max_changed ( slider_index_max () );
  }
}

void
Slider::slider_index_changed ()
{
  // Dummy implementation
}

void
Slider::slider_index_max_changed ()
{
  // Dummy implementation
}

} // namespace Wdg::Pad_Proxy
