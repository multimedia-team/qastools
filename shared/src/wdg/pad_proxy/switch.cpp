/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "switch.hpp"

namespace Wdg::Pad_Proxy
{

Switch::Switch ( QObject * parent_n )
: Proxy ( parent_n )
{
}

Switch::~Switch () = default;

void
Switch::set_switch_state ( bool state_n )
{
  if ( state_n != _switch_state ) {
    _switch_state = state_n;
    this->switch_state_changed ();
    Q_EMIT sig_switch_state_changed ( switch_state () );
  }
}

void
Switch::toggle_switch_state ()
{
  this->set_switch_state ( !switch_state () );
}

void
Switch::switch_state_changed ()
{
  // Dummy implementation
}

} // namespace Wdg::Pad_Proxy
