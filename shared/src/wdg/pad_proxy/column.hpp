/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <memory>

namespace Wdg::Pad_Proxy
{

// Forward declaration
class Enum;
class Group;
class Slider;
class Switch;

/// @brief Column
///
class Column : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Column ( QObject * parent_n = nullptr );

  virtual ~Column ();

  // -- Group

  Group *
  group () const;

  // -- Column index

  unsigned int
  column_index () const
  {
    return _column_index;
  }

  void
  set_column_index ( unsigned int idx_n );

  // -- Slider proxy

  Slider *
  slider_proxy () const
  {
    return _proxy_slider.get ();
  }

  void
  set_slider_proxy ( Slider * proxy_n );

  // -- Switch proxy

  Switch *
  switch_proxy () const
  {
    return _proxy_switch.get ();
  }

  void
  set_switch_proxy ( Switch * proxy_n );

  // -- Enum proxy

  Enum *
  enum_proxy () const
  {
    return _proxy_enum.get ();
  }

  void
  set_enum_proxy ( Enum * proxy_n );

  // -- Show value string

  void
  set_show_value_string ( bool flag_n );

  bool
  show_value_string () const
  {
    return _show_value_string;
  }

  // -- State info

  bool
  has_slider () const
  {
    return ( _proxy_slider != nullptr );
  }

  bool
  has_switch () const
  {
    return ( _proxy_switch != nullptr );
  }

  bool
  has_enum () const
  {
    return ( _proxy_enum != nullptr );
  }

  // -- Focus

  bool
  has_focus () const
  {
    return _has_focus;
  }

  /// @return true when the focus was set
  bool
  set_focus ( unsigned int row_n );

  // -- Value string

  virtual QString
  value_string () const;

  Q_SIGNAL
  void
  sig_value_string_changed ();

  virtual QString
  value_min_string () const;

  virtual QString
  value_max_string () const;

  // -- Event processing

  bool
  event ( QEvent * event_n ) override;

  protected:
  // -- Protected methods

  virtual void
  slider_proxy_changed ();

  virtual void
  switch_proxy_changed ();

  virtual void
  enum_proxy_changed ();

  virtual void
  show_value_string_changed ();

  private:
  // -- Attributes
  unsigned int _column_index = 0;
  std::unique_ptr< Slider > _proxy_slider;
  std::unique_ptr< Switch > _proxy_switch;
  std::unique_ptr< Enum > _proxy_enum;

  bool _has_focus = false;
  bool _show_value_string = true;
};

} // namespace Wdg::Pad_Proxy
