/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

namespace Wdg::Pad_Proxy
{

/// @brief Style
///
class Style
{
  public:
  // -- Construction

  Style ();

  ~Style ();

  // -- Attributes
  bool slider_has_minimum = false;
  unsigned int slider_minimum_idx = 0;
};

} // namespace Wdg::Pad_Proxy
