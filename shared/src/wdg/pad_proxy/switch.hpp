/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/proxy.hpp"

namespace Wdg::Pad_Proxy
{

/// @brief Switch
///
class Switch : public Proxy
{
  Q_OBJECT;

  public:
  // -- Construction

  Switch ( QObject * parent_n = nullptr );

  ~Switch ();

  // -- Switch state

  bool
  switch_state () const
  {
    return _switch_state;
  }

  Q_SLOT
  void
  set_switch_state ( bool state_n );

  Q_SLOT
  void
  toggle_switch_state ();

  Q_SIGNAL
  void
  sig_switch_state_changed ( bool state_n );

  protected:
  // -- Protected methods

  virtual void
  switch_state_changed ();

  private:
  // -- Attributes
  bool _switch_state = false;
};

} // namespace Wdg::Pad_Proxy
