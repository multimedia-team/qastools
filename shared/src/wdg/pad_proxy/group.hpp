/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <memory>
#include <vector>

namespace Wdg::Pad_Proxy
{

// Forward declaration
class Column;

/// @brief Group
///
class Group : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Group ( QObject * parent_n = nullptr );

  virtual ~Group ();

  // -- Pad

  /// @brief The pad object
  /// Gets set by the pad and shouldn't be set manually
  QObject *
  pad () const
  {
    return _pad;
  }

  void
  set_pad ( QObject * pad_n );

  // -- Group index

  /// @brief The group index
  /// Gets set by the pad and shouldn't be set manually
  unsigned int
  group_index () const
  {
    return _group_index;
  }

  void
  set_group_index ( unsigned int idx_n );

  // -- Group id

  /// @brief Unique group id
  const QString &
  group_id () const
  {
    return _group_id;
  }

  void
  set_group_id ( const QString & id_n );

  // -- Group name

  const QString &
  group_name () const
  {
    return _group_name;
  }

  void
  set_group_name ( const QString & name_n );

  // -- Tool tip

  const QString &
  tool_tip () const
  {
    return _tool_tip;
  }

  void
  set_tool_tip ( const QString & tip_n );

  // -- Style id

  unsigned int
  style_id () const
  {
    return _style_id;
  }

  void
  set_style_id ( unsigned int style_id_n );

  // -- Proxies columns

  std::size_t
  num_columns () const
  {
    return _columns.size ();
  }

  /// @brief Ownership of @param column_n is passed to the group
  void
  append_column ( Column * column_n );

  void
  clear_columns ();

  Column *
  column ( std::size_t idx_n ) const
  {
    return _columns[ idx_n ].get ();
  }

  // -- Statistics

  unsigned int
  num_sliders () const
  {
    return _num_sliders;
  }

  unsigned int
  num_switches () const
  {
    return _num_switches;
  }

  // -- Focus

  unsigned char
  focus_column () const
  {
    return _focus_column;
  }

  unsigned char
  focus_row () const
  {
    return _focus_row;
  }

  bool
  has_focus () const
  {
    return _has_focus;
  }

  /// @return true when the focus was set
  bool
  set_focus ( unsigned int column_n, unsigned int row_n );

  protected:
  // -- Protected methods

  bool
  event ( QEvent * event_n ) override;

  private:
  // -- Attributes
  QObject * _pad = nullptr;
  unsigned int _group_index = 0;
  unsigned int _style_id = 0;

  QString _group_id;
  QString _group_name;
  QString _tool_tip;

  std::vector< std::unique_ptr< Column > > _columns;

  unsigned int _num_sliders = 0;
  unsigned int _num_switches = 0;
  unsigned char _focus_column = 0;
  unsigned char _focus_row = 0;
  bool _has_focus = false;
};

} // namespace Wdg::Pad_Proxy
