/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "column.hpp"
#include "wdg/event_types.hpp"
#include "wdg/pad_proxy/enum.hpp"
#include "wdg/pad_proxy/group.hpp"
#include "wdg/pad_proxy/slider.hpp"
#include "wdg/pad_proxy/switch.hpp"
#include "wdg/pass_events.hpp"
#include <QCoreApplication>
#include <QFocusEvent>
#include <iostream>

namespace Wdg::Pad_Proxy
{

Column::Column ( QObject * parent_n )
: QObject ( parent_n )
{
}

Column::~Column () = default;

Group *
Column::group () const
{
  Group * res = nullptr;
  QObject * par = parent ();
  if ( par != nullptr ) {
    res = dynamic_cast< Group * > ( par );
  }
  return res;
}

void
Column::set_column_index ( unsigned int idx_n )
{
  _column_index = idx_n;
}

void
Column::set_slider_proxy ( Slider * proxy_n )
{
  if ( _proxy_slider.get () != proxy_n ) {
    _proxy_slider.reset ( proxy_n );
    if ( _proxy_slider ) {
      _proxy_slider->setParent ( this );
      _proxy_slider->set_index ( 0 );
    }
    this->slider_proxy_changed ();
  }
}

void
Column::set_switch_proxy ( Switch * proxy_n )
{
  if ( _proxy_switch.get () != proxy_n ) {
    _proxy_switch.reset ( proxy_n );
    if ( _proxy_switch ) {
      _proxy_switch->setParent ( this );
      _proxy_switch->set_index ( 1 );
    }
    this->switch_proxy_changed ();
  }
}

void
Column::set_enum_proxy ( Enum * proxy_n )
{
  if ( _proxy_enum.get () != proxy_n ) {
    _proxy_enum.reset ( proxy_n );
    if ( _proxy_enum ) {
      _proxy_enum->setParent ( this );
      _proxy_enum->set_index ( 2 );
    }
    this->enum_proxy_changed ();
  }
}

void
Column::set_show_value_string ( bool flag_n )
{
  if ( flag_n != _show_value_string ) {
    _show_value_string = flag_n;
    this->show_value_string_changed ();
  }
}

QString
Column::value_string () const
{
  QString res;
  if ( slider_proxy () != nullptr ) {
    const QString mask ( "%1" );
    res = mask.arg ( slider_proxy ()->slider_index () );
  }
  return res;
}

QString
Column::value_min_string () const
{
  QString res;
  if ( slider_proxy () != nullptr ) {
    const QString mask ( "%1" );
    res = mask.arg ( 0 );
  }
  return res;
}

QString
Column::value_max_string () const
{
  QString res;
  if ( slider_proxy () != nullptr ) {
    const QString mask ( "%1" );
    res = mask.arg ( slider_proxy ()->slider_index_max () );
  }
  return res;
}

void
Column::slider_proxy_changed ()
{
  // Dummy implementation
}

void
Column::switch_proxy_changed ()
{
  // Dummy implementation
}

void
Column::enum_proxy_changed ()
{
  // Dummy implementation
}

void
Column::show_value_string_changed ()
{
  // Dummy implementation
}

bool
Column::set_focus ( unsigned int row_n )
{
  bool success = false;

  Proxy * proxy = nullptr;
  if ( has_slider () && ( slider_proxy ()->index () == row_n ) ) {
    proxy = slider_proxy ();
  } else if ( has_switch () && ( switch_proxy ()->index () == row_n ) ) {
    proxy = switch_proxy ();
  } else if ( has_enum () && ( enum_proxy ()->index () == row_n ) ) {
    proxy = enum_proxy ();
  }
  if ( proxy == nullptr ) {
    proxy = slider_proxy ();
  }
  if ( proxy == nullptr ) {
    proxy = switch_proxy ();
  }
  if ( proxy == nullptr ) {
    proxy = enum_proxy ();
  }

  if ( proxy != nullptr ) {
    QWidget * wdg = proxy->widget ();
    if ( wdg != nullptr ) {
      if ( wdg->isEnabled () ) {
        wdg->setFocus ();
        success = true;
      }
    }
  }
  return success;
}

bool
Column::event ( QEvent * event_n )
{
  if ( event_n->type () == Wdg::evt_pass_event_focus ) {
    Wdg::Pass_Event_Focus * ev_fp (
        static_cast< Wdg::Pass_Event_Focus * > ( event_n ) );

    _has_focus = ev_fp->ev_focus.gotFocus ();
    if ( parent () != nullptr ) {
      ev_fp->column_idx = column_index ();
      QCoreApplication::sendEvent ( parent (), event_n );
    }
    return true;
  }

  return QObject::event ( event_n );
}

} // namespace Wdg::Pad_Proxy
