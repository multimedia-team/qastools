/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QTreeView>

namespace Wdg
{

/// @brief Tree_View_KV
///
class Tree_View_KV : public QTreeView
{
  Q_OBJECT

  public:
  // -- Construction

  Tree_View_KV ( QWidget * parent = nullptr );

  ~Tree_View_KV ();

  // -- Model

  void
  setModel ( QAbstractItemModel * model_n ) override;

  // -- Activate current

  bool
  activate_current () const
  {
    return _activate_current;
  }

  void
  set_activate_current ( bool flag_n );

  // -- Public slots

  Q_SLOT
  void
  set_expanded_recursive ( const QModelIndex & index_n,
                           int depth_n,
                           bool expanded_n );

  Q_SLOT
  void
  expand_recursive ( const QModelIndex & index_n, int depth_n );

  Q_SLOT
  void
  collapse_recursive ( const QModelIndex & index_n, int depth_n );

  Q_SLOT
  void
  adjust_first_column_width ();

  protected:
  // -- Protected methods

  void
  currentChanged ( const QModelIndex & current, const QModelIndex & previous );

  private:
  // -- Attributes
  bool _activate_current = true;
};

} // namespace Wdg
