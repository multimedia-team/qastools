/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QHash>
#include <QObject>
#include <QPalette>

namespace Wdg
{

/// @brief Style_Db
///
class Style_Db : public QObject
{
  Q_OBJECT

  public:
  // -- Public types

  enum Style
  {
    STYLE_DEFAULT = 0,
    STYLE_USER = 32 // User defined styles should start here
  };

  public:
  // -- Construction

  Style_Db ( QObject * parent_n = nullptr );

  ~Style_Db ();

  // -- Palette

  /// @brief Returns the default palette
  const QPalette &
  palette_default () const;

  /// @brief Returns the default palette if the given style_id_n has no palette
  const QPalette &
  palette ( unsigned int style_id_n ) const;

  void
  insert_palette ( unsigned int style_id_n, const QPalette & pal_n );

  Q_SIGNAL
  void
  sig_palette_changed ( unsigned int style_id_n );

  // -- Colors

  const QColor &
  color ( unsigned int style_id_n, QPalette::ColorRole role_n ) const
  {
    return palette ( style_id_n ).color ( role_n );
  }

  const QColor &
  color ( unsigned int style_id_n,
          QPalette::ColorGroup group_n,
          QPalette::ColorRole role_n ) const
  {
    return palette ( style_id_n ).color ( group_n, role_n );
  }

  private:
  // -- Attributes
  QHash< unsigned int, QPalette > _palettes;
};

} // namespace Wdg
