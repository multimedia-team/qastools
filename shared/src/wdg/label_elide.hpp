/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>
#include <QWidget>

namespace Wdg
{

/// @brief Label that elides the string when the widget width is too small
///
class Label_Elide : public QWidget
{
  public:
  // -- Construction

  Label_Elide ( QWidget * parent_n = nullptr );

  ~Label_Elide ();

  // -- Text

  const QString &
  text () const
  {
    return _text;
  }

  void
  setText ( const QString txt_n );

  // -- Alignment

  Qt::Alignment
  alignment () const
  {
    return _alignment;
  }

  void
  setAlignment ( Qt::Alignment align_n );

  // -- Elide mode

  Qt::TextElideMode
  elideMode () const
  {
    return _elide_mode;
  }

  void
  setElideMode ( Qt::TextElideMode mode_n );

  // -- Size hints

  QSize
  minimumSizeHint () const override;

  QSize
  sizeHint () const override;

  protected:
  // -- Protected methods

  void
  update_text_elided ();

  void
  resizeEvent ( QResizeEvent * event_n ) override;

  void
  paintEvent ( QPaintEvent * event_n ) override;

  private:
  // -- Attributes
  QString _text;
  QString _text_elided;
  Qt::Alignment _alignment = ( Qt::AlignLeft | Qt::AlignVCenter );
  Qt::TextElideMode _elide_mode;
};

} // namespace Wdg
