/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "widgets_set.hpp"

namespace Wdg::Switches_Pad
{

Widgets_Set::Widgets_Set () = default;

Widgets_Set::~Widgets_Set () = default;

void
Widgets_Set::set_label ( QWidget * wdg_n )
{
  _label.reset ( wdg_n );
}

void
Widgets_Set::set_input ( QWidget * wdg_n )
{
  _input.reset ( wdg_n );
}

} // namespace Wdg::Switches_Pad
