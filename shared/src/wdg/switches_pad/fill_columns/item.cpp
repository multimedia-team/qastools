/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "item.hpp"
#include <QApplication>
#include <QWidget>
#include <iostream>

namespace Wdg::Switches_Pad::Fill_Columns
{

Item::Item () = default;

Item::Item ( Item && item_n ) = default;

Item::~Item () = default;

Item &
Item::operator= ( Item && item_n ) = default;

} // namespace Wdg::Switches_Pad::Fill_Columns
