/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/switches_pad/fill_columns/item.hpp"
#include <QLayout>
#include <vector>

namespace Wdg::Switches_Pad::Fill_Columns
{

/// @brief Layout
///
class Layout : public QLayout
{
  public:
  // -- Construction

  Layout ( QWidget * parent_n = nullptr );

  ~Layout ();

  // -- Size hints

  QSize
  sizeHint () const override;

  QSize
  minimumSize () const override;

  bool
  hasHeightForWidth () const;

  int
  heightForWidth ( int width ) const;

  // -- Spacings

  int
  horizontal_spacing () const;

  void
  set_horizontal_spacing ( int spacing_n );

  int
  vertical_spacing () const;

  void
  set_vertical_spacing ( int spacing_n );

  unsigned int
  horizontal_spacing_default () const;

  unsigned int
  vertical_spacing_default () const;

  unsigned int
  spacing_default ( Qt::Orientation orient_n ) const;

  // -- Viewport geometry

  const QRect &
  viewport_geometry () const
  {
    return _viewport;
  }

  void
  set_viewport_geometry ( const QRect & rect_n );

  // Layout methods

  QLayoutItem *
  itemAt ( int index_n ) const override;

  int
  count () const override;

  void
  addItem ( QLayoutItem * item_n ) override;

  QLayoutItem *
  takeAt ( int index_n ) override;

  void
  invalidate ();

  void
  setGeometry ( const QRect & rect_n );

  protected:
  // -- Protected methods

  void
  update_cache_const () const;

  void
  update_cache ();

  QSize
  calc_columns ( unsigned int width_n, unsigned int height_n );

  private:
  // -- Attributes
  std::vector< Item > _items;

  QRect _viewport;

  int _hspace;
  int _vspace;

  bool _cache_dirty;
};

} // namespace Wdg::Switches_Pad::Fill_Columns
