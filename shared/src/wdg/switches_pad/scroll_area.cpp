/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "scroll_area.hpp"
#include "wdg/switches_pad/fill_columns/layout.hpp"
#include "wdg/switches_pad/pad.hpp"
#include <QEvent>
#include <QScrollBar>
#include <iostream>

namespace Wdg::Switches_Pad
{

Scroll_Area::Scroll_Area ( QWidget * parent_n )
: QScrollArea ( parent_n )
{
  setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
  setWidgetResizable ( true );
}

Scroll_Area::~Scroll_Area () = default;

QSize
Scroll_Area::minimumSizeHint () const
{
  ensurePolished ();

  if ( widget () == nullptr ) {
    return QScrollArea::minimumSizeHint ();
  }

  QSize res ( 0, 0 );

  // Height
  {
    Wdg::Switches_Pad::Pad * sw_pad =
        dynamic_cast< Wdg::Switches_Pad::Pad * > ( widget () );

    const Wdg::Switches_Pad::Fill_Columns::Layout * lay_cols =
        dynamic_cast< const Wdg::Switches_Pad::Fill_Columns::Layout * > (
            widget ()->layout () );

    {
      // Number of visible rows
      unsigned int num_vis ( 4 );
      if ( sw_pad != nullptr ) {
        num_vis = sw_pad->num_proxies_groups ();
      }

      // Number of minimum visible rows
      unsigned int min_rows ( 4 );
      if ( num_vis < 6 ) {
        min_rows = 3;
      }
      if ( num_vis < 2 ) {
        min_rows = 2;
      }

      res.rheight () += widget ()->fontMetrics ().height () * min_rows;
      if ( lay_cols != 0 ) {
        res.rheight () +=
            lay_cols->vertical_spacing_default () * ( min_rows - 1 );
      }
    }
    if ( lay_cols != nullptr ) {
      QMargins mgs = lay_cols->contentsMargins ();
      res.rheight () += mgs.top () + mgs.bottom ();
    }
  }

  // Widget width
  {
    QSize wdg_msh = widget ()->minimumSizeHint ();
    if ( wdg_msh.width () > 0 ) {
      res.rwidth () += wdg_msh.width ();
    }
  }

  // Scrollbar
  if ( verticalScrollBar () != nullptr ) {
    QSize sb_msh = verticalScrollBar ()->minimumSizeHint ();
    if ( sb_msh.width () <= 0 ) {
      sb_msh = verticalScrollBar ()->sizeHint ();
    }
    if ( sb_msh.width () > 0 ) {
      res.rwidth () += sb_msh.width ();
    }
  }

  // Contents margins
  {
    QMargins mgs ( contentsMargins () );
    res.rwidth () += mgs.left () + mgs.right ();
    res.rheight () += mgs.top () + mgs.bottom ();
  }

  return res;
}

QSize
Scroll_Area::sizeHint () const
{
  return minimumSizeHint ();
}

void
Scroll_Area::set_widget ( QWidget * wdg_n )
{
  if ( widget () != nullptr ) {
    widget ()->removeEventFilter ( this );
  }

  setWidget ( wdg_n );

  if ( widget () != nullptr ) {
    widget ()->installEventFilter ( this );
  }

  updateGeometry ();
}

QWidget *
Scroll_Area::take_widget ()
{
  if ( widget () != nullptr ) {
    widget ()->removeEventFilter ( this );
  }
  return takeWidget ();
}

void
Scroll_Area::resizeEvent ( QResizeEvent * event_n )
{
  if ( widget () != nullptr ) {
    Wdg::Switches_Pad::Pad * spad =
        dynamic_cast< Wdg::Switches_Pad::Pad * > ( widget () );
    if ( spad != nullptr ) {
      spad->set_viewport_geometry ( viewport ()->contentsRect () );
    }
  }
  QScrollArea::resizeEvent ( event_n );
}

bool
Scroll_Area::eventFilter ( QObject * watched_n, QEvent * event_n )
{
  if ( watched_n == widget () ) {
    if ( event_n->type () == QEvent::LayoutRequest ) {
      // std::cout << "Scroll_Area::eventFilter: " << event_n->type() <<
      //"\n";
      updateGeometry ();
    }
  }

  return false;
}

} // namespace Wdg::Switches_Pad
