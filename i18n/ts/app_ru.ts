<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>ALSA::CTL_Arg_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>CARD</source>
        <translation>CARD</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>SOCKET</source>
        <translation>SOCKET</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>CTL</source>
        <translation>CTL</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>CARD</source>
        <translation>CARD</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>HWDEP</source>
        <translation>HWDEP</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>MIXER</source>
        <translation>MIXER</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>PCM</source>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>RAWMIDI</source>
        <translation>RAWMIDI</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="89"/>
        <source>TIMER</source>
        <translation>TIMER</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="90"/>
        <source>SEQUENCER</source>
        <translation>SEQUENCER</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="91"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="81"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>NONE</source>
        <translation>NONE</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>BOOLEAN</source>
        <translation>BOOLEAN</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>INTEGER</source>
        <translation>INTEGER</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>ENUMERATED</source>
        <translation>ENUMERATED</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>BYTES</source>
        <translation>BYTES</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="98"/>
        <source>IEC958</source>
        <translatorcomment>S/PDIF</translatorcomment>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="99"/>
        <source>INTEGER64</source>
        <translation>INTEGER64</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="100"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="46"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Front Left</source>
        <translation>Фронтальный левый</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="75"/>
        <source>Front Right</source>
        <translation>Фронтальный правый</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Rear Left</source>
        <translation>Задний левый</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Rear Right</source>
        <translation>Задний правый</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="78"/>
        <source>Front Center</source>
        <translation>Фронтальный центральный</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="79"/>
        <source>Woofer</source>
        <translatorcomment>SubWoofer</translatorcomment>
        <translation>Сабвуфер</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Side Left</source>
        <translation>Боковой левый</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Side Right</source>
        <translation>Боковой правый</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Center</source>
        <translation>Задний центральный</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="15"/>
        <source>Auto Gain Control</source>
        <translation>Auto Gain Control</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="16"/>
        <source>Auto-Mute Mode</source>
        <translation>Auto-Mute Mode</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="17"/>
        <source>Bass</source>
        <translation>Bass</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="18"/>
        <source>Beep</source>
        <translation>Beep</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="19"/>
        <source>Capture</source>
        <translation>Capture</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="20"/>
        <source>Center</source>
        <translation>Center</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="21"/>
        <source>Channel Mode</source>
        <translation>Channel Mode</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="22"/>
        <source>External Amplifier</source>
        <translation>External Amplifier</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="23"/>
        <source>External Mic</source>
        <translation>External Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="24"/>
        <source>Ext Mic</source>
        <translation>Ext Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="25"/>
        <source>Front</source>
        <translation>Front</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="26"/>
        <source>Front Mic</source>
        <translation>Front Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="27"/>
        <source>Front Mic Boost</source>
        <translation>Front Mic Boost</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Headphone</source>
        <translation>Headphone</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Headphone LFE</source>
        <translation>Headphone LFE</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Headphone Center</source>
        <translation>Headphone Center</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Input Source</source>
        <translation>Input Source</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Internal Mic</source>
        <translation>Internal Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Int Mic</source>
        <translation>Int Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>Line Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Loopback Mixing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Master</source>
        <translation>Master</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Mic</source>
        <translation>Микрофон</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Mic Boost</source>
        <translation>Mic Boost</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Mic Boost (+20dB)</source>
        <translation>Mic Boost (+20dB)</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Mic Select</source>
        <translation>Mic Select</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>PC Speaker</source>
        <translation>PC Speaker</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Rear Mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Rear Mic Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Side</source>
        <translation>Side</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Speaker</source>
        <translation>Speaker</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Surround</source>
        <translation>Surround</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Treble</source>
        <translation>Treble</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>Enabled</source>
        <translation>Enabled</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>Disabled</source>
        <translation>Disabled</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>1ch</source>
        <translation>1ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>2ch</source>
        <translation>2ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>3ch</source>
        <translation>3ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>4ch</source>
        <translation>4ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>5ch</source>
        <translation>5ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>6ch</source>
        <translation>6ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="58"/>
        <source>7ch</source>
        <translation>7ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="59"/>
        <source>8ch</source>
        <translation>8ch</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>Front Mic</source>
        <translation>Front Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>Internal Mic</source>
        <translation>Internal Mic</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>Mic</source>
        <translation>Микрофон</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>Mic1</source>
        <translation>Микрофон 1</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>Mic2</source>
        <translation>Микрофон 2</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="68"/>
        <source>Mic3</source>
        <translation>Микрофон 3</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Mic4</source>
        <translation>Микрофон 4</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Rear Mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
</context>
<context>
    <name>ALSA::Flags</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="87"/>
        <source>not readable</source>
        <translation>не доступен на чтение</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="89"/>
        <source>readable</source>
        <translation>доступен на чтение</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="95"/>
        <source>not writable</source>
        <translation>не доступен на запись</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="97"/>
        <source>writable</source>
        <translation>доступен на запись</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="103"/>
        <source>not volatile</source>
        <translation>неизменяющийся</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="105"/>
        <source>volatile</source>
        <translation>изменяющийся</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="111"/>
        <source>not active</source>
        <translation>не активен</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="113"/>
        <source>active</source>
        <translation>активен</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Int</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>minimum</source>
        <translation>минимум</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>maximum</source>
        <translation>максимум</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="36"/>
        <source>Integer range:</source>
        <translation>Целочисленный диапазон:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="37"/>
        <source>Decibel range:</source>
        <translation>Диапазон (дБ):</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="125"/>
        <source>Channel %1</source>
        <translation>Канал %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="126"/>
        <source>Index %1</source>
        <translation>Номер %1</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Unsupported</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/unsupported.cpp" line="20"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Элемент типа %1 не поддерживается</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Int_Proxy::Column</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="13"/>
        <source>%1 dB</source>
        <extracomment>Decibel value string template</extracomment>
        <translation>%1 дБ</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="15"/>
        <source>%1 %</source>
        <extracomment>Percent value string template</extracomment>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Mixer</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="32"/>
        <source>Element name</source>
        <translation>Название элемента</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="36"/>
        <source>Joined</source>
        <translation>Объединить</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="37"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="108"/>
        <source>Element index</source>
        <translation>Номер элемента</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="38"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="41"/>
        <source>Channel %1</source>
        <translation>Канал %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="40"/>
        <source>Ch. %1</source>
        <translation>Канал %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="45"/>
        <source>Index: %1</source>
        <translation>Номер: %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="46"/>
        <source>Channel: %2</source>
        <translation>Номер канала: %2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="101"/>
        <source>Index:</source>
        <translation>Номер:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="102"/>
        <source>Device:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="103"/>
        <source>Flags:</source>
        <translation>Флаги:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="104"/>
        <source>Channels:</source>
        <translation>Количество каналов:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="105"/>
        <source>Num. id:</source>
        <translation>Числовой идентификатор:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="109"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="125"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="110"/>
        <source>Flags</source>
        <translation>Флаги</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="111"/>
        <source>Channel count</source>
        <translation>Количество каналов</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="112"/>
        <source>Numeric Id</source>
        <translation>Числовой идентификатор</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="126"/>
        <source>Subdevice</source>
        <translation>Дочернее устройство</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="295"/>
        <source>No element selected</source>
        <translation>Элемент не выбран</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Table_Model</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="21"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="29"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="30"/>
        <source>Element name</source>
        <translation>Название элемента</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="33"/>
        <source>Idx</source>
        <extracomment>Idx - abbreviation for Index</extracomment>
        <translation>Номер</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="34"/>
        <source>Element index</source>
        <translation>Номер элемента</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="36"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="37"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="39"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="40"/>
        <source>Element type</source>
        <translation>Тип элемента</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="43"/>
        <source>Ch.</source>
        <extracomment>Ch. - abbreviation for Channel</extracomment>
        <translation>Ch.</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="44"/>
        <source>Channel count</source>
        <translation>Количество каналов</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="46"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="47"/>
        <source>Flags</source>
        <translation>Флаги</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="49"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="50"/>
        <source>Numeric Id</source>
        <translation>Числовой идентификатор</translation>
    </message>
</context>
<context>
    <name>MWdg::Inputs_Setup</name>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="22"/>
        <source>s</source>
        <extracomment>&quot;s&quot; is an abbreviation for &quot;split&quot; or &quot;separate&quot;. Something like &quot;j&quot; for &quot;joined&quot; or &quot;c&quot; for &quot;channel&quot; may be appropriate, too.</extracomment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="25"/>
        <source>l</source>
        <extracomment>&quot;l&quot; is an abbreviation for &quot;level&quot;</extracomment>
        <translation>l</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="28"/>
        <source>m</source>
        <extracomment>&quot;m&quot; is an abbreviation for &quot;mute&quot;</extracomment>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="32"/>
        <source>p</source>
        <extracomment>&quot;p&quot; is an abbreviation for &quot;playback&quot;</extracomment>
        <translation>p</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="36"/>
        <source>c</source>
        <extracomment>&quot;c&quot; is an abbreviation for &quot;capture&quot; or &quot;record&quot;</extracomment>
        <translation>c</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="42"/>
        <source>Split &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Разделить &amp;каналы</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="46"/>
        <source>Join &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Объединить &amp;каналы</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="50"/>
        <source>&amp;Level channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Выровнять &amp;громкость</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="21"/>
        <source>Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="22"/>
        <source>Capture</source>
        <translation type="unfinished">Capture</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Settings_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="239"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="241"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="275"/>
        <source>Sliders</source>
        <translation type="unfinished">Регуляторы громкости</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="319"/>
        <source>Switches</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Mixer</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="64"/>
        <source>&amp;Mute</source>
        <translation>&amp;Выключить звук</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="65"/>
        <source>&amp;Mute all</source>
        <translation>&amp;Выключить звук у всех каналов</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="66"/>
        <source>Un&amp;mute</source>
        <translation>&amp;Включить звук</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="67"/>
        <source>Un&amp;mute all</source>
        <translation>&amp;Включить звук у всех каналов</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="68"/>
        <source>Toggle &amp;mutes</source>
        <translation>&amp;Включить/выключить звук</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="26"/>
        <source>Playback slider</source>
        <translation type="unfinished">Регулятор громкости воспроизведения</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="27"/>
        <source>Capture slider</source>
        <translation type="unfinished">Регулятор громкости записи</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="28"/>
        <source>Playback switch</source>
        <translation type="unfinished">Выключатель воспроизведения</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="29"/>
        <source>Capture switch</source>
        <translation type="unfinished">Выключатель записи</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="119"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxy::Column</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="17"/>
        <source>%1 dB</source>
        <translation>%1 дБ</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="18"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Switches::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="25"/>
        <source>Playback selection</source>
        <translation type="unfinished">Playback selection</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="26"/>
        <source>Capture selection</source>
        <translation type="unfinished">Capture selection</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="27"/>
        <source>Playback switch</source>
        <translation type="unfinished">Выключатель воспроизведения</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="28"/>
        <source>Capture switch</source>
        <translation type="unfinished">Выключатель записи</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="147"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Slider_Status_Widget</name>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="23"/>
        <source>Slider value</source>
        <translation>Состояние канала</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="32"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="35"/>
        <source>Max.</source>
        <translation>Макс.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="36"/>
        <source>Maximum</source>
        <translation>Максимальное</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="39"/>
        <source>Min.</source>
        <translation>Мин.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="41"/>
        <source>Minimum</source>
        <translation>Минимальное</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="46"/>
        <source>Volume</source>
        <translation>Громкость</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="52"/>
        <source>Current volume</source>
        <translation>Текущая громкость</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="60"/>
        <source>Volume maximum</source>
        <translation>Максимальная громкость</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="65"/>
        <source>Volume minimum</source>
        <translation>Минимальная громкость</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="72"/>
        <source>Decibel</source>
        <translation>Громкость (дБ)</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="78"/>
        <source>Current Decibel value</source>
        <translation>Текущая громкость (дБ)</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="88"/>
        <source>Decibel maximum</source>
        <translation>Максимальная громкость (дБ)</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="93"/>
        <source>Decibel minimum</source>
        <translation>Минимальная громкость (дБ)</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="98"/>
        <source>Element name</source>
        <translation>Название элемента</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="322"/>
        <source>Close slider value display</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="17"/>
        <source>Show playback</source>
        <translation type="unfinished">Отображать элементы востроизведения</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="18"/>
        <source>Show capture</source>
        <translation type="unfinished">Отображать элементы записи</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="21"/>
        <source>Show playback controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="22"/>
        <source>Show capture controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection_Actions</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="17"/>
        <source>Show &amp;playback controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="18"/>
        <source>Show &amp;capture controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::User_Device_Input</name>
    <message>
        <location filename="../../shared/src/mwdg/user_device_input.cpp" line="20"/>
        <source>User device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="33"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="40"/>
        <source>&amp;Fullscreen mode</source>
        <translation>П&amp;олноэкранный режим</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="34"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="41"/>
        <source>Exit &amp;fullscreen mode</source>
        <translation>Выйти из п&amp;олноэкранного режима</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="25"/>
        <location filename="../../qashctl/src/main_window.cpp" line="102"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="149"/>
        <source>&amp;Quit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="155"/>
        <source>&amp;Settings</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="156"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="30"/>
        <location filename="../../qashctl/src/main_window.cpp" line="127"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="164"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="109"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="172"/>
        <source>Show &amp;device selection</source>
        <translation>Отображать панель выбора &amp;устройства</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="41"/>
        <location filename="../../qashctl/src/main_window.cpp" line="146"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="224"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="47"/>
        <location filename="../../qashctl/src/main_window.cpp" line="152"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="230"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="53"/>
        <location filename="../../qashctl/src/main_window.cpp" line="161"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="241"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="217"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="219"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>QSnd::HCtl::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/mixer.cpp" line="62"/>
        <source>Empty device name</source>
        <translation>Имя устройства пусто</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer/mixer.cpp" line="81"/>
        <source>Empty device name</source>
        <translation>Имя устройства пусто</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixers_Model</name>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="272"/>
        <source>Default</source>
        <translation type="unfinished">Устройство по умолчанию</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="276"/>
        <source>User defined</source>
        <translation type="unfinished">Другое</translation>
    </message>
</context>
<context>
    <name>Tray::Icon</name>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="28"/>
        <source>&amp;Show mixer</source>
        <translation>&amp;Отображать микшер</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="29"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="35"/>
        <source>&amp;Close %1</source>
        <extracomment>%1 will be replaced with the program title</extracomment>
        <translation>&amp;Закрыть %1</translation>
    </message>
</context>
<context>
    <name>Tray::Notifier</name>
    <message>
        <location filename="../../qasmixer/src/tray/notifier.cpp" line="24"/>
        <source>Open mixer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tray::Shared</name>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="21"/>
        <source>Volume is at %1 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="22"/>
        <source>Volume change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="23"/>
        <source>Muted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Alsa_Config_View</name>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="23"/>
        <source>ALSA configuration</source>
        <translation>Конфигурация ALSA</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="27"/>
        <source>&amp;Expand</source>
        <translation>&amp;Раскрыть</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="28"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Свернуть</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="29"/>
        <source>&amp;Sort</source>
        <translation>&amp;Упорядочить</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="31"/>
        <source>Depth:</source>
        <translation>Глубина:</translation>
    </message>
</context>
<context>
    <name>Views::Basic_Dialog</name>
    <message>
        <location filename="../../shared/src/views/basic_dialog.cpp" line="66"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_Bar</name>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="23"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="26"/>
        <source>Show device settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_View</name>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="25"/>
        <source>&amp;Close device selection</source>
        <translation>Закрыть панель выбора у&amp;стройства</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="56"/>
        <source>Mixer device</source>
        <translation>Устройство</translation>
    </message>
</context>
<context>
    <name>Views::Device_Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="35"/>
        <source>Device settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="74"/>
        <source>Sort by playback and capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="88"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="111"/>
        <source>Address</source>
        <translation type="unfinished">Адрес</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="112"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="113"/>
        <source>Id</source>
        <translation type="unfinished">Id</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="114"/>
        <source>Name</source>
        <translation type="unfinished">Название</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="115"/>
        <source>Long name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="116"/>
        <source>Mixer name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="117"/>
        <source>Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="118"/>
        <source>Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="133"/>
        <source>Information</source>
        <translation type="unfinished">Информация</translation>
    </message>
</context>
<context>
    <name>Views::Info_Dialog</name>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="40"/>
        <location filename="../../shared/src/views/info_dialog.cpp" line="62"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="76"/>
        <source>Internet</source>
        <translation>Интернет</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="80"/>
        <source>Project page</source>
        <translation>Страница проекта</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="108"/>
        <source>Developers</source>
        <translation>Программирование</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="130"/>
        <source>Translators</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="189"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="190"/>
        <source>People</source>
        <translation>Авторы</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="191"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="66"/>
        <source>%1 is a collection of desktop applications for the Linux sound system %2.</source>
        <translation>%1 — это набор приложений для работы со звуковой системой %2.</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="20"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="29"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="118"/>
        <source>Contributors</source>
        <translation>Участники</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="168"/>
        <source>The license file %1 is not available.</source>
        <translation>Файл лицензии %1 не доступен.</translation>
    </message>
</context>
<context>
    <name>Views::Message_Widget</name>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="31"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>Не удалось открыть микшер</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="32"/>
        <source>No device selected</source>
        <translation>Устройство не выбрано</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="68"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="69"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="70"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>Views::Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="24"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="105"/>
        <source>Startup</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="112"/>
        <source>Sliders</source>
        <translation>Регуляторы громкости</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="153"/>
        <source>Appearance</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="216"/>
        <source>Input</source>
        <translation>Управление</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="358"/>
        <source>System tray</source>
        <translation>Трей</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="49"/>
        <source>Startup mixer device</source>
        <translation>Устройство используемое после запуска</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="54"/>
        <source>From last session</source>
        <translation>Из предыдущей сессии</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="115"/>
        <source>Show slider value labels</source>
        <translation>Отображать текущее значение громкости числом</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="127"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="129"/>
        <source>Show tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="160"/>
        <source>Mouse wheel</source>
        <translation>Колесико мыши</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="163"/>
        <source>Number of turns for a slider change from 0% to 100%</source>
        <translation>Количество оборотов колесика мыши, чтобы изменить громкость от 0% до 100%</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="222"/>
        <source>Show tray icon</source>
        <translation>Отображать значок в трее</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="223"/>
        <source>Close to tray</source>
        <translation>Закрываться в трей</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="236"/>
        <source>System tray usage</source>
        <translation>Использование системного трея</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="245"/>
        <source>Notification balloon</source>
        <translation>Всплывающая подсказка</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="249"/>
        <source>Show balloon on a volume change</source>
        <translation>Отображать всплывающую подсказку при изменении громкости</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="259"/>
        <source>Balloon lifetime</source>
        <translation>Длительность всплывающей подсказки</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="262"/>
        <source>ms</source>
        <extracomment>ms - abbreviation for milliseconds</extracomment>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="295"/>
        <source>Mini mixer device</source>
        <translation>Устройство для минимикшера</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="53"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="297"/>
        <source>Default</source>
        <translation>Устройство по умолчанию</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="299"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Текущее (то же устройство, что и в главном окне)</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="55"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="300"/>
        <source>User defined</source>
        <translation>Другое</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="64"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="309"/>
        <source>User device:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="65"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="310"/>
        <source>e.g. hw:0</source>
        <translation>Например: hw:0</translation>
    </message>
</context>
</TS>
