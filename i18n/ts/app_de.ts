<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>ALSA::CTL_Arg_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>CARD</source>
        <translation>Karte</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>SOCKET</source>
        <translation>Socket</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>CTL</source>
        <translation>CTL</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>CARD</source>
        <translation>Karte</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>HWDEP</source>
        <translation>Gerätespez.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>MIXER</source>
        <translation>Mixer</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>PCM</source>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>RAWMIDI</source>
        <translation>Rohmidi</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="89"/>
        <source>TIMER</source>
        <translation>Zeitgeber</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="90"/>
        <source>SEQUENCER</source>
        <translation>Sequencer</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="91"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="81"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>NONE</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>BOOLEAN</source>
        <translation>Bool</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>INTEGER</source>
        <translation>Integer</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>ENUMERATED</source>
        <translation>Aufzählung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>BYTES</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="98"/>
        <source>IEC958</source>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="99"/>
        <source>INTEGER64</source>
        <translation>Integer64</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="100"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="46"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Front Left</source>
        <translation>Vorne links</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="75"/>
        <source>Front Right</source>
        <translation>Vorne rechts</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Rear Left</source>
        <translation>Hinten links</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Rear Right</source>
        <translation>Hinten rechts</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="78"/>
        <source>Front Center</source>
        <translation>Vorne Mitte</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="79"/>
        <source>Woofer</source>
        <translation>Tieftöner</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Side Left</source>
        <translation>Seite links</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Side Right</source>
        <translation>Seite rechts</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Center</source>
        <translation>Hinten Mitte</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Master</source>
        <translation>Hauptregler</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Speaker</source>
        <translation>Lautsprecher</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Headphone</source>
        <translation>Kopfhörer</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Headphone LFE</source>
        <translation>Kopfhörer LFE</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Headphone Center</source>
        <translation>Kopfhörer Mitte</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="25"/>
        <source>Front</source>
        <translation>Vorne</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="16"/>
        <source>Auto-Mute Mode</source>
        <translation>Auto-Stumm-Modus</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="26"/>
        <source>Front Mic</source>
        <translation>Mik. vorne</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="27"/>
        <source>Front Mic Boost</source>
        <translation>Mik. vorne Verstärkung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>Line</source>
        <translation>Line</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>Line Boost</source>
        <translation>Line-Verstärkung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Loopback Mixing</source>
        <translation>Loopback-Mischung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Mic Boost</source>
        <translation>Mik. Verstärkung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Rear Mic</source>
        <translation>Mik. hinten</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Rear Mic Boost</source>
        <translation>Mik. hinten Verstärkung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Surround</source>
        <translation>Umgebung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="20"/>
        <source>Center</source>
        <translation>Mitte</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Side</source>
        <translation>Seite</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="19"/>
        <source>Capture</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="18"/>
        <source>Beep</source>
        <translation>Signalton</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>PC Speaker</source>
        <translation>PC Lautsprecher</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="17"/>
        <source>Bass</source>
        <translation>Bässe</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Treble</source>
        <translation>Höhen</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="21"/>
        <source>Channel Mode</source>
        <translation>Kanal Modus</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="15"/>
        <source>Auto Gain Control</source>
        <translation>Autom. Verst.-Regelung</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Mic Select</source>
        <translation>Mikrofonwahl</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Internal Mic</source>
        <translation>Internes Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Int Mic</source>
        <translation>Internes Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="23"/>
        <source>External Mic</source>
        <translation>Externes Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="24"/>
        <source>Ext Mic</source>
        <translation>Externes Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Mic Boost (+20dB)</source>
        <translation>Mik. Verst. (+20dB)</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="22"/>
        <source>External Amplifier</source>
        <translation>Externer Verstärker</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Input Source</source>
        <translation>Eingangsquelle</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>1ch</source>
        <translation>1 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>2ch</source>
        <translation>2 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>3ch</source>
        <translation>3 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>4ch</source>
        <translation>4 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>5ch</source>
        <translation>5 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>6ch</source>
        <translation>6 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="58"/>
        <source>7ch</source>
        <translation>7 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="59"/>
        <source>8ch</source>
        <translation>8 Kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>Internal Mic</source>
        <translation>Internes Mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>Line</source>
        <translation>Line</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>Front Mic</source>
        <translation>Mik. vorne</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>Enabled</source>
        <translation>Aktiviert</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>Mic1</source>
        <translation>Mik. 1</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>Mic2</source>
        <translation>Mik. 2</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="68"/>
        <source>Mic3</source>
        <translation>Mik. 3</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Mic4</source>
        <translation>Mik. 4</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Rear Mic</source>
        <translation>Mik. hinten</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
</context>
<context>
    <name>ALSA::Flags</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="87"/>
        <source>not readable</source>
        <translation>nicht lesbar</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="89"/>
        <source>readable</source>
        <translation>lesbar</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="95"/>
        <source>not writable</source>
        <translation>nicht schreibbar</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="97"/>
        <source>writable</source>
        <translation>schreibbar</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="103"/>
        <source>not volatile</source>
        <translation>nicht volatil</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="105"/>
        <source>volatile</source>
        <translation>volatil</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="111"/>
        <source>not active</source>
        <translation>nicht aktiv</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="113"/>
        <source>active</source>
        <translation>aktiv</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Int</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="36"/>
        <source>Integer range:</source>
        <translation>Integerbereich:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="37"/>
        <source>Decibel range:</source>
        <translation>Dezibelbereich:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="125"/>
        <source>Channel %1</source>
        <translation>Kanal %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="126"/>
        <source>Index %1</source>
        <translation>Index %1</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Unsupported</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/unsupported.cpp" line="20"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Elemente vom Typ %1 werden nicht unterstützt</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Int_Proxy::Column</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="13"/>
        <source>%1 dB</source>
        <extracomment>Decibel value string template</extracomment>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="15"/>
        <source>%1 %</source>
        <extracomment>Percent value string template</extracomment>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Mixer</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="32"/>
        <source>Element name</source>
        <translation>Elementname</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="36"/>
        <source>Joined</source>
        <translation>Verbunden</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="37"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="108"/>
        <source>Element index</source>
        <translation>Elementindex</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="38"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="41"/>
        <source>Channel %1</source>
        <translation>Kanal %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="40"/>
        <source>Ch. %1</source>
        <translation>Kan. %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="45"/>
        <source>Index: %1</source>
        <translation>Index: %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="46"/>
        <source>Channel: %2</source>
        <translation>Kanal: %2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="101"/>
        <source>Index:</source>
        <translation>Index:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="102"/>
        <source>Device:</source>
        <translation>Gerät:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="103"/>
        <source>Flags:</source>
        <translation>Flaggen:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="104"/>
        <source>Channels:</source>
        <translation>Kanäle:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="105"/>
        <source>Num. id:</source>
        <translation>Num. id:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="109"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="125"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="110"/>
        <source>Flags</source>
        <translation>Flaggen</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="111"/>
        <source>Channel count</source>
        <translation>Kanalanzahl</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="112"/>
        <source>Numeric Id</source>
        <translation>Numerische Id</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="126"/>
        <source>Subdevice</source>
        <translation>Untergerät</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="295"/>
        <source>No element selected</source>
        <translation>Kein Element ausgewählt</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Table_Model</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="21"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="29"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="30"/>
        <source>Element name</source>
        <translation>Elementname</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="33"/>
        <source>Idx</source>
        <extracomment>Idx - abbreviation for Index</extracomment>
        <translation>Idx</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="34"/>
        <source>Element index</source>
        <translation>Elementindex</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="36"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="37"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="39"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="40"/>
        <source>Element type</source>
        <translation>Elementtyp</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="43"/>
        <source>Ch.</source>
        <extracomment>Ch. - abbreviation for Channel</extracomment>
        <translation>Kan.</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="44"/>
        <source>Channel count</source>
        <translation>Kanalanzahl</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="46"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="47"/>
        <source>Flags</source>
        <translation>Flaggen</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="49"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="50"/>
        <source>Numeric Id</source>
        <translation>Numerische Id</translation>
    </message>
</context>
<context>
    <name>MWdg::Inputs_Setup</name>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="22"/>
        <source>s</source>
        <extracomment>&quot;s&quot; is an abbreviation for &quot;split&quot; or &quot;separate&quot;. Something like &quot;j&quot; for &quot;joined&quot; or &quot;c&quot; for &quot;channel&quot; may be appropriate, too.</extracomment>
        <translatorcomment>Trennen</translatorcomment>
        <translation>t</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="25"/>
        <source>l</source>
        <extracomment>&quot;l&quot; is an abbreviation for &quot;level&quot;</extracomment>
        <translatorcomment>Nivellieren</translatorcomment>
        <translation>n</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="28"/>
        <source>m</source>
        <extracomment>&quot;m&quot; is an abbreviation for &quot;mute&quot;</extracomment>
        <translatorcomment>Stumm</translatorcomment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="32"/>
        <source>p</source>
        <extracomment>&quot;p&quot; is an abbreviation for &quot;playback&quot;</extracomment>
        <translatorcomment>Wiedergabe</translatorcomment>
        <translation>w</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="36"/>
        <source>c</source>
        <extracomment>&quot;c&quot; is an abbreviation for &quot;capture&quot; or &quot;record&quot;</extracomment>
        <translatorcomment>Aufnahme</translatorcomment>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="42"/>
        <source>Split &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>&amp;Kanäle trennen</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="46"/>
        <source>Join &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>&amp;Kanäle verbinden</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="50"/>
        <source>&amp;Level channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Kanäle &amp;nivellieren</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="21"/>
        <source>Playback</source>
        <translation>Wiedergabe</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="22"/>
        <source>Capture</source>
        <translation>Aufnahme</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Settings_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="239"/>
        <source>Controls</source>
        <translation>Regler</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="241"/>
        <source>Show</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="275"/>
        <source>Sliders</source>
        <translation>Schieberegler</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="319"/>
        <source>Switches</source>
        <translation>Schalter</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Mixer</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="64"/>
        <source>&amp;Mute</source>
        <translation>Aus&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="65"/>
        <source>&amp;Mute all</source>
        <translation>Alle aus&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="66"/>
        <source>Un&amp;mute</source>
        <translation>Ein&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="67"/>
        <source>Un&amp;mute all</source>
        <translation>Alle Ein&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="68"/>
        <source>Toggle &amp;mutes</source>
        <translation>Um&amp;schalten</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="26"/>
        <source>Playback slider</source>
        <translation>Wiedergabeschieber</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="27"/>
        <source>Capture slider</source>
        <translation>Aufnahmeschieber</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="28"/>
        <source>Playback switch</source>
        <translation>Wiedergabeschalter</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="29"/>
        <source>Capture switch</source>
        <translation>Aufnahmeschalter</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="119"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxy::Column</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="17"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="18"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Switches::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="25"/>
        <source>Playback selection</source>
        <translation>Wiedergabeauswahl</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="26"/>
        <source>Capture selection</source>
        <translation>Aufnahmeauswahl</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="27"/>
        <source>Playback switch</source>
        <translation>Wiedergabeschalter</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="28"/>
        <source>Capture switch</source>
        <translation>Aufnahmeschalter</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="147"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Slider_Status_Widget</name>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="23"/>
        <source>Slider value</source>
        <translation>Reglerwert</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="32"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="35"/>
        <source>Max.</source>
        <translation>Max.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="36"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="39"/>
        <source>Min.</source>
        <translation>Min.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="41"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="46"/>
        <source>Volume</source>
        <translation>Lautstärke</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="52"/>
        <source>Current volume</source>
        <translation>Aktuelle Lautstärke</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="60"/>
        <source>Volume maximum</source>
        <translation>Lautstärken-Maximum</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="65"/>
        <source>Volume minimum</source>
        <translation>Lautstärken-Minimum</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="72"/>
        <source>Decibel</source>
        <translation>Dezibel</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="78"/>
        <source>Current Decibel value</source>
        <translation>Aktueller Dezibel-Wert</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="88"/>
        <source>Decibel maximum</source>
        <translation>Dezibel-Maximum</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="93"/>
        <source>Decibel minimum</source>
        <translation>Dezibel-Minimum</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="98"/>
        <source>Element name</source>
        <translation>Elementname</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="322"/>
        <source>Close slider value display</source>
        <translation>Reglerwertanzeige schließen</translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="17"/>
        <source>Show playback</source>
        <translation>Wiedergabe anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="18"/>
        <source>Show capture</source>
        <translation>Aufnahme anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="21"/>
        <source>Show playback controls</source>
        <translation>Wiedergaberegler anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="22"/>
        <source>Show capture controls</source>
        <translation>Aufnahmeregler anzeigen</translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection_Actions</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="17"/>
        <source>Show &amp;playback controls</source>
        <translation>&amp;Wiedergaberegler anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="18"/>
        <source>Show &amp;capture controls</source>
        <translation>&amp;Aufnahmeregler anzeigen</translation>
    </message>
</context>
<context>
    <name>MWdg::User_Device_Input</name>
    <message>
        <location filename="../../shared/src/mwdg/user_device_input.cpp" line="20"/>
        <source>User device</source>
        <translation>Benutzergerät</translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="33"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="40"/>
        <source>&amp;Fullscreen mode</source>
        <translation>Voll&amp;bildmodus</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="34"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="41"/>
        <source>Exit &amp;fullscreen mode</source>
        <translation>Voll&amp;bildmodus verlassen</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="25"/>
        <location filename="../../qashctl/src/main_window.cpp" line="102"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="149"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="155"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="156"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+e</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="30"/>
        <location filename="../../qashctl/src/main_window.cpp" line="127"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="164"/>
        <source>&amp;Refresh</source>
        <translation>Auff&amp;rischen</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="109"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="172"/>
        <source>Show &amp;device selection</source>
        <translation>&amp;Geräteauswahl anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="41"/>
        <location filename="../../qashctl/src/main_window.cpp" line="146"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="224"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="47"/>
        <location filename="../../qashctl/src/main_window.cpp" line="152"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="230"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="53"/>
        <location filename="../../qashctl/src/main_window.cpp" line="161"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="241"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="217"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="219"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>QSnd::HCtl::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/mixer.cpp" line="62"/>
        <source>Empty device name</source>
        <translation>Gerätename ist leer</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer/mixer.cpp" line="81"/>
        <source>Empty device name</source>
        <translation>Gerätename ist leer</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixers_Model</name>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="272"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="276"/>
        <source>User defined</source>
        <translation>Benutzerdefiniert</translation>
    </message>
</context>
<context>
    <name>Tray::Icon</name>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="28"/>
        <source>&amp;Show mixer</source>
        <translation>Mixer &amp;anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="29"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="35"/>
        <source>&amp;Close %1</source>
        <extracomment>%1 will be replaced with the program title</extracomment>
        <translation>%1 &amp;beenden</translation>
    </message>
</context>
<context>
    <name>Tray::Notifier</name>
    <message>
        <location filename="../../qasmixer/src/tray/notifier.cpp" line="24"/>
        <source>Open mixer</source>
        <translation>Mixer öffnen</translation>
    </message>
</context>
<context>
    <name>Tray::Shared</name>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="21"/>
        <source>Volume is at %1 %</source>
        <translation>Lautstärke ist bei %1 %</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="22"/>
        <source>Volume change</source>
        <translation>Lautstärkeänderung</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="23"/>
        <source>Muted</source>
        <translation>Stummgeschaltet</translation>
    </message>
</context>
<context>
    <name>Views::Alsa_Config_View</name>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="23"/>
        <source>ALSA configuration</source>
        <translation>ALSA-Konfiguration</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="27"/>
        <source>&amp;Expand</source>
        <translation>&amp;Ausklappen</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="28"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Einklappen</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="29"/>
        <source>&amp;Sort</source>
        <translation>&amp;Sortieren</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="31"/>
        <source>Depth:</source>
        <translation>Tiefe:</translation>
    </message>
</context>
<context>
    <name>Views::Basic_Dialog</name>
    <message>
        <location filename="../../shared/src/views/basic_dialog.cpp" line="66"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_Bar</name>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="23"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="26"/>
        <source>Show device settings</source>
        <translation>Geräteeinstellungen anzeigen</translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_View</name>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="25"/>
        <source>&amp;Close device selection</source>
        <translation>Geräteauswahl &amp;schließen</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="56"/>
        <source>Mixer device</source>
        <translation>Mixergerät</translation>
    </message>
</context>
<context>
    <name>Views::Device_Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="35"/>
        <source>Device settings</source>
        <translation>Geräteeinstellungen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="74"/>
        <source>Sort by playback and capture</source>
        <translation>Sortieren nach Wiedergabe und Aufnahme</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="88"/>
        <source>Controls</source>
        <translation>Regler</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="111"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="112"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="113"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="114"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="115"/>
        <source>Long name</source>
        <translation>Langname</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="116"/>
        <source>Mixer name</source>
        <translation>Mixername</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="117"/>
        <source>Driver</source>
        <translation>Treiber</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="118"/>
        <source>Components</source>
        <translation>Komponenten</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="133"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
</context>
<context>
    <name>Views::Info_Dialog</name>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="40"/>
        <location filename="../../shared/src/views/info_dialog.cpp" line="62"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="76"/>
        <source>Internet</source>
        <translation>Im Internet</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="80"/>
        <source>Project page</source>
        <translation>Projektseite</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="108"/>
        <source>Developers</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="130"/>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="189"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="190"/>
        <source>People</source>
        <translation>Leute</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="191"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="66"/>
        <source>%1 is a collection of desktop applications for the Linux sound system %2.</source>
        <translation>%1 ist eine Sammlung von Schreibtischanwendungen für die Linux-Klang-Architektur %2.</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="20"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="29"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="118"/>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="168"/>
        <source>The license file %1 is not available.</source>
        <translation>Die Lizenzdatei %1 ist nicht verfügbar.</translation>
    </message>
</context>
<context>
    <name>Views::Message_Widget</name>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="31"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>Das Gerät konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="32"/>
        <source>No device selected</source>
        <translation>Kein Gerät ausgewählt</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="68"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="69"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="70"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>Views::Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="24"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="105"/>
        <source>Startup</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="112"/>
        <source>Sliders</source>
        <translation>Schieberegler</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="153"/>
        <source>Appearance</source>
        <translation>Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="216"/>
        <source>Input</source>
        <translation>Eingabe</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="358"/>
        <source>System tray</source>
        <translation>Systemleiste</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="49"/>
        <source>Startup mixer device</source>
        <translation>Mixergerät beim Start</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="54"/>
        <source>From last session</source>
        <translation>Von letzter Sitzung</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="115"/>
        <source>Show slider value labels</source>
        <translation>Reglerwerte anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="127"/>
        <source>Controls</source>
        <translation>Regler</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="129"/>
        <source>Show tooltips</source>
        <translation>Werkzeugtipps anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="160"/>
        <source>Mouse wheel</source>
        <translation>Mausrad</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="163"/>
        <source>Number of turns for a slider change from 0% to 100%</source>
        <translation>Umdrehungen für eine Schieberänderung von 0% auf 100%</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="222"/>
        <source>Show tray icon</source>
        <translation>Symbol anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="223"/>
        <source>Close to tray</source>
        <translation>In Systemleiste schließen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="236"/>
        <source>System tray usage</source>
        <translation>Systemleistennutzung</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="245"/>
        <source>Notification balloon</source>
        <translation>Benachrichtigungsballon</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="249"/>
        <source>Show balloon on a volume change</source>
        <translation>Ballon bei einer Lautstärkenänderung anzeigen</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="259"/>
        <source>Balloon lifetime</source>
        <translation>Ballon-Lebensdauer</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="262"/>
        <source>ms</source>
        <extracomment>ms - abbreviation for milliseconds</extracomment>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="295"/>
        <source>Mini mixer device</source>
        <translation>Minimixergerät</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="53"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="297"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="299"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Aktuell (das Gleiche wie im Mixerfenster)</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="55"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="300"/>
        <source>User defined</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="64"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="309"/>
        <source>User device:</source>
        <translation>Benutzergerät:</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="65"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="310"/>
        <source>e.g. hw:0</source>
        <translation>z.B. hw:0</translation>
    </message>
</context>
</TS>
