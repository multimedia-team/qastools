<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>ALSA::CTL_Arg_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>CARD</source>
        <translation>כרטיס</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>SOCKET</source>
        <translation>שקע</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>CTL</source>
        <translation>‏CTL</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>CARD</source>
        <translatorcomment>כרטיס</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>HWDEP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>MIXER</source>
        <translatorcomment>מערבל</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>PCM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>RAWMIDI</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="89"/>
        <source>TIMER</source>
        <translatorcomment>קוצב זמן</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="90"/>
        <source>SEQUENCER</source>
        <translatorcomment>סקוונסר</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="91"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="81"/>
        <source>Unknown</source>
        <translation>לא מוכר</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>NONE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>BOOLEAN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>INTEGER</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>ENUMERATED</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>BYTES</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="98"/>
        <source>IEC958</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="99"/>
        <source>INTEGER64</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="100"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="46"/>
        <source>Unknown</source>
        <translation>לא מוכר</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Front Left</source>
        <translation>חזית שמאל</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="75"/>
        <source>Front Right</source>
        <translation>חזית ימין</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Rear Left</source>
        <translation>עורף שמאל</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Rear Right</source>
        <translation>עורף ימין</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="78"/>
        <source>Front Center</source>
        <translation>חזית מרכז</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="79"/>
        <source>Woofer</source>
        <translation>וופר</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Side Left</source>
        <translation>צד שמאל</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Side Right</source>
        <translation>צד ימין</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Center</source>
        <translation>מרכז עורף</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="15"/>
        <source>Auto Gain Control</source>
        <translation>בקרת תוספת אוטומטית</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="16"/>
        <source>Auto-Mute Mode</source>
        <translation>מצב השתק-אוטומטי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="17"/>
        <source>Bass</source>
        <translation>בס</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="18"/>
        <source>Beep</source>
        <translation>ביפ</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="19"/>
        <source>Capture</source>
        <translation>לכידה</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="20"/>
        <source>Center</source>
        <translation>מרכז</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="21"/>
        <source>Channel Mode</source>
        <translation>מצב ערוץ</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="22"/>
        <source>External Amplifier</source>
        <translation>מגבר חיצוני</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="23"/>
        <source>External Mic</source>
        <translation>מיקרופון חיצוני</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="24"/>
        <source>Ext Mic</source>
        <translation>מיקרופון חיצוני</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="25"/>
        <source>Front</source>
        <translation>חזית</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="26"/>
        <source>Front Mic</source>
        <translation>מיקרופון חזיתי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="27"/>
        <source>Front Mic Boost</source>
        <translation>מינוף מיקרופון חזיתי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Headphone</source>
        <translation>אוזנייה</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Headphone LFE</source>
        <translation>אוזנייה LFE</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Headphone Center</source>
        <translation>אוזנייה מרכז</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Input Source</source>
        <translation>קלט מקור</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Internal Mic</source>
        <translation>מיקרופון פנימי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Int Mic</source>
        <translation>מיקרופון פנימי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>Line Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Loopback Mixing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Master</source>
        <translation>ראשי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Mic</source>
        <translation>מיקרופון</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Mic Boost</source>
        <translation>מינוף מיקרופון</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Mic Boost (+20dB)</source>
        <translation>מינוף מיקרופון (‎+20דציבל)</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Mic Select</source>
        <translation>ברירת מיקרופון</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>PC Speaker</source>
        <translation>רמקול PC</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Rear Mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Rear Mic Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Side</source>
        <translation>צד</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Speaker</source>
        <translation>רמקול</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Surround</source>
        <translation>סראונד</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Treble</source>
        <translatorcomment>לשלש, פי שלושה</translatorcomment>
        <translation>טרבל</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Video</source>
        <translation>וידאו</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>Enabled</source>
        <translation>מאופשר</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>Disabled</source>
        <translation>מנוטרל</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>1ch</source>
        <translation>1ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>2ch</source>
        <translation>2ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>3ch</source>
        <translation>3ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>4ch</source>
        <translation>4ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>5ch</source>
        <translation>5ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>6ch</source>
        <translation>6ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="58"/>
        <source>7ch</source>
        <translation>7ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="59"/>
        <source>8ch</source>
        <translation>8ער</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>Front Mic</source>
        <translation>מיקרופון חזיתי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>Internal Mic</source>
        <translation>מיקרופון פנימי</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>Mic</source>
        <translation>מיק</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>Mic1</source>
        <translation>מיק1</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>Mic2</source>
        <translation>מיק2</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="68"/>
        <source>Mic3</source>
        <translation>מיק3</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Mic4</source>
        <translation>מיק4</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Rear Mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Video</source>
        <translation>וידאו</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
</context>
<context>
    <name>ALSA::Flags</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="87"/>
        <source>not readable</source>
        <translation>לא קריא</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="89"/>
        <source>readable</source>
        <translatorcomment>בר קריאה</translatorcomment>
        <translation>קריא</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="95"/>
        <source>not writable</source>
        <translation>לא כתיב</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="97"/>
        <source>writable</source>
        <translatorcomment>בר כתיבה</translatorcomment>
        <translation>כתיב</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="103"/>
        <source>not volatile</source>
        <translation>לא נדיף</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="105"/>
        <source>volatile</source>
        <translation>נדיף</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="111"/>
        <source>not active</source>
        <translation>לא פעיל</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="113"/>
        <source>active</source>
        <translation>פעיל</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Int</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>minimum</source>
        <translation>מינימום</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>maximum</source>
        <translation>מקסימום</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="36"/>
        <source>Integer range:</source>
        <translation>טווח מספר:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="37"/>
        <source>Decibel range:</source>
        <translation>טווח דציבל:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="125"/>
        <source>Channel %1</source>
        <translation>ערוץ %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="126"/>
        <source>Index %1</source>
        <translation>מדד %1</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Unsupported</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/unsupported.cpp" line="20"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>אלמנטים של הטיפוס %1 אינם נתמכים</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Int_Proxy::Column</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="13"/>
        <source>%1 dB</source>
        <extracomment>Decibel value string template</extracomment>
        <translation>%1 דציבל</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="15"/>
        <source>%1 %</source>
        <extracomment>Percent value string template</extracomment>
        <translation></translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Mixer</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="32"/>
        <source>Element name</source>
        <translation>שם אלמנט</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="36"/>
        <source>Joined</source>
        <translation>משולבים</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="37"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="108"/>
        <source>Element index</source>
        <translation>מפתח אלמנט</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="38"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="41"/>
        <source>Channel %1</source>
        <translation>ערוץ %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="40"/>
        <source>Ch. %1</source>
        <translation>ער. %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="45"/>
        <source>Index: %1</source>
        <translation>מדד: %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="46"/>
        <source>Channel: %2</source>
        <translation>ערוץ: %2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="101"/>
        <source>Index:</source>
        <translation>מדד:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="102"/>
        <source>Device:</source>
        <translation>התקן:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="103"/>
        <source>Flags:</source>
        <translation>דגלים:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="104"/>
        <source>Channels:</source>
        <translation>ערוצים:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="105"/>
        <source>Num. id:</source>
        <translation>מזהה מס׳:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="109"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="125"/>
        <source>Device</source>
        <translation>התקן</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="110"/>
        <source>Flags</source>
        <translation>דגלים</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="111"/>
        <source>Channel count</source>
        <translation>ספירת ערוץ</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="112"/>
        <source>Numeric Id</source>
        <translation>מזהה מספרי</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="126"/>
        <source>Subdevice</source>
        <translation>התקן משנה</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="295"/>
        <source>No element selected</source>
        <translation>לא נבחר אלמנט</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Table_Model</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="21"/>
        <source>%1,%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="29"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="30"/>
        <source>Element name</source>
        <translation>שם אלמנט</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="33"/>
        <source>Idx</source>
        <extracomment>Idx - abbreviation for Index</extracomment>
        <translation>מדד</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="34"/>
        <source>Element index</source>
        <translation>מפתח אלמנט</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="36"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="37"/>
        <source>Device</source>
        <translation>התקן</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="39"/>
        <source>Type</source>
        <translation>טיפוס</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="40"/>
        <source>Element type</source>
        <translation>טיפוס אלמנט</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="43"/>
        <source>Ch.</source>
        <extracomment>Ch. - abbreviation for Channel</extracomment>
        <translation>ער.</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="44"/>
        <source>Channel count</source>
        <translation>ספירת ערוץ</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="46"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="47"/>
        <source>Flags</source>
        <translation>דגלים</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="49"/>
        <source>Id</source>
        <translation>מזהה</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="50"/>
        <source>Numeric Id</source>
        <translation>מזהה מספרי</translation>
    </message>
</context>
<context>
    <name>MWdg::Inputs_Setup</name>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="22"/>
        <source>s</source>
        <extracomment>&quot;s&quot; is an abbreviation for &quot;split&quot; or &quot;separate&quot;. Something like &quot;j&quot; for &quot;joined&quot; or &quot;c&quot; for &quot;channel&quot; may be appropriate, too.</extracomment>
        <translatorcomment>פ</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="25"/>
        <source>l</source>
        <extracomment>&quot;l&quot; is an abbreviation for &quot;level&quot;</extracomment>
        <translatorcomment>א</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="28"/>
        <source>m</source>
        <extracomment>&quot;m&quot; is an abbreviation for &quot;mute&quot;</extracomment>
        <translatorcomment>ש</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="32"/>
        <source>p</source>
        <extracomment>&quot;p&quot; is an abbreviation for &quot;playback&quot;</extracomment>
        <translatorcomment>פ</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="36"/>
        <source>c</source>
        <extracomment>&quot;c&quot; is an abbreviation for &quot;capture&quot; or &quot;record&quot;</extracomment>
        <translatorcomment>ל</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="42"/>
        <source>Split &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>פצל &amp;ערוצים</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="46"/>
        <source>Join &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translatorcomment>מזג</translatorcomment>
        <translation>שלב &amp;ערוצים</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="50"/>
        <source>&amp;Level channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>&amp;אזן ערוצים</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="21"/>
        <source>Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="22"/>
        <source>Capture</source>
        <translation type="unfinished">לכידה</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Settings_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="239"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="241"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="275"/>
        <source>Sliders</source>
        <translation type="unfinished">מחוונים</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="319"/>
        <source>Switches</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Mixer</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="64"/>
        <source>&amp;Mute</source>
        <translation>&amp;השתק</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="65"/>
        <source>&amp;Mute all</source>
        <translation>&amp;השתק הכל</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="66"/>
        <source>Un&amp;mute</source>
        <translation>איין &amp;השתקה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="67"/>
        <source>Un&amp;mute all</source>
        <translatorcomment>&amp;איין השתקה מוחלטת</translatorcomment>
        <translation>איין &amp;השתק הכל</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="68"/>
        <source>Toggle &amp;mutes</source>
        <translatorcomment>השתקות</translatorcomment>
        <translation>החלף &amp;השתק</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="26"/>
        <source>Playback slider</source>
        <translation type="unfinished">מחוון פס קול</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="27"/>
        <source>Capture slider</source>
        <translation type="unfinished">מחוון לכידה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="28"/>
        <source>Playback switch</source>
        <translation type="unfinished">מתג פס קול</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="29"/>
        <source>Capture switch</source>
        <translation type="unfinished">מתג לכידה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="119"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxy::Column</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="17"/>
        <source>%1 dB</source>
        <translation>‎%1 דציבל</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="18"/>
        <source>%1 %</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Switches::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="25"/>
        <source>Playback selection</source>
        <translation type="unfinished">מבחר פס קול</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="26"/>
        <source>Capture selection</source>
        <translation type="unfinished">מבחר לכידה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="27"/>
        <source>Playback switch</source>
        <translation type="unfinished">מתג פס קול</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="28"/>
        <source>Capture switch</source>
        <translation type="unfinished">מתג לכידה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="147"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Slider_Status_Widget</name>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="23"/>
        <source>Slider value</source>
        <translation>ערך מחוון</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="32"/>
        <source>Value</source>
        <translation>ערך</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="35"/>
        <source>Max.</source>
        <translation>מקס.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="36"/>
        <source>Maximum</source>
        <translation>מקסימום</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="39"/>
        <source>Min.</source>
        <translation>מינ.</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="41"/>
        <source>Minimum</source>
        <translation>מינימום</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="46"/>
        <source>Volume</source>
        <translation>עוצמה</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="52"/>
        <source>Current volume</source>
        <translation>עוצמה נוכחית</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="60"/>
        <source>Volume maximum</source>
        <translation>עוצמה מקסימום</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="65"/>
        <source>Volume minimum</source>
        <translation>עוצמה מינימום</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="72"/>
        <source>Decibel</source>
        <translation>דציבל</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="78"/>
        <source>Current Decibel value</source>
        <translation>ערך דציבל נוכחי</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="88"/>
        <source>Decibel maximum</source>
        <translation>דציבל מקסימום</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="93"/>
        <source>Decibel minimum</source>
        <translation>דציבל מינימום</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="98"/>
        <source>Element name</source>
        <translation>שם אלמנט</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="322"/>
        <source>Close slider value display</source>
        <translation>סגור תצוגת ערך מחוון</translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="17"/>
        <source>Show playback</source>
        <translation type="unfinished">הצג פס קול</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="18"/>
        <source>Show capture</source>
        <translation type="unfinished">הצג לכידה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="21"/>
        <source>Show playback controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="22"/>
        <source>Show capture controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection_Actions</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="17"/>
        <source>Show &amp;playback controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="18"/>
        <source>Show &amp;capture controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::User_Device_Input</name>
    <message>
        <location filename="../../shared/src/mwdg/user_device_input.cpp" line="20"/>
        <source>User device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="33"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="40"/>
        <source>&amp;Fullscreen mode</source>
        <translation>&amp;מצב מסך מלא</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="34"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="41"/>
        <source>Exit &amp;fullscreen mode</source>
        <translation>בטל &amp;מצב מסך מלא</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="25"/>
        <location filename="../../qashctl/src/main_window.cpp" line="102"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="149"/>
        <source>&amp;Quit</source>
        <translation>י&amp;ציאה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="155"/>
        <source>&amp;Settings</source>
        <translation>&amp;הגדרות</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="156"/>
        <source>Ctrl+s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="30"/>
        <location filename="../../qashctl/src/main_window.cpp" line="127"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="164"/>
        <source>&amp;Refresh</source>
        <translation>&amp;רענן</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="109"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="172"/>
        <source>Show &amp;device selection</source>
        <translatorcomment>מבחר (של) התקן</translatorcomment>
        <translation>&amp;הצג מבחר התקנים</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="41"/>
        <location filename="../../qashctl/src/main_window.cpp" line="146"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="224"/>
        <source>&amp;File</source>
        <translation>&amp;קובץ</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="47"/>
        <location filename="../../qashctl/src/main_window.cpp" line="152"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="230"/>
        <source>&amp;View</source>
        <translation>&amp;תצוגה</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="53"/>
        <location filename="../../qashctl/src/main_window.cpp" line="161"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="241"/>
        <source>&amp;Help</source>
        <translation>&amp;עזרה</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="217"/>
        <source>Key</source>
        <translation>מפתח</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="219"/>
        <source>Value</source>
        <translation>ערך</translation>
    </message>
</context>
<context>
    <name>QSnd::HCtl::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/mixer.cpp" line="62"/>
        <source>Empty device name</source>
        <translation>שם התקן ריק</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer/mixer.cpp" line="81"/>
        <source>Empty device name</source>
        <translation>שם התקן ריק</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixers_Model</name>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="272"/>
        <source>Default</source>
        <translation type="unfinished">משתמט</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="276"/>
        <source>User defined</source>
        <translation type="unfinished">מוגדר משתמש</translation>
    </message>
</context>
<context>
    <name>Tray::Icon</name>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="28"/>
        <source>&amp;Show mixer</source>
        <translation>&amp;הצג מערבל</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="29"/>
        <source>Ctrl+s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="35"/>
        <source>&amp;Close %1</source>
        <extracomment>%1 will be replaced with the program title</extracomment>
        <translation>&amp;סגור את %1</translation>
    </message>
</context>
<context>
    <name>Tray::Notifier</name>
    <message>
        <location filename="../../qasmixer/src/tray/notifier.cpp" line="24"/>
        <source>Open mixer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tray::Shared</name>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="21"/>
        <source>Volume is at %1 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="22"/>
        <source>Volume change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="23"/>
        <source>Muted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Alsa_Config_View</name>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="23"/>
        <source>ALSA configuration</source>
        <translation>תצורת ALSA</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="27"/>
        <source>&amp;Expand</source>
        <translation>ה&amp;רחבה</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="28"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;צמצום</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="29"/>
        <source>&amp;Sort</source>
        <translation>&amp;סדר</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="31"/>
        <source>Depth:</source>
        <translation>עומק:</translation>
    </message>
</context>
<context>
    <name>Views::Basic_Dialog</name>
    <message>
        <location filename="../../shared/src/views/basic_dialog.cpp" line="66"/>
        <source>&amp;Close</source>
        <translation>&amp;סגור</translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_Bar</name>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="23"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">&amp;הגדרות</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="26"/>
        <source>Show device settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_View</name>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="25"/>
        <source>&amp;Close device selection</source>
        <translation>&amp;סגור מבחר התקנים</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="56"/>
        <source>Mixer device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Device_Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="35"/>
        <source>Device settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="74"/>
        <source>Sort by playback and capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="88"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="111"/>
        <source>Address</source>
        <translation type="unfinished">כתובת</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="112"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="113"/>
        <source>Id</source>
        <translation type="unfinished">מזהה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="114"/>
        <source>Name</source>
        <translation type="unfinished">שם</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="115"/>
        <source>Long name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="116"/>
        <source>Mixer name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="117"/>
        <source>Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="118"/>
        <source>Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="133"/>
        <source>Information</source>
        <translation type="unfinished">מידע</translation>
    </message>
</context>
<context>
    <name>Views::Info_Dialog</name>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="40"/>
        <location filename="../../shared/src/views/info_dialog.cpp" line="62"/>
        <source>About</source>
        <translation>אודות</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="66"/>
        <source>%1 is a collection of desktop applications for the Linux sound system %2.</source>
        <translation>‏%1 הינו אוסף של יישומים שולחניים עבור מערכת הצלילים של Linux ‏%2.</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="76"/>
        <source>Internet</source>
        <translation>‎מרשתת</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="80"/>
        <source>Project page</source>
        <translation>עמוד פרויקט</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="108"/>
        <source>Developers</source>
        <translation>‎מפתחים</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="130"/>
        <source>Translators</source>
        <translation>‎מתרגמים</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="189"/>
        <source>Information</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="190"/>
        <source>People</source>
        <translation>אנשים</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="191"/>
        <source>License</source>
        <translation>רשיון</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="118"/>
        <source>Contributors</source>
        <translation>‎תורמים</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="20"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="29"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="168"/>
        <source>The license file %1 is not available.</source>
        <translation>קובץ הרשיון %1 אינו זמין.</translation>
    </message>
</context>
<context>
    <name>Views::Message_Widget</name>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="31"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>התקן מערבל לא היה ניתן לפתיחה</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="32"/>
        <source>No device selected</source>
        <translation>לא נבחר התקן</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="68"/>
        <source>Function</source>
        <translation>פונקציה</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="69"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="70"/>
        <source>Error</source>
        <translation>שגיאה</translation>
    </message>
</context>
<context>
    <name>Views::Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="24"/>
        <source>Settings</source>
        <translation>הגדרות</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="105"/>
        <source>Startup</source>
        <translation>הפעלה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="112"/>
        <source>Sliders</source>
        <translation>מחוונים</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="153"/>
        <source>Appearance</source>
        <translation>מראה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="216"/>
        <source>Input</source>
        <translation>קלט</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="358"/>
        <source>System tray</source>
        <translation>מגש מערכת</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="49"/>
        <source>Startup mixer device</source>
        <translation>התקן מערבל הפעלה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="54"/>
        <source>From last session</source>
        <translation>מן סשן קודם</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="115"/>
        <source>Show slider value labels</source>
        <translation>הצג ערך תוויות מחוון</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="127"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="129"/>
        <source>Show tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="160"/>
        <source>Mouse wheel</source>
        <translation>גלגל עכבר</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="163"/>
        <source>Number of turns for a slider change from 0% to 100%</source>
        <translation>מספר של סיבובים לשינוי מחוון מן 0% עד 100%</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="222"/>
        <source>Show tray icon</source>
        <translation>הצג סמל מגש</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="223"/>
        <source>Close to tray</source>
        <translation>סגור אל מגש</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="236"/>
        <source>System tray usage</source>
        <translation>שימוש במגש מערכת</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="245"/>
        <source>Notification balloon</source>
        <translation>בלון התראה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="249"/>
        <source>Show balloon on a volume change</source>
        <translation>הצג בלון בעת שינוי עוצמה</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="259"/>
        <source>Balloon lifetime</source>
        <translation>תוכלת חיי בלון</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="262"/>
        <source>ms</source>
        <extracomment>ms - abbreviation for milliseconds</extracomment>
        <translation>מ״ש</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="295"/>
        <source>Mini mixer device</source>
        <translatorcomment>מיניאטורי</translatorcomment>
        <translation>התקן מערבל מוקטן</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="53"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="297"/>
        <source>Default</source>
        <translation>משתמט</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="299"/>
        <source>Current (same as in main mixer window)</source>
        <translation>נוכחי (זהה כמו אצל חלון מערבל ראשי)</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="55"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="300"/>
        <source>User defined</source>
        <translation>מוגדר משתמש</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="64"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="309"/>
        <source>User device:</source>
        <translation>התקן משתמש:</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="65"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="310"/>
        <source>e.g. hw:0</source>
        <translation>לדוגמא hw:0</translation>
    </message>
</context>
</TS>
