<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>ALSA::CTL_Arg_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>CARD</source>
        <translation>Karta</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>SOCKET</source>
        <translation>ZÁSUVKA</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>CTL</source>
        <translation>CTL</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>CARD</source>
        <translation>Karta</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>HWDEP</source>
        <translation>Zařízení</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>MIXER</source>
        <translation>Směšovač</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>PCM</source>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>RAWMIDI</source>
        <translation>SurovéMIDI</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="89"/>
        <source>TIMER</source>
        <translation>Časomíra</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="90"/>
        <source>SEQUENCER</source>
        <translation>Sekvencer</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="91"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="81"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>NONE</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>BOOLEAN</source>
        <translation>Booleánský</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>INTEGER</source>
        <translation>Celočíselný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>ENUMERATED</source>
        <translation>Vyjmenovaný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>BYTES</source>
        <translation>Byty</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="98"/>
        <source>IEC958</source>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="99"/>
        <source>INTEGER64</source>
        <translation>Integer64</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="100"/>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="46"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Front Left</source>
        <translation>Vpředu vlevo</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="75"/>
        <source>Front Right</source>
        <translation>Vpředu vpravo</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Rear Left</source>
        <translation>Vzadu vlevo</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Rear Right</source>
        <translation>Vzadu vpravo</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="78"/>
        <source>Front Center</source>
        <translation>Vpředu uprostřed</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="79"/>
        <source>Woofer</source>
        <translation>Basový reproduktor</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Side Left</source>
        <translation>Strana vlevo</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Side Right</source>
        <translation>Strana vpravo</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Center</source>
        <translation>Vzadu uprostřed</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Master</source>
        <translation>Hlavní</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Speaker</source>
        <translation>Reproduktor</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Headphone</source>
        <translation>Sluchátka</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Headphone LFE</source>
        <translation>Sluchátka LFE</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Headphone Center</source>
        <translation>Sluchátka uprostřed</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="25"/>
        <source>Front</source>
        <translation>Vpředu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="16"/>
        <source>Auto-Mute Mode</source>
        <translation>Režim automatického ztlumení</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="26"/>
        <source>Front Mic</source>
        <translation>Mik. vpředu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="27"/>
        <source>Front Mic Boost</source>
        <translation>Zesílení mik. vpředu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>Line Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Loopback Mixing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Mic Boost</source>
        <translation>Zesílení mik</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Rear Mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Rear Mic Boost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Surround</source>
        <translation>Okolí</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="20"/>
        <source>Center</source>
        <translation>Střed</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Side</source>
        <translation>Strana</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="19"/>
        <source>Capture</source>
        <translation>Zachytávání</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="18"/>
        <source>Beep</source>
        <translation>Pípnutí</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>PC Speaker</source>
        <translation>Reproduktor PC</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="17"/>
        <source>Bass</source>
        <translation>Basy</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Treble</source>
        <translation>Výšky</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="21"/>
        <source>Channel Mode</source>
        <translation>Kanálový režim</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="15"/>
        <source>Auto Gain Control</source>
        <translation>Ovládání auto. zesílení</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Mic Select</source>
        <translation>Výběr mikrofonu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Internal Mic</source>
        <translation>Vnitřní mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Int Mic</source>
        <translation>Vnitřní mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="23"/>
        <source>External Mic</source>
        <translation>Vnější mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="24"/>
        <source>Ext Mic</source>
        <translation>Vnější mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Mic Boost (+20dB)</source>
        <translation>Zesílení mik (+20dB)</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="22"/>
        <source>External Amplifier</source>
        <translation>Vnější zesilovač</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Input Source</source>
        <translation>Vstupní zdroj</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>1ch</source>
        <translation>1 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>2ch</source>
        <translation>2 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>3ch</source>
        <translation>3 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>4ch</source>
        <translation>4 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>5ch</source>
        <translation>5 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>6ch</source>
        <translation>6 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="58"/>
        <source>7ch</source>
        <translation>7 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="59"/>
        <source>8ch</source>
        <translation>8 kan.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>Internal Mic</source>
        <translation>Vnitřní mik.</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>Front Mic</source>
        <translation>Mik. vpředu</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>Enabled</source>
        <translation>Povoleno</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>Disabled</source>
        <translation>Zakázáno</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>Mic1</source>
        <translation>Mik. 1</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>Mic2</source>
        <translation>Mik. 2</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="68"/>
        <source>Mic3</source>
        <translation>Mik. 3</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Mic4</source>
        <translation>Mik. 4</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Rear Mic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
</context>
<context>
    <name>ALSA::Flags</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="87"/>
        <source>not readable</source>
        <translation>nečitelný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="89"/>
        <source>readable</source>
        <translation>čitelný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="95"/>
        <source>not writable</source>
        <translation>nezapisovatelný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="97"/>
        <source>writable</source>
        <translation>zapisovatelný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="103"/>
        <source>not volatile</source>
        <translation>ne nestálý</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="105"/>
        <source>volatile</source>
        <translation>nestálý</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="111"/>
        <source>not active</source>
        <translation>nečinný</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/hctl/info_db.cpp" line="113"/>
        <source>active</source>
        <translation>činný</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Int</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>minimum</source>
        <translation>Nejméně</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="35"/>
        <source>maximum</source>
        <translation>Nejvíce</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="36"/>
        <source>Integer range:</source>
        <translation>Celočíselný rozsah:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="37"/>
        <source>Decibel range:</source>
        <translation>Rozsah decibelů:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="125"/>
        <source>Channel %1</source>
        <translation>Kanál %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/int.cpp" line="126"/>
        <source>Index %1</source>
        <translation>Ukazatel %1</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Edit::Unsupported</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/edit/unsupported.cpp" line="20"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Prvky typu %1 nejsou podporovány</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Int_Proxy::Column</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="13"/>
        <source>%1 dB</source>
        <extracomment>Decibel value string template</extracomment>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/int_proxy/column.cpp" line="15"/>
        <source>%1 %</source>
        <extracomment>Percent value string template</extracomment>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Mixer</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="32"/>
        <source>Element name</source>
        <translation>Název prvku</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="36"/>
        <source>Joined</source>
        <translation>Spojeno</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="37"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="108"/>
        <source>Element index</source>
        <translation>Ukazatel prvku</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="38"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="41"/>
        <source>Channel %1</source>
        <translation>Kanál %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="40"/>
        <source>Ch. %1</source>
        <translation>Kan.%1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="45"/>
        <source>Index: %1</source>
        <translation>Ukazatel: %1</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="46"/>
        <source>Channel: %2</source>
        <translation>Kanál: %2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="101"/>
        <source>Index:</source>
        <translation>Ukazatel:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="102"/>
        <source>Device:</source>
        <translation>Zařízení:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="103"/>
        <source>Flags:</source>
        <translation>Příznaky:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="104"/>
        <source>Channels:</source>
        <translation>Kanály:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="105"/>
        <source>Num. id:</source>
        <translation>Čísel. ID:</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="109"/>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="125"/>
        <source>Device</source>
        <translation>Zařízení</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="110"/>
        <source>Flags</source>
        <translation>Příznaky</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="111"/>
        <source>Channel count</source>
        <translation>Počet kanálů</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="112"/>
        <source>Numeric Id</source>
        <translation>Číselné ID</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="126"/>
        <source>Subdevice</source>
        <translation>Podzařízení</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/mixer.cpp" line="295"/>
        <source>No element selected</source>
        <translation>Nebyl vybrán žádný prvek</translation>
    </message>
</context>
<context>
    <name>MWdg::HCtl::Table_Model</name>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="21"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="29"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="30"/>
        <source>Element name</source>
        <translation>Název prvku</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="33"/>
        <source>Idx</source>
        <extracomment>Idx - abbreviation for Index</extracomment>
        <translation>Ukaz</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="34"/>
        <source>Element index</source>
        <translation>Index prvku</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="36"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="37"/>
        <source>Device</source>
        <translation>Zařízení</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="39"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="40"/>
        <source>Element type</source>
        <translation>Typ prvku</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="43"/>
        <source>Ch.</source>
        <extracomment>Ch. - abbreviation for Channel</extracomment>
        <translation>Kan.</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="44"/>
        <source>Channel count</source>
        <translation>Počet kanálů</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="46"/>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="47"/>
        <source>Flags</source>
        <translation>Příznaky</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="49"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/mwdg/hctl/table_model.cpp" line="50"/>
        <source>Numeric Id</source>
        <translation>Číselné ID</translation>
    </message>
</context>
<context>
    <name>MWdg::Inputs_Setup</name>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="22"/>
        <source>s</source>
        <extracomment>&quot;s&quot; is an abbreviation for &quot;split&quot; or &quot;separate&quot;. Something like &quot;j&quot; for &quot;joined&quot; or &quot;c&quot; for &quot;channel&quot; may be appropriate, too.</extracomment>
        <translation>roz</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="25"/>
        <source>l</source>
        <extracomment>&quot;l&quot; is an abbreviation for &quot;level&quot;</extracomment>
        <translation>úr</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="28"/>
        <source>m</source>
        <extracomment>&quot;m&quot; is an abbreviation for &quot;mute&quot;</extracomment>
        <translation>zt</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="32"/>
        <source>p</source>
        <extracomment>&quot;p&quot; is an abbreviation for &quot;playback&quot;</extracomment>
        <translation>př</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="36"/>
        <source>c</source>
        <extracomment>&quot;c&quot; is an abbreviation for &quot;capture&quot; or &quot;record&quot;</extracomment>
        <translation>nah</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="42"/>
        <source>Split &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Rozdělit &amp;kanály</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="46"/>
        <source>Join &amp;channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>Spojit &amp;kanály</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/inputs_setup.cpp" line="50"/>
        <source>&amp;Level channels</source>
        <extracomment>Used in the context menu (slider/switch right click menu)</extracomment>
        <translation>&amp;Srovnat rozdíly kanálů</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="21"/>
        <source>Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_model.cpp" line="22"/>
        <source>Capture</source>
        <translation type="unfinished">Zachytávání</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Proxies_Settings_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="239"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="241"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="275"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/proxies_settings_model.cpp" line="319"/>
        <source>Switches</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Mixer</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="64"/>
        <source>&amp;Mute</source>
        <translation>&amp;Ztlumit</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="65"/>
        <source>&amp;Mute all</source>
        <translation>&amp;Ztlumit vše</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="66"/>
        <source>Un&amp;mute</source>
        <translation>&amp;Zrušit ztlumení</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="67"/>
        <source>Un&amp;mute all</source>
        <translation>&amp;Zrušit ztlumení u všeho</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/mixer.cpp" line="68"/>
        <source>Toggle &amp;mutes</source>
        <translation>Přepnout &amp;ztlumení</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="26"/>
        <source>Playback slider</source>
        <translation type="unfinished">Posuvník přehrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="27"/>
        <source>Capture slider</source>
        <translation type="unfinished">Posuvník nahrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="28"/>
        <source>Playback switch</source>
        <translation type="unfinished">Přepínač přehrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="29"/>
        <source>Capture switch</source>
        <translation type="unfinished">Přepínač nahrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxies_model.cpp" line="119"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Sliders::Proxy::Column</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="17"/>
        <source>%1 dB</source>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/sliders/proxy/column.cpp" line="18"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>MWdg::Mixer::Switches::Proxies_Model</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="25"/>
        <source>Playback selection</source>
        <translation type="unfinished">Výběr přehrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="26"/>
        <source>Capture selection</source>
        <translation type="unfinished">Výběr nahrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="27"/>
        <source>Playback switch</source>
        <translation type="unfinished">Přepínač přehrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="28"/>
        <source>Capture switch</source>
        <translation type="unfinished">Přepínač nahrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/mixer/switches/proxies_model.cpp" line="147"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>MWdg::Slider_Status_Widget</name>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="23"/>
        <source>Slider value</source>
        <translation>Hodnota posuvníku</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="32"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="35"/>
        <source>Max.</source>
        <translation>Nejvíce</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="36"/>
        <source>Maximum</source>
        <translation>Nejvíce</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="39"/>
        <source>Min.</source>
        <translation>Nejméně</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="41"/>
        <source>Minimum</source>
        <translation>Nejméně</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="46"/>
        <source>Volume</source>
        <translation>Hlasitost</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="52"/>
        <source>Current volume</source>
        <translation>Nynější hlasitost</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="60"/>
        <source>Volume maximum</source>
        <translation>Nejvyšší hlasitost</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="65"/>
        <source>Volume minimum</source>
        <translation>Nejnižší hlasitost</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="72"/>
        <source>Decibel</source>
        <translation>Decibel</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="78"/>
        <source>Current Decibel value</source>
        <translation>Nynější hodnota decibelů</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="88"/>
        <source>Decibel maximum</source>
        <translation>Nejvíce decibelů</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="93"/>
        <source>Decibel minimum</source>
        <translation>Nejméně decibelů</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="98"/>
        <source>Element name</source>
        <translation>Název prvku</translation>
    </message>
    <message>
        <location filename="../../shared/src/mwdg/slider_status_widget.cpp" line="322"/>
        <source>Close slider value display</source>
        <translation>Zavřít zobrazení hodnoty posuvníku</translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="17"/>
        <source>Show playback</source>
        <translation type="unfinished">Ukázat přehrávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="18"/>
        <source>Show capture</source>
        <translation type="unfinished">Ukázat zachytávání</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="21"/>
        <source>Show playback controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection.cpp" line="22"/>
        <source>Show capture controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::Stream_View_Selection_Actions</name>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="17"/>
        <source>Show &amp;playback controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/mwdg/stream_view_selection_actions.cpp" line="18"/>
        <source>Show &amp;capture controls</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MWdg::User_Device_Input</name>
    <message>
        <location filename="../../shared/src/mwdg/user_device_input.cpp" line="20"/>
        <source>User device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="33"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="40"/>
        <source>&amp;Fullscreen mode</source>
        <translation>Režim na &amp;celou obrazovku</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="34"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="41"/>
        <source>Exit &amp;fullscreen mode</source>
        <translation>Ukončit režim na ce&amp;lou obrazovku</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="25"/>
        <location filename="../../qashctl/src/main_window.cpp" line="102"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="149"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončit</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="155"/>
        <source>&amp;Settings</source>
        <translation>&amp;Nastavení</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/main_window.cpp" line="156"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="30"/>
        <location filename="../../qashctl/src/main_window.cpp" line="127"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="164"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../../qashctl/src/main_window.cpp" line="109"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="172"/>
        <source>Show &amp;device selection</source>
        <translation>Ukázat výběr &amp;zařízení</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="41"/>
        <location filename="../../qashctl/src/main_window.cpp" line="146"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="224"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="47"/>
        <location filename="../../qashctl/src/main_window.cpp" line="152"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="230"/>
        <source>&amp;View</source>
        <translation>&amp;Pohled</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/main_window.cpp" line="53"/>
        <location filename="../../qashctl/src/main_window.cpp" line="161"/>
        <location filename="../../qasmixer/src/main_window.cpp" line="241"/>
        <source>&amp;Help</source>
        <translation>&amp;Nápověda</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="217"/>
        <source>Key</source>
        <translation>Klíč</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/qsnd/alsa_config_model.cpp" line="219"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>QSnd::HCtl::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/hctl/mixer.cpp" line="62"/>
        <source>Empty device name</source>
        <translation>Prázdný název zařízení</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer::Mixer</name>
    <message>
        <location filename="../../shared/src/qsnd/mixer/mixer.cpp" line="81"/>
        <source>Empty device name</source>
        <translation>Prázdný název zařízení</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixers_Model</name>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="272"/>
        <source>Default</source>
        <translation type="unfinished">Výchozí</translation>
    </message>
    <message>
        <location filename="../../shared/src/qsnd/mixers_model.cpp" line="276"/>
        <source>User defined</source>
        <translation type="unfinished">Stanoveno uživatelem</translation>
    </message>
</context>
<context>
    <name>Tray::Icon</name>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="28"/>
        <source>&amp;Show mixer</source>
        <translation>&amp;Ukázat směšovač</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="29"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/icon.cpp" line="35"/>
        <source>&amp;Close %1</source>
        <extracomment>%1 will be replaced with the program title</extracomment>
        <translation>&amp;Zavřít %1</translation>
    </message>
</context>
<context>
    <name>Tray::Notifier</name>
    <message>
        <location filename="../../qasmixer/src/tray/notifier.cpp" line="24"/>
        <source>Open mixer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tray::Shared</name>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="21"/>
        <source>Volume is at %1 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="22"/>
        <source>Volume change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/tray/shared.cpp" line="23"/>
        <source>Muted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Alsa_Config_View</name>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="23"/>
        <source>ALSA configuration</source>
        <translation>Nastavení Alsa</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="27"/>
        <source>&amp;Expand</source>
        <translation>&amp;Rozbalit</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="28"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Složit</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="29"/>
        <source>&amp;Sort</source>
        <translation>&amp;Třídit</translation>
    </message>
    <message>
        <location filename="../../qasconfig/src/views/alsa_config_view.cpp" line="31"/>
        <source>Depth:</source>
        <translation>Hloubka:</translation>
    </message>
</context>
<context>
    <name>Views::Basic_Dialog</name>
    <message>
        <location filename="../../shared/src/views/basic_dialog.cpp" line="66"/>
        <source>&amp;Close</source>
        <translation type="unfinished">&amp;Zavřít</translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_Bar</name>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="23"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">&amp;Nastavení</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_selection_bar.cpp" line="26"/>
        <source>Show device settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Device_Selection_View</name>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="25"/>
        <source>&amp;Close device selection</source>
        <translation>&amp;Zavřít výběr zařízení</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/device_selection_view.cpp" line="56"/>
        <source>Mixer device</source>
        <translation type="unfinished">Zařízení směšovače</translation>
    </message>
</context>
<context>
    <name>Views::Device_Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="35"/>
        <source>Device settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="74"/>
        <source>Sort by playback and capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="88"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="111"/>
        <source>Address</source>
        <translation type="unfinished">Adresa</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="112"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="113"/>
        <source>Id</source>
        <translation type="unfinished">ID</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="114"/>
        <source>Name</source>
        <translation type="unfinished">Název</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="115"/>
        <source>Long name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="116"/>
        <source>Mixer name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="117"/>
        <source>Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="118"/>
        <source>Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/device_settings_dialog.cpp" line="133"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Views::Info_Dialog</name>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="40"/>
        <location filename="../../shared/src/views/info_dialog.cpp" line="62"/>
        <source>About</source>
        <translation>O programu</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="76"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="80"/>
        <source>Project page</source>
        <translation>Projektová stránka</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="108"/>
        <source>Developers</source>
        <translation>Vývojáři</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="130"/>
        <source>Translators</source>
        <translation>Překladatelé</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="189"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="190"/>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="191"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="66"/>
        <source>%1 is a collection of desktop applications for the Linux sound system %2.</source>
        <translation>%1 je sbírka programů pro linuxový zvukový systém %2.</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="20"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="29"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="118"/>
        <source>Contributors</source>
        <translation>Přispěvatelé</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/info_dialog.cpp" line="168"/>
        <source>The license file %1 is not available.</source>
        <translation>Soubor s povolením %1 není dostupný.</translation>
    </message>
</context>
<context>
    <name>Views::Message_Widget</name>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="31"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>Zařízení směšovače se nepodařilo otevřít</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="32"/>
        <source>No device selected</source>
        <translation>Nebylo vybráno žádné zařízení</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="68"/>
        <source>Function</source>
        <translation>Funkce</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="69"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../../shared/src/views/message_widget.cpp" line="70"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
</context>
<context>
    <name>Views::Settings_Dialog</name>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="24"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="105"/>
        <source>Startup</source>
        <translation>Spuštění</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="112"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="153"/>
        <source>Appearance</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="216"/>
        <source>Input</source>
        <translation>Vstup</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="358"/>
        <source>System tray</source>
        <translation>Oznamovací oblast panelu</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="49"/>
        <source>Startup mixer device</source>
        <translation>Zařízení spouštěcího směšovače</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="54"/>
        <source>From last session</source>
        <translation>Z posledního sezení</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="115"/>
        <source>Show slider value labels</source>
        <translation>Ukázat štítky s hodnotami posuvníku</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="127"/>
        <source>Controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="129"/>
        <source>Show tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="160"/>
        <source>Mouse wheel</source>
        <translation>Kolečko myši</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="163"/>
        <source>Number of turns for a slider change from 0% to 100%</source>
        <translation>Počet kroků pro změnu posuvníku z 0% na 100%</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="222"/>
        <source>Show tray icon</source>
        <translation>Ukázat ikonu v oznamovací oblasti</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="223"/>
        <source>Close to tray</source>
        <translation>Zavřít do oznamovací oblasti panelu</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="236"/>
        <source>System tray usage</source>
        <translation>Používání oznamovací oblasti panelu</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="245"/>
        <source>Notification balloon</source>
        <translation>Bublina s oznámením</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="249"/>
        <source>Show balloon on a volume change</source>
        <translation>Při změně hlasitosti ukázat bublinu</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="259"/>
        <source>Balloon lifetime</source>
        <translation>Délka života bubliny</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="262"/>
        <source>ms</source>
        <extracomment>ms - abbreviation for milliseconds</extracomment>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="295"/>
        <source>Mini mixer device</source>
        <translation>Zařízení malého směšovače</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="53"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="297"/>
        <source>Default</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="299"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Nynější (stejné jako v hlavním okně se směšovačem)</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="55"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="300"/>
        <source>User defined</source>
        <translation>Stanoveno uživatelem</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="64"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="309"/>
        <source>User device:</source>
        <translation>Uživatelské zařízení:</translation>
    </message>
    <message>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="65"/>
        <location filename="../../qasmixer/src/views/settings_dialog.cpp" line="310"/>
        <source>e.g. hw:0</source>
        <translation>Např. hw:0</translation>
    </message>
</context>
</TS>
