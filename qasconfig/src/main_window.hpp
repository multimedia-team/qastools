/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/alsa_config_model.hpp"
#include "views/alsa_config_view.hpp"
#include <QDialog>
#include <QMainWindow>
#include <QPointer>

/// @brief Main_Window
///
class Main_Window : public QMainWindow
{
  Q_OBJECT

  public:
  // -- Construction

  Main_Window ();

  ~Main_Window ();

  // -- Size hint

  QSize
  sizeHint () const override;

  // -- State store and restore

  void
  restore_state ();

  void
  save_state ();

  // -- Public slots

  Q_SLOT
  void
  refresh ();

  protected:
  // -- Protected methods

  void
  closeEvent ( QCloseEvent * event_n ) override;

  private:
  // -- Private slots

  Q_SLOT
  void
  show_dialog_about ();

  Q_SLOT
  void
  show_dialog_about_qt ();

  // -- Private methods

  void
  init_menu_bar ();

  private:
  // Attributes
  QSnd::Alsa_Config_Model _alsa_cfg_model;
  Views::Alsa_Config_View _alsa_cfg_view;

  QPointer< QDialog > _info_dialog;
};
