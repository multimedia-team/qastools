/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <memory>

// Forward declaration
class Main_Window;

/// @brief Manages all items that appear on the desktop
///
class Desktop_Items
{
  public:
  // -- Construction

  Desktop_Items ();

  ~Desktop_Items ();

  /// @brief Reads options from storage and command line
  ///
  /// @return A negative value on an error
  int
  init_settings ( int argc, char * argv[] );

  /// @brief Start the mixer window and/or tray icon
  ///
  void
  start ( bool restore_session_n = false );

  /// @brief Command line option parser
  ///
  /// @return A negative value on an error
  int
  parse_cmd_options ( int argc, char * argv[] );

  private:
  // -- Attributes
  std::unique_ptr< Main_Window > _mwin;
};
