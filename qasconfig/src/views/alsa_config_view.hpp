/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QCheckBox>
#include <QModelIndex>
#include <QPushButton>
#include <QSpinBox>
#include <QStringList>

// Forward declaration
class QAbstractItemModel;
class QSortFilterProxyModel;
class QItemSelection;
namespace QSnd
{
class Alsa_Config_Model;
}
namespace Wdg
{
class Tree_View_KV;
}

namespace Views
{

class Alsa_Config_View : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Alsa_Config_View ( QWidget * parent_n = nullptr );

  ~Alsa_Config_View ();

  // -- Model

  QSnd::Alsa_Config_Model *
  model () const
  {
    return _alsa_cfg;
  }

  void
  set_model ( QSnd::Alsa_Config_Model * model_n );

  bool
  sorting_enabled () const;

  // Public slots
  public:
  Q_SLOT
  void
  reload_config ();

  Q_SLOT
  void
  expand_to_level ( bool expanded_n );

  Q_SLOT
  void
  expand_to_level ();

  Q_SLOT
  void
  collapse_to_level ();

  Q_SLOT
  void
  set_sorting_enabled ( bool flag_n );

  // Protected slots
  protected:
  Q_SLOT
  void
  items_selected ( const QItemSelection & sel0_n,
                   const QItemSelection & sel1_n );

  protected:
  // -- Protected methods

  void
  collect_expanded ( QStringList & lst_n,
                     const QModelIndex & = QModelIndex () );

  void
  set_expanded ( QStringList & lst_n );

  void
  update_button_state ();

  private:
  // -- Attributes
  QSnd::Alsa_Config_Model * _alsa_cfg = nullptr;
  QSortFilterProxyModel * _sort_model;

  // Widgets
  Wdg::Tree_View_KV * _tree_view;
  QPushButton _btn_expand;
  QPushButton _btn_collapse;
  QCheckBox _btn_sort;
  QSpinBox _expand_depth;

  QStringList _expanded_items;
};

} // namespace Views
