/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "static_tree.hpp"
#include <QAbstractItemModel>

/// @brief Static_Tree_Model
///
class Static_Tree_Model : public QAbstractItemModel
{
  // Public typedefs
  public:
  using Node = Static_Tree::Node;

  public:
  // -- Construction

  Static_Tree_Model ();

  ~Static_Tree_Model ();

  std::size_t
  num_columns () const
  {
    return _num_columns;
  }

  const Node *
  get_node ( const QModelIndex & index_n ) const;

  QModelIndex
  index ( const Node * node_n ) const;

  // Model methods

  QModelIndex
  index ( int row,
          int column = 0,
          const QModelIndex & parent = QModelIndex () ) const override;

  QModelIndex
  parent ( const QModelIndex & index ) const override;

  int
  rowCount ( const QModelIndex & parent = QModelIndex () ) const override;

  int
  columnCount ( const QModelIndex & parent = QModelIndex () ) const override;

  protected:
  // -- Protected methods

  Static_Tree &
  stree ()
  {
    return _stree;
  }

  const Static_Tree &
  stree () const
  {
    return _stree;
  }

  private:
  // -- Attributes
  Static_Tree _stree;
  std::size_t _num_columns = 2;
};
