/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QAbstractTableModel>
#include <QStringList>
#include <vector>

// Forward declaration
namespace QSnd::HCtl
{
class Mixer;
class Elem;
class Elem_Group;
} // namespace QSnd::HCtl

namespace MWdg::HCtl
{

/// @brief Table_Model
///
class Table_Model : public QAbstractTableModel
{
  Q_OBJECT;

  public:
  // -- Construction

  Table_Model ( QObject * parent_n = nullptr );

  ~Table_Model ();

  // -- Mixer class

  QSnd::HCtl::Mixer *
  snd_mixer () const
  {
    return _snd_mixer;
  }

  void
  set_snd_mixer ( QSnd::HCtl::Mixer * snd_mixer_n );

  // Interface type index

  unsigned int
  iface_type_idx () const
  {
    return _iface_type_idx;
  }

  void
  set_iface_type_idx ( unsigned int type_n );

  QSnd::HCtl::Elem *
  elem ( const QModelIndex & idx_n ) const;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const;

  int
  columnCount ( const QModelIndex & parent_n = QModelIndex () ) const;

  QVariant
  data ( const QModelIndex & index_n, int role_n = Qt::DisplayRole ) const;

  QVariant
  headerData ( int section_n,
               Qt::Orientation orientation_n,
               int role_n = Qt::DisplayRole ) const;

  protected:
  // -- Protected methods

  void
  clear ();

  void
  load ();

  private:
  // -- Attributes
  QSnd::HCtl::Mixer * _snd_mixer;

  std::vector< QSnd::HCtl::Elem * > _elems;

  unsigned int _iface_type_idx;

  QString _str_dev_mask;
  QString _ttip_dev;
  QString _ttip_name_mask;

  QString _col_names[ 7 ];
  QString _col_ttips[ 7 ];

  const unsigned int _num_columns;
  const Qt::Alignment _align_cc;
  const Qt::Alignment _align_lc;
};

} // namespace MWdg::HCtl
