/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "editor.hpp"
#include "mwdg/hctl/editor_data.hpp"
#include "mwdg/hctl/proxy/group.hpp"
#include "mwdg/mixer_styles.hpp"
#include "qsnd/hctl/elem.hpp"
#include "wdg/ds/switch.hpp"
#include "wdg/ds/widget_types.hpp"
#include "wdg/label_width.hpp"
#include <QCheckBox>
#include <iostream>

namespace MWdg::HCtl
{

Editor::Editor ( MWdg::HCtl::Editor_Data * data_n, QWidget * parent_n )
: QWidget ( parent_n )
, _editor_data ( data_n )
, _inputs_setup ( 0 )
{
}

Editor::~Editor () = default;

void
Editor::set_inputs_setup ( const MWdg::Inputs_Setup * setup_n )
{
  _inputs_setup = setup_n;
}

void
Editor::update_proxies_values ()
{
  // Dummy
}

QGridLayout *
Editor::create_channel_grid ( const std::vector< QWidget * > & items_n,
                              bool bold_labels_n )
{
  QGridLayout * lay_grid ( new QGridLayout () );
  lay_grid->setContentsMargins ( 0, 0, 0, 0 );

  if ( items_n.size () == 0 ) {
    return lay_grid;
  }

  const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );

  std::size_t num = items_n.size ();
  unsigned int lay_cols = 1;
  for ( std::size_t ii = 1; ii <= num; ii *= 2 ) {
    if ( ii * ii >= num ) {
      lay_cols = ii;
      break;
    }
  }

  const QString val ( "%1" );
  for ( std::size_t cii = 0; cii < num; ++cii ) {
    QWidget * wdg_input ( items_n[ cii ] );
    const QString str_idx ( val.arg ( cii ) );

    Wdg::Label_Width * wdg_label ( new Wdg::Label_Width );
    wdg_label->set_min_text ( val.arg ( 999 ) );
    wdg_label->setText ( str_idx );
    wdg_label->setToolTip (
        editor_data ()->ttip_grid_lbl_channel.arg ( str_idx ) );
    wdg_label->setAlignment ( align_cc );
    wdg_label->setEnabled ( wdg_input->isEnabled () );

    if ( bold_labels_n ) {
      QFont fnt ( wdg_label->font () );
      fnt.setBold ( true );
      wdg_label->setFont ( fnt );
    }

    unsigned int lay_row = ( cii / lay_cols );
    unsigned int lay_col = ( cii % lay_cols );
    lay_grid->addWidget ( wdg_label, lay_row * 2, lay_col, align_cc );
    lay_grid->addWidget ( wdg_input, lay_row * 2 + 1, lay_col, align_cc );
  }

  return lay_grid;
}

unsigned int
Editor::elem_style_id ( const QSnd::HCtl::Elem * elem_n ) const
{
  unsigned int style_id ( 0 );
  {
    const QString ename ( elem_n->elem_name () );
    if ( ename.contains ( "playback", Qt::CaseInsensitive ) ) {
      style_id = MWdg::Mixer_Style::PLAYBACK;
    } else if ( ename.contains ( "capture", Qt::CaseInsensitive ) ) {
      style_id = MWdg::Mixer_Style::CAPTURE;
    } else if ( ename.contains ( "input", Qt::CaseInsensitive ) ) {
      style_id = MWdg::Mixer_Style::CAPTURE;
    } else if ( ename.contains ( "mic", Qt::CaseInsensitive ) ) {
      style_id = MWdg::Mixer_Style::CAPTURE;
    } else {
      style_id = MWdg::Mixer_Style::PLAYBACK;
    }
  }
  return style_id;
}

QWidget *
Editor::create_small_joined_switch ( MWdg::HCtl::Proxy::Group * pgroup_n,
                                     QSnd::HCtl::Elem * elem_n )
{
  Wdg::DS::Switch * btn = new Wdg::DS::Switch ( editor_data ()->wdg_style_db,
                                                editor_data ()->image_alloc );
  btn->setChecked ( elem_n->values_equal () );
  btn->setToolTip ( editor_data ()->str_joined );
  btn->set_variant_id ( Wdg::DS::SV_SVG_JOINED );

  connect ( btn,
            SIGNAL ( toggled ( bool ) ),
            pgroup_n,
            SLOT ( set_joined ( bool ) ) );

  return btn;
}

} // namespace MWdg::HCtl
