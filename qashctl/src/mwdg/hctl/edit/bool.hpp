/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/editor.hpp"
#include <vector>

// Forward declaration
namespace MWdg::HCtl::Proxy
{
class Group;
}

namespace MWdg::HCtl::Edit
{

/// @brief Bool
///
class Bool : public MWdg::HCtl::Editor
{
  Q_OBJECT

  public:
  // -- Construction

  Bool ( MWdg::HCtl::Editor_Data * data_n, QWidget * parent_n = nullptr );

  ~Bool ();

  // -- Setup

  void
  setup_single ();

  void
  setup_multi ();

  MWdg::HCtl::Proxy::Group *
  create_proxies_group ( QSnd::HCtl::Elem * elem_n );

  // -- Public slots

  Q_SLOT
  void
  update_proxies_values ();

  private:
  // -- Attributes
  std::vector< MWdg::HCtl::Proxy::Group * > _proxies_groups;
};

} // namespace MWdg::HCtl::Edit
