/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/editor.hpp"
#include "wdg/scroll_area_horizontal.hpp"
#include "wdg/sliders_pad/pad.hpp"
#include <QAction>
#include <QMenu>
#include <QPointer>
#include <memory>
#include <vector>

// Forward declaration
namespace MWdg::HCtl
{
class Slider_Status_Widget;
} // namespace MWdg::HCtl
namespace MWdg::HCtl::Int_Proxy
{
class Group;
class Slider;
} // namespace MWdg::HCtl::Int_Proxy

namespace MWdg::HCtl::Edit
{

/// @brief Int
///
class Int : public MWdg::HCtl::Editor
{
  Q_OBJECT

  public:
  // -- Construction

  Int ( MWdg::HCtl::Editor_Data * data_n, QWidget * parent_n = nullptr );

  ~Int ();

  void
  clear ();

  void
  rebuild ();

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  void
  update_proxies_values ();

  QString
  integer_string ( long value_n ) const;

  QString
  dB_string ( double value_n ) const;

  bool
  event ( QEvent * event_n ) override;

  bool
  eventFilter ( QObject * watched_n, QEvent * event_n ) override;

  // Protected slots
  protected:
  Q_SLOT
  void
  update_focus_proxies ();

  Q_SLOT
  void
  context_menu_cleanup_behind ();

  Q_SLOT
  void
  action_toggle_joined ();

  Q_SLOT
  void
  action_level_volumes ();

  Q_SLOT
  void
  footer_label_selected ( unsigned int group_idx_n, unsigned int column_idx_n );

  private:
  // -- Private methods

  void
  setup_single ();

  void
  setup_multi ();

  void
  setup_widgets ();

  QLayout *
  create_range_label ();

  Wdg::Pad_Proxy::Group *
  create_proxies_group ( QSnd::HCtl::Elem * elem_n, bool multi_n = false );

  // Context menu

  bool
  context_menu_start ( const QPoint & pos_n );

  /// @return The number of visible actions
  unsigned int
  context_menu_update ();

  private:
  // -- Attributes
  std::vector< MWdg::HCtl::Int_Proxy::Slider * > _proxies_slider;
  std::vector< Wdg::Pad_Proxy::Group * > _proxies_groups;

  std::unique_ptr< Wdg::Sliders_Pad::Pad > _sliders_pad;
  std::unique_ptr< Wdg::Scroll_Area_Horizontal > _scroll_area;

  // Slider status widget
  QPointer< MWdg::HCtl::Slider_Status_Widget > _status_wdg;
  unsigned int _status_group_idx;
  unsigned int _status_column_idx;

  // Context menu
  QPointer< MWdg::HCtl::Int_Proxy::Group > _focus_proxies_group;
  unsigned int _focus_proxy_column;

  QPointer< MWdg::HCtl::Int_Proxy::Group > _act_proxies_group;
  unsigned int _act_proxy_column;

  QMenu _cmenu;
  QAction _act_toggle_joined;
  QAction _act_level_channels;

  QString _range_mask;
  QString _range_ttip;
  QString _str_int_range;
  QString _str_dB_range;
};

} // namespace MWdg::HCtl::Edit
