/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "group.hpp"
#include "mwdg/event_types.hpp"
#include "mwdg/hctl/int_proxy/column.hpp"
#include "mwdg/hctl/int_proxy/slider.hpp"
#include "qsnd/hctl/elem.hpp"
#include "wdg/pad_proxy/switch.hpp"
#include <QCoreApplication>
#include <QEvent>
#include <cassert>
#include <iostream>

namespace MWdg::HCtl::Int_Proxy
{

Group::Group ( QSnd::HCtl::Elem * snd_elem_n, QObject * parent_n )
: Wdg::Pad_Proxy::Group ( parent_n )
, _snd_elem ( snd_elem_n )
, _notify_value_change ( false )
{
  assert ( _snd_elem != nullptr );

  connect ( _snd_elem,
            SIGNAL ( sig_values_changed () ),
            this,
            SLOT ( update_values () ) );
}

Group::~Group () = default;

bool
Group::is_joined () const
{
  bool res ( false );
  if ( num_columns () > 0 ) {
    MWdg::HCtl::Int_Proxy::Column * col ( mcolumn ( 0 ) );
    if ( col->has_slider () ) {
      res = col->mslider_proxy ()->is_joined ();
    }
  }
  return res;
}

inline MWdg::HCtl::Int_Proxy::Column *
Group::mcolumn ( std::size_t idx_n ) const
{
  return static_cast< MWdg::HCtl::Int_Proxy::Column * > ( column ( idx_n ) );
}

void
Group::set_joined ( bool flag_n )
{
  if ( num_columns () < 2 ) {
    return;
  }

  for ( std::size_t cii = 0; cii < num_columns (); ++cii ) {
    MWdg::HCtl::Int_Proxy::Column * col = mcolumn ( cii );
    if ( col->has_slider () ) {
      MWdg::HCtl::Int_Proxy::Slider * pslider = col->mslider_proxy ();
      pslider->set_joined ( flag_n );
      bool is_writable = ( pslider->snd_elem () != nullptr ) &&
                         ( pslider->snd_elem ()->is_writable () );
      if ( is_writable ) {
        pslider->set_enabled ( ( cii == 0 ) || !flag_n );
      }
      if ( is_writable && flag_n && ( cii == 0 ) ) {
        pslider->snd_elem ()->level_integers ();
      }
    }
  }

  {
    MWdg::HCtl::Int_Proxy::Column * col = mcolumn ( 0 );
    if ( col->has_switch () ) {
      col->switch_proxy ()->set_switch_state ( flag_n );
    }
  }
}

bool
Group::volumes_equal () const
{
  return _snd_elem->integers_equal ();
}

void
Group::level_volumes ( unsigned int column_n )
{
  if ( num_columns () < 2 ) {
    return;
  }
  if ( column_n >= num_columns () ) {
    column_n = 0;
  }
  MWdg::HCtl::Int_Proxy::Column * col = mcolumn ( column_n );
  if ( col->has_slider () ) {
    MWdg::HCtl::Int_Proxy::Slider * psl = col->mslider_proxy ();
    if ( psl->snd_elem ()->is_writable () ) {
      psl->snd_elem ()->set_integer_all ( psl->integer_value () );
    }
  }
}

void
Group::set_notify_value_change ( bool flag_n )
{
  _notify_value_change = flag_n;
}

void
Group::update_values ()
{
  for ( std::size_t cii = 0; cii < num_columns (); ++cii ) {
    MWdg::HCtl::Int_Proxy::Column * col ( mcolumn ( cii ) );
    if ( col->has_slider () ) {
      col->slider_proxy ()->update_value_from_source ();
    }
  }

  // Notify parent on demand
  if ( notify_value_change () && ( parent () != 0 ) ) {
    QEvent ev_req ( MWdg::evt_values_changed );
    QCoreApplication::sendEvent ( parent (), &ev_req );
  }
}

} // namespace MWdg::HCtl::Int_Proxy
