/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/int_proxy/slider.hpp"
#include "wdg/pad_proxy/column.hpp"
#include <QLocale>
#include <QObject>

namespace MWdg::HCtl::Int_Proxy
{

/// @brief Column
///
class Column : public Wdg::Pad_Proxy::Column
{
  Q_OBJECT

  public:
  // -- Construction

  Column ();

  ~Column ();

  MWdg::HCtl::Int_Proxy::Slider *
  mslider_proxy () const
  {
    return static_cast< MWdg::HCtl::Int_Proxy::Slider * > ( slider_proxy () );
  }

  // -- Value string

  QString
  value_string () const;

  QString
  value_min_string () const;

  QString
  value_max_string () const;

  protected:
  // -- Protected methods

  void
  slider_proxy_changed ();

  void
  show_value_string_changed ();

  void
  update_connections ();

  void
  integer_string ( QString & str_n, long value_n ) const;

  void
  dB_string ( QString & str_n, long dB_value_n ) const;

  void
  percent_string ( QString & str_n, int permille_n ) const;

  private:
  // -- Attributes
  QString _str_value_dB;
  QString _str_value_pc;
  QLocale _loc;
};

} // namespace MWdg::HCtl::Int_Proxy
