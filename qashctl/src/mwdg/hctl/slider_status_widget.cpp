/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "slider_status_widget.hpp"
#include "qsnd/hctl/elem.hpp"
#include "wdg/pad_proxy/column.hpp"
#include "wdg/pad_proxy/group.hpp"
#include "wdg/sliders_pad/pad.hpp"
#include <iostream>

namespace MWdg::HCtl
{

Slider_Status_Widget::Slider_Status_Widget ( QWidget * parent_n )
: MWdg::Slider_Status_Widget ( parent_n )
, _proxy_slider ( nullptr )
{
}

Slider_Status_Widget::~Slider_Status_Widget () = default;

void
Slider_Status_Widget::select_slider ( unsigned int grp_idx_n,
                                      unsigned int col_idx_n )
{
  if ( sliders_pad () != nullptr ) {
    MWdg::HCtl::Int_Proxy::Slider * proxy_new ( _proxy_slider );
    if ( grp_idx_n < sliders_pad ()->num_proxies_groups () ) {
      Wdg::Pad_Proxy::Group * pgroup (
          sliders_pad ()->proxies_group ( grp_idx_n ) );

      if ( col_idx_n < pgroup->num_sliders () ) {
        proxy_new = dynamic_cast< MWdg::HCtl::Int_Proxy::Slider * > (
            pgroup->column ( col_idx_n )->slider_proxy () );
      }
    }
    set_slider_proxy ( proxy_new );
  }
}

void
Slider_Status_Widget::proxy_destroyed ()
{
  set_slider_proxy ( nullptr );
}

void
Slider_Status_Widget::set_slider_proxy (
    MWdg::HCtl::Int_Proxy::Slider * proxy_n )
{
  // std::cout << "Slider_Status_Widget::set_slider_proxy " <<
  //  proxy_n  << "\n";

  if ( _proxy_slider != proxy_n ) {

    // Disconnect previous proxy
    if ( _proxy_slider != nullptr ) {
      if ( _proxy_slider->snd_elem () != nullptr ) {
        disconnect ( _proxy_slider->snd_elem (), 0, this, 0 );
      }
      disconnect ( _proxy_slider, 0, this, 0 );
    }

    _proxy_slider = proxy_n;

    setup_values ();

    if ( _proxy_slider != nullptr ) {
      if ( _proxy_slider->snd_elem () != nullptr ) {
        connect ( _proxy_slider->snd_elem (),
                  SIGNAL ( sig_values_changed () ),
                  this,
                  SLOT ( update_values () ) );
      }
      connect ( _proxy_slider,
                SIGNAL ( destroyed ( QObject * ) ),
                this,
                SLOT ( proxy_destroyed () ) );

      update_values ();
    }
  }
}

QString
Slider_Status_Widget::elem_name () const
{
  QString res;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->group_name ();
    if ( res != _proxy_slider->item_name () ) {
      res += " - ";
      res += _proxy_slider->item_name ();
    }
  }
  return res;
}

bool
Slider_Status_Widget::elem_has_volume () const
{
  return ( _proxy_slider != nullptr );
}

bool
Slider_Status_Widget::elem_has_dB () const
{
  bool res ( false );
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->snd_elem ()->has_dB ();
  }
  return res;
}

long
Slider_Status_Widget::elem_volume_value () const
{
  long res ( 0 );
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->integer_value ();
  }
  return res;
}

void
Slider_Status_Widget::elem_set_volume ( long value_n ) const
{
  if ( _proxy_slider != nullptr ) {
    _proxy_slider->set_integer_value ( value_n );
  }
}

long
Slider_Status_Widget::elem_volume_min () const
{
  long res ( 0 );
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->integer_min ();
  }
  return res;
}

long
Slider_Status_Widget::elem_volume_max () const
{
  long res ( 0 );
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->integer_max ();
  }
  return res;
}

long
Slider_Status_Widget::elem_dB_value () const
{
  long res ( 0 );
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->snd_elem ()->dB_value ( _proxy_slider->elem_idx () );
  }
  return res;
}

void
Slider_Status_Widget::elem_set_nearest_dB ( long dB_value_n ) const
{
  if ( _proxy_slider != nullptr ) {
    const long vol_near (
        _proxy_slider->snd_elem ()->ask_int_from_dB ( dB_value_n ) );
    _proxy_slider->set_integer_value ( vol_near );
  }
}

long
Slider_Status_Widget::elem_dB_min () const
{
  long res ( 0 );
  if ( _proxy_slider != nullptr ) {
    long dummy;
    _proxy_slider->snd_elem ()->dB_range ( &res, &dummy );
  }
  return res;
}

long
Slider_Status_Widget::elem_dB_max () const
{
  long res ( 0 );
  if ( _proxy_slider != nullptr ) {
    long dummy;
    _proxy_slider->snd_elem ()->dB_range ( &dummy, &res );
  }
  return res;
}

} // namespace MWdg::HCtl
