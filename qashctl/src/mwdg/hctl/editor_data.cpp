/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "editor_data.hpp"

namespace MWdg::HCtl
{

Editor_Data::Editor_Data ()
: ctl_info_db ( nullptr )
, wdg_style_db ( nullptr )
, snd_elem_group ( nullptr )
, elem_idx ( 0 )
, image_alloc ( nullptr )
{
}

Editor_Data::~Editor_Data () = default;

} // namespace MWdg::HCtl
