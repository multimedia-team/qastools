/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QAbstractItemModel>
#include <QStandardItemModel>
#include <QStringList>
#include <vector>

// Forward declaration
namespace QSnd::HCtl
{
class Mixer;
class Elem;
class Elem_Group;
} // namespace QSnd::HCtl

namespace MWdg::HCtl
{

/// @brief Tree_Model
///
class Tree_Model : public QStandardItemModel
{
  Q_OBJECT;

  public:
  // -- Construction

  Tree_Model ( QObject * parent_n = nullptr );

  ~Tree_Model ();

  // -- Mixer class

  QSnd::HCtl::Mixer *
  snd_mixer () const
  {
    return _snd_mixer;
  }

  void
  set_snd_mixer ( QSnd::HCtl::Mixer * snd_mixer_n );

  void
  set_snd_dir_foreground ( unsigned int dir_n, const QBrush & brush_n );

  /// @return 0 - nothing; 1 - iface_type; 2 - group/element; 3 - group/element
  unsigned int
  index_data ( const QModelIndex & idx_n,
               QSnd::HCtl::Elem_Group ** grp_n,
               unsigned int * elem_index_n,
               unsigned int * iface_type_n = nullptr ) const;

  QModelIndex
  elem_index ( QSnd::HCtl::Elem * elem_n ) const;

  QModelIndex
  elem_desc_index ( const QString & iface_name_n,
                    const QString & elem_name_n,
                    unsigned int elem_idx_n ) const;

  protected:
  // -- Protected types

  struct Type_Group
  {
    unsigned int iface_type_idx;
    std::vector< QSnd::HCtl::Elem_Group * > list;
  };

  private:
  // -- Private methods

  void
  clear ();

  void
  load ();

  void
  compress_group_list ( std::vector< QSnd::HCtl::Elem_Group * > & lst_n );

  void
  update_colors ();

  const QBrush *
  get_foreground ( const QString & name_n ) const;

  private:
  // -- Attributes
  QSnd::HCtl::Mixer * _snd_mixer;
  std::vector< Type_Group * > _type_groups;
  QBrush _brush_snd_dir[ 2 ];
};

} // namespace MWdg::HCtl
