/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/int_proxy/slider.hpp"
#include "mwdg/slider_status_widget.hpp"

namespace MWdg::HCtl
{

/// @brief Slider_Status_Widget
///
class Slider_Status_Widget : public MWdg::Slider_Status_Widget
{
  Q_OBJECT

  public:
  // -- Construction

  Slider_Status_Widget ( QWidget * parent_n = nullptr );

  ~Slider_Status_Widget ();

  MWdg::HCtl::Int_Proxy::Slider *
  proxy_slider () const
  {
    return _proxy_slider;
  }

  // -- Element info

  QString
  elem_name () const;

  bool
  elem_has_volume () const;

  bool
  elem_has_dB () const;

  long
  elem_volume_value () const;

  void
  elem_set_volume ( long value_n ) const;

  long
  elem_volume_min () const;

  long
  elem_volume_max () const;

  long
  elem_dB_value () const;

  void
  elem_set_nearest_dB ( long dB_value_n ) const;

  long
  elem_dB_min () const;

  long
  elem_dB_max () const;

  // -- Slider selection

  void
  select_slider ( unsigned int grp_idx_n, unsigned int col_idx_n );

  protected:
  // -- Protected slots

  Q_SLOT
  void
  proxy_destroyed ();

  protected:
  // -- Protected methods

  void
  set_slider_proxy ( MWdg::HCtl::Int_Proxy::Slider * proxy_n );

  private:
  // -- Attributes
  MWdg::HCtl::Int_Proxy::Slider * _proxy_slider;
};

} // namespace MWdg::HCtl
