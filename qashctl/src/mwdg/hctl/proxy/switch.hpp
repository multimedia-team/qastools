/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/proxy/proxy.hpp"
#include <QObject>

namespace MWdg::HCtl::Proxy
{

/// @brief Switch
///
class Switch : public MWdg::HCtl::Proxy::Proxy
{
  Q_OBJECT

  public:
  // -- Construction

  Switch ( QObject * parent_n );

  ~Switch ();

  bool
  switch_state () const
  {
    return _switch_state;
  }

  // -- Signals

  Q_SIGNAL
  void
  sig_switch_state_changed ( bool state_n );

  // -- Public slots

  Q_SLOT
  void
  set_switch_state ( bool state_n );

  Q_SLOT
  void
  update_value_from_source ();

  protected:
  // -- Protected methods

  void
  switch_state_changed ();

  private:
  // -- Attributes
  bool _switch_state;
  bool _updating_state;
};

} // namespace MWdg::HCtl::Proxy
