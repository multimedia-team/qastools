/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>

// Forward declaration
namespace QSnd::HCtl
{
class Elem;
}

namespace MWdg::HCtl::Proxy
{

/// @brief Proxy
///
class Proxy : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Proxy ( QObject * parent_n = nullptr );

  ~Proxy () override;

  // -- Snd element

  QSnd::HCtl::Elem *
  snd_elem () const
  {
    return _snd_elem;
  }

  void
  set_snd_elem ( QSnd::HCtl::Elem * elem_n );

  // -- Snd element index

  unsigned int
  elem_idx () const
  {
    return _elem_idx;
  }

  void
  set_elem_idx ( unsigned int idx_n );

  // -- Enabled

  bool
  is_enabled () const
  {
    return _is_enabled;
  }

  void
  set_enabled ( bool flag_n );

  // -- Joined

  bool
  is_joined () const
  {
    return _is_joined;
  }

  void
  set_joined ( bool flag_n );

  bool
  joined_by_key () const;

  // -- Focus

  bool
  has_focus () const
  {
    return _has_focus;
  }

  // -- Signals

  Q_SIGNAL
  void
  sig_enabled_changed ( bool flag_n );

  // -- Public slots

  Q_SLOT
  virtual void
  update_value_from_source ();

  protected:
  // -- Event processing

  bool
  eventFilter ( QObject * obj_n, QEvent * event_n ) override;

  private:
  // -- Attributes
  QSnd::HCtl::Elem * _snd_elem;
  unsigned int _elem_idx;
  bool _is_enabled;
  bool _is_joined;
  bool _has_focus;
};

} // namespace MWdg::HCtl::Proxy
