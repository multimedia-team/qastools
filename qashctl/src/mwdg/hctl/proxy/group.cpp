/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "group.hpp"
#include "qsnd/hctl/elem.hpp"
#include <iostream>

namespace MWdg::HCtl::Proxy
{

Group::Group ( QObject * parent_n )
: QObject ( parent_n )
{
}

Group::~Group () = default;

void
Group::append_proxy ( MWdg::HCtl::Proxy::Proxy * proxy_n )
{
  if ( proxy_n != nullptr ) {
    _proxies.push_back ( proxy_n );
    proxy_n->setParent ( this );
  }
}

void
Group::update_values ()
{
  for ( std::size_t pii = 0; pii < num_proxies (); ++pii ) {
    proxy ( pii )->update_value_from_source ();
  }
}

void
Group::set_joined ( bool flag_n )
{
  for ( std::size_t pii = 0; pii < num_proxies (); ++pii ) {
    MWdg::HCtl::Proxy::Proxy * pro = proxy ( pii );
    pro->set_joined ( flag_n );
    if ( ( pro->snd_elem () != nullptr ) &&
         ( pro->snd_elem ()->is_writable () ) ) {
      pro->set_enabled ( ( pii == 0 ) || !flag_n );
    }
  }

  if ( flag_n && ( num_proxies () > 1 ) ) {
    MWdg::HCtl::Proxy::Proxy * pro = proxy ( 0 );
    if ( ( pro->snd_elem () != nullptr ) &&
         ( pro->snd_elem ()->is_writable () ) ) {
      pro->snd_elem ()->level_values ();
    }
  }
}

} // namespace MWdg::HCtl::Proxy
