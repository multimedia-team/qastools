/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "proxy.hpp"
#include "qsnd/hctl/elem.hpp"
#include <QApplication>
#include <QFocusEvent>

namespace MWdg::HCtl::Proxy
{

Proxy::Proxy ( QObject * parent_n )
: QObject ( parent_n )
, _snd_elem ( nullptr )
, _elem_idx ( 0 )
, _is_enabled ( true )
, _is_joined ( false )
, _has_focus ( false )
{
}

Proxy::~Proxy () = default;

void
Proxy::set_snd_elem ( QSnd::HCtl::Elem * elem_n )
{
  if ( _snd_elem != nullptr ) {
    disconnect ( _snd_elem, 0, this, 0 );
  }

  _snd_elem = elem_n;

  if ( _snd_elem != nullptr ) {
    connect ( _snd_elem,
              SIGNAL ( sig_values_changed () ),
              this,
              SLOT ( update_value_from_source () ) );
  }
}

void
Proxy::set_elem_idx ( unsigned int idx_n )
{
  _elem_idx = idx_n;
}

void
Proxy::set_joined ( bool flag_n )
{
  _is_joined = flag_n;
}

void
Proxy::set_enabled ( bool flag_n )
{
  if ( is_enabled () != flag_n ) {
    _is_enabled = flag_n;
    Q_EMIT sig_enabled_changed ( is_enabled () );
  }
}

void
Proxy::update_value_from_source ()
{
  // Dummy implementation
}

bool
Proxy::joined_by_key () const
{
  bool res ( true );
  if ( ( QApplication::keyboardModifiers () & Qt::ControlModifier ) == 0 ) {
    res = false;
  }
  res = ( res && has_focus () );
  return res;
}

bool
Proxy::eventFilter ( QObject * obj_n, QEvent * event_n )
{
  bool res = QObject::eventFilter ( obj_n, event_n );

  if ( !res ) {
    if ( event_n->type () == QEvent::FocusIn ) {
      _has_focus = true;
    } else if ( event_n->type () == QEvent::FocusOut ) {
      _has_focus = false;
    }
  }

  return res;
}

} // namespace MWdg::HCtl::Proxy
