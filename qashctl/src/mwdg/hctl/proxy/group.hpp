/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/proxy/proxy.hpp"
#include <QObject>
#include <vector>

namespace MWdg::HCtl::Proxy
{

/// @brief Group
///
class Group : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Group ( QObject * parent_n );

  ~Group () override;

  // -- Proxies

  std::size_t
  num_proxies () const
  {
    return _proxies.size ();
  }

  MWdg::HCtl::Proxy::Proxy *
  proxy ( std::size_t idx_n ) const
  {
    return _proxies[ idx_n ];
  }

  void
  append_proxy ( MWdg::HCtl::Proxy::Proxy * proxy_n );

  // -- Public slots

  Q_SLOT
  void
  update_values ();

  Q_SLOT
  void
  set_joined ( bool flag_n );

  private:
  // -- Attributes
  std::vector< MWdg::HCtl::Proxy::Proxy * > _proxies;
};

} // namespace MWdg::HCtl::Proxy
