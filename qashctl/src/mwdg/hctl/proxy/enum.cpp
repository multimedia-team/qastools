/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "enum.hpp"
#include "qsnd/hctl/elem.hpp"
#include <iostream>

namespace MWdg::HCtl::Proxy
{

Enum::Enum ( QObject * parent_n )
: MWdg::HCtl::Proxy::Proxy ( parent_n )
, _enum_index ( 0 )
, _updating_state ( false )
{
}

Enum::~Enum () = default;

unsigned int
Enum::enum_num_items () const
{
  return snd_elem ()->enum_num_items ();
}

const char *
Enum::enum_item_name ( unsigned int index_n )
{
  return snd_elem ()->enum_item_name ( index_n );
}

void
Enum::set_enum_index ( unsigned int index_n )
{
  if ( enum_index () != index_n ) {
    _enum_index = index_n;
    this->enum_index_changed ();
    Q_EMIT sig_enum_index_changed ( enum_index () );
    Q_EMIT sig_enum_index_changed ( static_cast< int > ( enum_index () ) );
  }
}

void
Enum::set_enum_index ( int index_n )
{
  if ( index_n >= 0 ) {
    set_enum_index ( static_cast< unsigned int > ( index_n ) );
  }
}

void
Enum::enum_index_changed ()
{
  if ( ( snd_elem () != nullptr ) && ( snd_elem ()->is_writable () ) &&
       !_updating_state ) {
    if ( is_joined () || joined_by_key () ) {
      snd_elem ()->set_enum_index_all ( enum_index () );
    } else {
      snd_elem ()->set_enum_index ( elem_idx (), enum_index () );
    }
  }
}

void
Enum::update_value_from_source ()
{
  if ( ( snd_elem () != nullptr ) && !_updating_state ) {
    _updating_state = true;
    set_enum_index ( snd_elem ()->enum_index ( elem_idx () ) );
    _updating_state = false;
  }
}

} // namespace MWdg::HCtl::Proxy
