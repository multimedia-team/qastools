/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

/// @brief Initializes program wide used variables one time
int
init_globals ();
