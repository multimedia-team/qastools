/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/inputs_setup.hpp"
#include "mwdg/mixer_device_setup.hpp"
#include "views/device_selection_view_setup.hpp"
#include "views/mixer_hctl_setup.hpp"
#include <QByteArray>
#include <QKeySequence>

/// @brief Main_Window_Setup
///
class Main_Window_Setup
{
  public:
  Main_Window_Setup ();

  public:
  // -- Attributes
  bool show_dev_select;

  MWdg::Mixer_Device_Setup mixer_dev;
  MWdg::Inputs_Setup inputs;

  Views::Mixer_HCTL_Setup hctl;
  Views::Device_Selection_View_Setup dev_select;
};
