/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "mixer_hctl_setup.hpp"

namespace Views
{

Mixer_HCTL_Setup::Mixer_HCTL_Setup () = default;

Mixer_HCTL_Setup::~Mixer_HCTL_Setup () = default;

} // namespace Views
