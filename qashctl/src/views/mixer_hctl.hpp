/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "views/view_base.hpp"
#include <QSplitter>
#include <QStackedLayout>
#include <QTableView>

// Forward declaration
namespace QSnd::HCtl
{
class Mixer;
}
namespace Wdg
{
class Tree_View_KV;
}
namespace MWdg::HCtl
{
class Mixer;
class Table_Model;
class Tree_Model;
} // namespace MWdg::HCtl
namespace Views
{
class Mixer_HCTL_Setup;
}
namespace Wdg
{
class Style_Db;
}
namespace dpe
{
class Image_Allocator;
}

namespace Views
{

/// @brief Mixer_HCTL
///
class Mixer_HCTL : public Views::View_Base
{
  Q_OBJECT

  public:
  // -- Construction

  Mixer_HCTL ( Wdg::Style_Db * wdg_style_db_n,
               dpe::Image_Allocator * image_alloc_n,
               QWidget * parent_n = nullptr );

  ~Mixer_HCTL () override;

  // -- Mixer device and view setup

  void
  set_mixer_dev_setup ( const MWdg::Mixer_Device_Setup * setup_n ) override;

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n ) override;

  void
  set_view_setup ( Views::View_Base_Setup * setup_n ) override;

  void
  save_state ();

  protected:
  // -- Protected slots

  Q_SLOT
  void
  tree_element_selected ( const QModelIndex & idx_n );

  Q_SLOT
  void
  table_element_selected ( const QModelIndex & idx_n );

  Q_SLOT
  void
  update_tree_model_colors ();

  protected:
  // -- Protected methods

  void
  setup_view ();

  void
  expand_tree_items ();

  void
  adjust_table_columns ();

  void
  restore_tree_view_selection ();

  private:
  // -- Attributes
  Views::Mixer_HCTL_Setup * _view_setup = nullptr;
  Wdg::Style_Db * _wdg_style_db = nullptr;

  // Databases
  QSnd::HCtl::Mixer * _snd_mixer;
  MWdg::HCtl::Tree_Model * _tree_model;
  MWdg::HCtl::Table_Model * _table_model;

  // Splitter
  QSplitter _splitter;

  // Side widgets
  QWidget _wdg_side;
  Wdg::Tree_View_KV * _tree_view;

  // Central widgets
  QWidget _wdg_center;
  QStackedLayout * _lay_center_stack;
  QTableView * _table_view;
  MWdg::HCtl::Mixer * _mixer_hctl;

  unsigned int _default_iface_type_idx;
};

} // namespace Views
