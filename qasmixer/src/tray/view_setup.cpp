/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "view_setup.hpp"

namespace Tray
{

View_Setup::View_Setup () = default;

View_Setup::~View_Setup () = default;

} // namespace Tray
