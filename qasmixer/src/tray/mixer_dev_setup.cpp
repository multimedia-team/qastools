/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "mixer_dev_setup.hpp"

namespace Tray
{

Mixer_Dev_Setup::Mixer_Dev_Setup ()
: user_device ( "hw:0" )
{
}

Mixer_Dev_Setup::~Mixer_Dev_Setup () = default;

} // namespace Tray
