/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "stream_view_selection_actions.hpp"
#include "mwdg/inputs_setup.hpp"
#include "qastools_config.hpp"
#include <QFileInfo>

namespace MWdg
{

Stream_View_Selection_Actions::Stream_View_Selection_Actions (
    QObject * parent_n )
: QObject ( parent_n )
{
  // Button texts
  _act_stream_text[ 0 ] = tr ( "Show &playback controls" );
  _act_stream_text[ 1 ] = tr ( "Show &capture controls" );

  // Button icons
  {
    const QString fd_app[ 2 ] = { "show-playback", "show-capture" };
    const QString fd_def[ 2 ] = { "media-playback-start", "media-record" };

    QString icon_path_base ( INSTALL_DIR_APP_ICONS );
    icon_path_base.append ( "/" );
    for ( std::size_t ii = 0; ii < 2; ++ii ) {
      QIcon icon;
      {
        QString icon_path ( icon_path_base );
        icon_path.append ( fd_app[ ii ] );
        icon_path.append ( ".svg" );
        QFileInfo finfo ( icon_path );
        if ( finfo.exists () && finfo.isReadable () ) {
          icon = QIcon ( icon_path );
        } else {
          icon = QIcon::fromTheme ( fd_def[ ii ] );
        }
      }
      _act_stream_icon[ ii ] = icon;
    }
  }

  // Actions
  for ( std::size_t ii = 0; ii < 2; ++ii ) {
    _actions[ ii ].reset ( new QAction ( this ) );
    _actions[ ii ]->setCheckable ( true );
    _actions[ ii ]->setText ( _act_stream_text[ ii ] );
    if ( !_act_stream_icon[ ii ].isNull () ) {
      _actions[ ii ]->setIcon ( _act_stream_icon[ ii ] );
    }
  }

  connect ( _actions[ 0 ].get (),
            &QAction::toggled,
            this,
            &Stream_View_Selection_Actions::playback_changed );

  connect ( _actions[ 1 ].get (),
            &QAction::toggled,
            this,
            &Stream_View_Selection_Actions::capture_changed );
}

Stream_View_Selection_Actions::~Stream_View_Selection_Actions () = default;

void
Stream_View_Selection_Actions::set_inputs_setup (
    const MWdg::Inputs_Setup * setup_n )
{
  if ( setup_n != nullptr ) {
    // Set the tooltip shortcut
    for ( std::size_t ii = 0; ii < 2; ++ii ) {
      _actions[ ii ]->setShortcut ( setup_n->ks_toggle_vis_stream[ ii ] );
    }
  }
}

void
Stream_View_Selection_Actions::set_playback_silent ( bool value_n )
{
  ++_set_silent[ 0 ];
  _actions[ 0 ]->setChecked ( value_n );
  --_set_silent[ 0 ];
}

void
Stream_View_Selection_Actions::set_capture_silent ( bool value_n )
{
  ++_set_silent[ 1 ];
  _actions[ 1 ]->setChecked ( value_n );
  --_set_silent[ 1 ];
}

void
Stream_View_Selection_Actions::set_available ( bool value_n )
{
  if ( _available != value_n ) {
    _available = value_n;
    _actions[ 0 ]->setEnabled ( _available );
    _actions[ 1 ]->setEnabled ( _available );
  }
}

void
Stream_View_Selection_Actions::playback_changed ( bool value_n )
{
  if ( _set_silent[ 0 ] == 0 ) {
    Q_EMIT sig_playback ( value_n );
  }
}

void
Stream_View_Selection_Actions::capture_changed ( bool value_n )
{
  if ( _set_silent[ 1 ] == 0 ) {
    Q_EMIT sig_capture ( value_n );
  }
}

} // namespace MWdg
