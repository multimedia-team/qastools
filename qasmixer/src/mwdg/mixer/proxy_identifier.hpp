/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>

namespace MWdg::Mixer
{

/// @brief Identifies a proxy element
///
class Proxy_Identifier
{
  public:
  // -- Construction

  Proxy_Identifier ();

  ~Proxy_Identifier ();

  void
  clear ();

  bool
  is_valid () const
  {
    return !group_id.isEmpty ();
  }

  public:
  // -- Attributes
  QString group_id;
  unsigned char column_idx = 0;
  unsigned char row_idx = 0;
  bool has_focus = false;
};

} // namespace MWdg::Mixer
