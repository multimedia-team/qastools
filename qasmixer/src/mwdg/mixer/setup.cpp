/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "setup.hpp"

namespace MWdg::Mixer
{

Setup::Setup () = default;

Setup::~Setup () = default;

} // namespace MWdg::Mixer
