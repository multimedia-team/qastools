/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "proxy_identifier.hpp"

namespace MWdg::Mixer
{

Proxy_Identifier::Proxy_Identifier () = default;

Proxy_Identifier::~Proxy_Identifier () = default;

void
Proxy_Identifier::clear ()
{
  group_id.clear ();
  column_idx = 0;
  row_idx = 0;
  has_focus = false;
}

} // namespace MWdg::Mixer
