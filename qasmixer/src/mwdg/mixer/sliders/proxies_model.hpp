/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/mixer/proxies_model.hpp"
#include <array>
#include <vector>

// Forward declaration
namespace MState::Mixer
{
class State;
}
namespace MWdg::Mixer::Proxy
{
class Group;
}
namespace QSnd::Mixer
{
class Mixer;
}

namespace MWdg::Mixer::Sliders
{

/// @brief Proxies_Model
///
class Proxies_Model : public MWdg::Mixer::Proxies_Model
{
  Q_OBJECT

  public:
  // -- Construction

  Proxies_Model ( QObject * parent_n = nullptr );

  ~Proxies_Model () override;

  // -- Setup

  private:
  // -- Utilty

  void
  create_proxies_groups ( Proxies_Groups_Owner & groups_n ) override;

  void
  setup_proxies_group_joined ( MWdg::Mixer::Proxy::Group * pgrp_n );

  void
  setup_proxies_group_separate ( MWdg::Mixer::Proxy::Group * pgrp_n );

  void
  create_proxies_column ( MWdg::Mixer::Proxy::Group * pgrp_n,
                          unsigned int channel_idx_n );

  MState::Mixer::Controls *
  get_mixer_state_controls () const override;

  void
  join_proxies_group ( MWdg::Mixer::Proxy::Group * pgrp_n ) override;

  void
  separate_proxies_group ( MWdg::Mixer::Proxy::Group * pgrp_n ) override;

  private:
  // Strings
  struct
  {
    QString l10n_slider[ 2 ];
    QString l10n_switch[ 2 ];
  } _tool_tip;
};

} // namespace MWdg::Mixer::Sliders
