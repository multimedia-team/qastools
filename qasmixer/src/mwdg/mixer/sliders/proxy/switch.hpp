/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/mixer/elem.hpp"
#include "wdg/pad_proxy/switch.hpp"

namespace MWdg::Mixer::Sliders::Proxy
{

/// @brief Switch
///
class Switch : public Wdg::Pad_Proxy::Switch
{
  Q_OBJECT

  public:
  // -- Construction

  Switch ();

  ~Switch ();

  // -- Mixer element

  QSnd::Mixer::Elem *
  qsnd_mixer_elem () const
  {
    return _qsnd_mixer_elem;
  }

  void
  set_qsnd_mixer_elem ( QSnd::Mixer::Elem * selem_n );

  // -- Snd dir

  unsigned char
  snd_dir () const
  {
    return _snd_dir;
  }

  void
  set_snd_dir ( unsigned char dir_n );

  // -- Channel index

  unsigned int
  channel_idx () const
  {
    return _channel_idx;
  }

  void
  set_channel_idx ( unsigned int idx_n );

  // -- is joined

  bool
  is_joined () const
  {
    return _is_joined;
  }

  void
  set_is_joined ( bool flag_n );

  // -- Event processing

  bool
  eventFilter ( QObject * obj_n, QEvent * event_n ) override;

  // -- Public slots

  Q_SLOT
  void
  update_value_from_source () override;

  protected:
  // -- Protected methods

  void
  switch_state_changed ();

  private:
  // -- Attributes
  QSnd::Mixer::Elem * _qsnd_mixer_elem;
  unsigned int _channel_idx;
  unsigned char _snd_dir;
  bool _is_joined;
  bool _alsa_updating;
};

} // namespace MWdg::Mixer::Sliders::Proxy
