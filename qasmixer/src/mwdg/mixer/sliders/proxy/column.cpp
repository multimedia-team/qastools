/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "column.hpp"
#include "mwdg/mixer/sliders/proxy/slider.hpp"
#include "mwdg/mixer/sliders/proxy/switch.hpp"
#include "wdg/event_types.hpp"
#include "wdg/pass_events.hpp"
#include <QCoreApplication>
#include <iostream>

namespace MWdg::Mixer::Sliders::Proxy
{

Column::Column ()
{
  _str_value_dB = tr ( "%1 dB" );
  _str_value_pc = tr ( "%1 %" );
}

Column::~Column () = default;

inline Slider *
Column::mixer_slider_proxy () const
{
  return static_cast< Slider * > ( slider_proxy () );
}

inline Switch *
Column::mixer_switch_proxy () const
{
  return static_cast< Switch * > ( switch_proxy () );
}

void
Column::update_mixer_values ()
{
  if ( slider_proxy () != nullptr ) {
    slider_proxy ()->update_value_from_source ();
  }
  if ( switch_proxy () != nullptr ) {
    switch_proxy ()->update_value_from_source ();
  }
}

void
Column::slider_proxy_changed ()
{
  update_connections ();
}

void
Column::switch_proxy_changed ()
{
  update_connections ();
}

void
Column::show_value_string_changed ()
{
  update_connections ();
}

void
Column::update_connections ()
{
  if ( slider_proxy () != nullptr ) {
    Slider * msps = mixer_slider_proxy ();
    disconnect ( msps, 0, this, 0 );
    if ( show_value_string () ) {
      if ( msps->has_dB () ) {
        connect ( msps,
                  SIGNAL ( sig_dB_value_changed ( long ) ),
                  this,
                  SIGNAL ( sig_value_string_changed () ) );
      } else {
        connect ( msps,
                  SIGNAL ( sig_slider_index_changed ( unsigned long ) ),
                  this,
                  SIGNAL ( sig_value_string_changed () ) );
      }
    }
  }
}

// Value string

QString
Column::value_string () const
{
  QString res;
  if ( has_slider () && show_value_string () ) {
    Slider * msps = mixer_slider_proxy ();
    if ( msps->has_dB () ) {
      dB_string ( res, msps->dB_value () );
    } else {
      percent_string ( res, msps->volume_permille () );
    }
  }
  return res;
}

QString
Column::value_min_string () const
{
  QString res;
  if ( has_slider () && show_value_string () ) {
    Slider * msps = mixer_slider_proxy ();
    if ( msps->has_dB () ) {
      dB_string ( res, msps->dB_max () );
    } else {
      percent_string ( res, -1000 );
    }
  }
  return res;
}

QString
Column::value_max_string () const
{
  QString res;
  if ( has_slider () && show_value_string () ) {
    Slider * msps = mixer_slider_proxy ();
    if ( msps->has_dB () ) {
      dB_string ( res, msps->dB_min () );
    } else {
      percent_string ( res, 1000 );
    }
  }
  return res;
}

void
Column::dB_string ( QString & str_n, long dB_value_n ) const
{
  str_n = _loc.toString ( dB_value_n / 100.0, 'f', 2 );
  str_n = _str_value_dB.arg ( str_n );
}

void
Column::percent_string ( QString & str_n, int permille_n ) const
{
  str_n = _loc.toString ( permille_n / 10.0, 'f', 1 );
  str_n = _str_value_pc.arg ( str_n );
}

bool
Column::event ( QEvent * event_n )
{
  if ( event_n->type () == Wdg::evt_pass_event_key ) {
    if ( parent () != nullptr ) {
      Wdg::Pass_Event_Key * ev_kp =
          static_cast< Wdg::Pass_Event_Key * > ( event_n );
      ev_kp->column_idx = column_index ();
      QCoreApplication::sendEvent ( parent (), event_n );
    }
    return true;
  }

  return MWdg::Mixer::Proxy::Column::event ( event_n );
}

} // namespace MWdg::Mixer::Sliders::Proxy
