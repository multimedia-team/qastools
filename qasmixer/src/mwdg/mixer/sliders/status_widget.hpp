/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/slider_status_widget.hpp"

// Forward declaration
namespace MWdg::Mixer::Sliders::Proxy
{
class Slider;
}

namespace MWdg::Mixer::Sliders
{

/// @brief Status_Widget
///
class Status_Widget : public Slider_Status_Widget
{
  Q_OBJECT

  public:
  // -- Construction

  Status_Widget ( QWidget * parent_n = nullptr );

  ~Status_Widget ();

  // -- Proxy

  MWdg::Mixer::Sliders::Proxy::Slider *
  proxy_slider () const
  {
    return _proxy_slider;
  }

  // -- Element info

  QString
  elem_name () const override;

  bool
  elem_has_volume () const override;

  bool
  elem_has_dB () const override;

  long
  elem_volume_value () const override;

  void
  elem_set_volume ( long value_n ) const override;

  long
  elem_volume_min () const override;

  long
  elem_volume_max () const override;

  long
  elem_dB_value () const override;

  void
  elem_set_nearest_dB ( long dB_value_n ) const override;

  long
  elem_dB_min () const override;

  long
  elem_dB_max () const override;

  // -- Slider selection

  void
  select_slider ( unsigned int grp_idx_n, unsigned int col_idx_n );

  protected:
  // -- Protected slots

  Q_SLOT
  void
  proxy_destroyed ();

  protected:
  // -- Protected methods

  void
  set_slider_proxy ( MWdg::Mixer::Sliders::Proxy::Slider * proxy_n );

  private:
  // -- Attributes
  MWdg::Mixer::Sliders::Proxy::Slider * _proxy_slider = nullptr;
};

} // namespace MWdg::Mixer::Sliders
