/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "group.hpp"
#include "qsnd/mixer/elem.hpp"
#include <iostream>

namespace MWdg::Mixer::Switches::Proxy
{

Group::Group ( QObject * parent_n )
: MWdg::Mixer::Proxy::Group ( parent_n )
{
}

Group::~Group () = default;

bool
Group::should_be_separated () const
{
  QSnd::Mixer::Elem * qsme = qsnd_mixer_elem ();
  if ( qsme != nullptr ) {
    if ( ( qsme->num_switch_channels ( snd_dir () ) > 1 ) &&
         !qsme->switches_equal ( snd_dir () ) ) {
      return true;
    }
    if ( ( qsme->num_enum_channels ( snd_dir () ) > 1 ) &&
         !qsme->enums_equal ( snd_dir () ) ) {
      return true;
    }
  }
  return false;
}

} // namespace MWdg::Mixer::Switches::Proxy
