/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/mixer/proxy/column.hpp"

namespace MWdg::Mixer::Switches::Proxy
{

/// @brief Column
///
class Column : public MWdg::Mixer::Proxy::Column
{
  Q_OBJECT

  public:
  // -- Construction

  Column ();

  ~Column () override;

  // -- Interface

  void
  update_mixer_values () override;
};

} // namespace MWdg::Mixer::Switches::Proxy
