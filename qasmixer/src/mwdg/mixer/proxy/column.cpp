/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "column.hpp"
#include <iostream>

namespace MWdg::Mixer::Proxy
{

Column::Column () = default;

Column::~Column () = default;

} // namespace MWdg::Mixer::Proxy
