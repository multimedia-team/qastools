/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/group.hpp"
#include <QTimer>
#include <cstdint>

// Forward declaration
namespace QSnd::Mixer
{
class Elem;
}

namespace MWdg::Mixer::Proxy
{

/// @brief Group
///
class Group : public Wdg::Pad_Proxy::Group
{
  Q_OBJECT

  public:
  // -- Construction

  Group ( QObject * parent_n = nullptr );

  ~Group () override;

  // -- QSnd mixer element

  QSnd::Mixer::Elem *
  qsnd_mixer_elem () const
  {
    return _qsnd_mixer_elem;
  }

  virtual void
  set_qsnd_mixer_elem ( QSnd::Mixer::Elem * qsme_n );

  // -- Stream direction (playback or capture)

  std::uint8_t
  snd_dir () const
  {
    return _snd_dir;
  }

  void
  set_snd_dir ( std::uint8_t dir_n );

  // -- Joined flag

  bool
  is_joined () const
  {
    return _is_joined;
  }

  void
  set_is_joined ( bool flag_n );

  // -- Group visibility

  bool
  is_visible () const
  {
    return _is_visible;
  }

  void
  set_is_visible ( bool flag_n );

  // -- Separation info

  bool
  can_be_separated () const;

  virtual bool
  should_be_separated () const = 0;

  bool
  needs_separation () const;

  // -- Separation request

  bool
  separation_request () const
  {
    return _separation_request;
  }

  void
  set_separation_request ( bool flag_n );

  Q_SIGNAL
  void
  sig_separation_requested ();

  // -- Interface

  Q_SLOT
  void
  update_mixer_values ();

  Q_SIGNAL
  void
  sig_value_change ();

  private:
  // -- Utility

  Q_SLOT
  void
  timer_separation_check ();

  private:
  // -- Attributes
  QSnd::Mixer::Elem * _qsnd_mixer_elem = nullptr;
  std::uint8_t _snd_dir = 0;
  bool _is_joined = false;
  bool _is_visible = false;
  bool _separation_request = false;
  QTimer _separation_timer;
};

} // namespace MWdg::Mixer::Proxy
