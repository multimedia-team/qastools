/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

namespace MWdg::Mixer
{

/// @brief Setup
///
class Setup
{
  public:
  // -- Construction

  Setup ();

  ~Setup ();

  public:
  // -- Attributes
  bool show_slider_value_labels = true;
  bool show_tool_tips = true;
};

} // namespace MWdg::Mixer
