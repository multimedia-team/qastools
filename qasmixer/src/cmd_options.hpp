/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>

/// @brief Command (line) options
///
/// These can come from the command line or from a second instance
class CMD_Options
{

  public:
  CMD_Options ();

  public:
  // -- Attributes
  bool start_tray_minimized;
  bool start_single_instance;
  QString start_ctl_address;
};
