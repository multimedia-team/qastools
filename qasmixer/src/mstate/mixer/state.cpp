/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "state.hpp"
#include <iostream>

namespace MState::Mixer
{

State::State ()
{
  _splitter_sizes.resize ( 2, 0 );
  _splitter_sizes[ 0 ] = 10000; // A value way too high
  _splitter_sizes[ 1 ] = 1;     // A value way too low

  _show_stream.fill ( true );
}

State::~State () = default;

void
State::set_splitter_sizes ( const QList< int > & list_n )
{
  _splitter_sizes = list_n;

  // Sanitize
  if ( _splitter_sizes.size () != 2 ) {
    _splitter_sizes.resize ( 2, 0 );
  }
  for ( int & size : _splitter_sizes ) {
    if ( size <= 0 ) {
      size = 1;
    }
  }
}

} // namespace MState::Mixer
