/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "views/device_selection_view.hpp"

// Forward declaration
namespace MWdg
{
class Inputs_Setup;
class Stream_View_Selection;
} // namespace MWdg

namespace Views
{

/// @brief Device_Selection_Bar
///
class Device_Selection_Bar : public Views::Device_Selection_View
{
  Q_OBJECT

  public:
  // -- Construction

  Device_Selection_Bar ( QSnd::Cards_Db * cards_db_n,
                         QWidget * parent_n = nullptr );

  ~Device_Selection_Bar ();

  // -- Setup

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  // -- Device settings

  Q_SIGNAL
  void
  sig_device_settings ();

  // -- Stream selection

  MWdg::Stream_View_Selection *
  stream_selection () const
  {
    return _stream_select.get ();
  }

  private:
  // -- Attributes
  std::unique_ptr< MWdg::Stream_View_Selection > _stream_select;
};

} // namespace Views
