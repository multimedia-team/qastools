/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "device_settings_dialog_setup.hpp"

namespace Views
{

Device_Settings_Dialog_Setup::Device_Settings_Dialog_Setup () = default;

Device_Settings_Dialog_Setup::~Device_Settings_Dialog_Setup () = default;

} // namespace Views
