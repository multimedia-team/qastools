/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/card_info.hpp"
#include "qsnd/ctl_address.hpp"
#include "views/multi_page_dialog.hpp"

// Forward declaration
namespace MWdg::Mixer
{
class Proxies_Model;
class Proxies_Settings_Model;
} // namespace MWdg::Mixer
namespace Views
{
class Device_Settings_Dialog_Setup;
}
namespace Wdg
{
class Style_Db;
}
class QCheckBox;
class QStandardItemModel;
class QStandardItem;
class QTreeView;

namespace Views
{

/// @brief Device_Settings_Dialog
///
class Device_Settings_Dialog : public Views::Multi_Page_Dialog
{
  Q_OBJECT

  public:
  // -- Construction

  Device_Settings_Dialog ( Wdg::Style_Db * wdg_style_db_n,
                           QWidget * parent_n = nullptr,
                           Qt::WindowFlags flags_n = Qt::WindowFlags () );

  ~Device_Settings_Dialog ();

  // -- Setup

  void
  set_setup ( Device_Settings_Dialog_Setup * setup_n );

  void
  set_models ( MWdg::Mixer::Proxies_Model * model_sliders_n,
               MWdg::Mixer::Proxies_Model * model_switches_n );

  // -- Size hint

  QSize
  sizeHint () const override;

  // -- Signals

  Q_SIGNAL
  void
  sig_visibility_changed ();

  Q_SIGNAL
  void
  sig_sorting_changed ();

  protected:
  // -- Event processing

  void
  closeEvent ( QCloseEvent * event_n ) override;

  private:
  // -- Utility

  void
  init_info ();

  void
  init_controls ();

  Q_SLOT
  void
  update_values ();

  void
  update_values_controls ();

  void
  update_values_information ();

  Q_SLOT
  void
  controls_sort_by_stream_changed ( int state_n );

  private:
  // -- Attributes
  Wdg::Style_Db * _wdg_style_db = nullptr;
  Device_Settings_Dialog_Setup * _setup = nullptr;
  // Controls
  struct
  {
    MWdg::Mixer::Proxies_Settings_Model * settings_model;
    QTreeView * tree_view = nullptr;
    QCheckBox * cbox_sort_by_stream = nullptr;
    bool updating_values = false;
  } _controls;
  // Information
  struct
  {
    QSnd::Card_Info card_info;
    QStandardItemModel * model = nullptr;
    QStandardItem * item_address = nullptr;
    QStandardItem * item_index = nullptr;
    QStandardItem * item_id = nullptr;
    QStandardItem * item_name = nullptr;
    QStandardItem * item_long_name = nullptr;
    QStandardItem * item_mixer_name = nullptr;
    QStandardItem * item_driver = nullptr;
    QStandardItem * item_components = nullptr;
  } _info;
};

} // namespace Views
