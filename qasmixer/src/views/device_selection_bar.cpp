/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "device_selection_bar.hpp"
#include "mwdg/stream_view_selection.hpp"
#include <QPushButton>

namespace Views
{

Device_Selection_Bar::Device_Selection_Bar ( QSnd::Cards_Db * cards_db_n,
                                             QWidget * parent_n )
: Views::Device_Selection_View ( cards_db_n, parent_n )
{
  // Device information dialog button
  {
    QPushButton * btn = new QPushButton;
    {
      const QString icon_name ( "preferences-system" );
      if ( QIcon::hasThemeIcon ( icon_name ) ) {
        btn->setIcon ( QIcon::fromTheme ( icon_name ) );
      } else {
        btn->setText ( tr ( "&Settings" ) );
      }
    }
    btn->setToolTip ( tr ( "Show device settings" ) );
    connect ( btn,
              &QPushButton::clicked,
              this,
              &Device_Selection_Bar::sig_device_settings );

    QHBoxLayout * lay_hbox = new QHBoxLayout;
    lay_hbox->setContentsMargins ( 0, 0, 0, 0 );
    lay_hbox->addStretch ( 1 );
    lay_hbox->addWidget ( btn, 0 );
    lay_top ()->addLayout ( lay_hbox );
  }

  // Stream view selection (buttons)
  {
    _stream_select = std::make_unique< MWdg::Stream_View_Selection > ();
    QHBoxLayout * lay_hbox = new QHBoxLayout;
    lay_hbox->setContentsMargins ( 0, 0, 0, 0 );
    lay_hbox->addWidget ( _stream_select.get (), 0 );
    lay_hbox->addStretch ( 1 );
    lay_bottom ()->addLayout ( lay_hbox );
  }
}

Device_Selection_Bar::~Device_Selection_Bar () = default;

void
Device_Selection_Bar::set_inputs_setup ( const MWdg::Inputs_Setup * setup_n )
{
  _stream_select->set_inputs_setup ( setup_n );
}

} // namespace Views
