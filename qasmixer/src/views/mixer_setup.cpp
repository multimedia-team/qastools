/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "mixer_setup.hpp"

namespace Views
{

Mixer_Setup::Mixer_Setup () = default;

Mixer_Setup::~Mixer_Setup () = default;

} // namespace Views
